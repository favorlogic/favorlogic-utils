import {sanitizeFilename} from './sanitize-filename';

describe('sanitizeFilename', () => {
    const maxNameLength = 255;
    const emptyFilenameErrMsg = 'Sanitized filename is empty';

    it('removes illegal characters', () => {
        expect(sanitizeFilename('f/i:le.?\\s*u<ff>i"x|1')).toBe('file.suffix1');
    });

    it('removes control characters', () => {
        expect(sanitizeFilename('asd \x00 b \x1f c \x80 d \x9f asd')).toBe('asd  b  c  d  asd');
    });

    it('replaces reserved folder dots with given char', () => {
        expect(sanitizeFilename('.', '-')).toBe('-');
        expect(sanitizeFilename('..', '-')).toBe('-');
    });

    it('replaces windows reserved filename with given char', () => {
        expect(sanitizeFilename('con', 'x')).toBe('x');
        expect(sanitizeFilename('CoN', 'x')).toBe('x');
        expect(sanitizeFilename('CON.something', 'x')).toBe('x');
        expect(sanitizeFilename('CONa.something', 'x')).toBe('CONa.something');
        expect(sanitizeFilename('com3', 'x')).toBe('x');
        expect(sanitizeFilename('lpt8', 'x')).toBe('x');
        expect(sanitizeFilename('lpt8.', 'x')).toBe('x');
    });

    it('replaces windows reserved filename with given char', () => {
        expect(sanitizeFilename('con', 'x')).toBe('x');
        expect(sanitizeFilename('CoN', 'x')).toBe('x');
        expect(sanitizeFilename('CON.something', 'x')).toBe('x');
        expect(sanitizeFilename('CONa.something', 'x')).toBe('CONa.something');
        expect(sanitizeFilename('com3', 'x')).toBe('x');
        expect(sanitizeFilename('lpt8', 'x')).toBe('x');
        expect(sanitizeFilename('lpt8.', 'x')).toBe('x');
    });

    it('removes windows trailing characters', () => {
        expect(sanitizeFilename('file.ext.')).toBe('file.ext');
        expect(sanitizeFilename('file.ext...')).toBe('file.ext');
        expect(sanitizeFilename('file.ext   ')).toBe('file.ext');
    });

    it('throws on empty filenames', () => {
        expect(() => sanitizeFilename('???')).toThrow(emptyFilenameErrMsg);
        expect(() => sanitizeFilename('.')).toThrow(emptyFilenameErrMsg);
        expect(() => sanitizeFilename('..')).toThrow(emptyFilenameErrMsg);
        expect(() => sanitizeFilename('con')).toThrow(emptyFilenameErrMsg);
        expect(() => sanitizeFilename('com3')).toThrow(emptyFilenameErrMsg);
    });

    it('cuts excessive characters', () => {
        const tooManyRepeats = 50;
        const tooLongFilename = '0123456789abcd'.repeat(tooManyRepeats);
        expect(sanitizeFilename(tooLongFilename)).toBe(tooLongFilename.substring(0, maxNameLength));
    });
});
