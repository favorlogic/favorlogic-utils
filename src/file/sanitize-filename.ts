const maxNameLength = 255;
const illegal = /["*/:<>?\\|]/g;
const control = /[\x00-\x1f\x80-\x9f]/g; // eslint-disable-line no-control-regex
const reserved = /^\.+$/;
const windowsReserved = /^(con|prn|aux|nul|com\d|lpt\d)(\..*)?$/i;
const windowsTrailing = /[ .]+$/;

export function sanitizeFilename(input: string, replacement = ''): string {
    const sanitized = input
        .replace(illegal, replacement)
        .replace(control, replacement)
        .replace(reserved, replacement)
        .replace(windowsReserved, replacement)
        .replace(windowsTrailing, replacement);

    if (sanitized.length === 0) {
        throw new Error('Sanitized filename is empty');
    }

    return sanitized.substring(0, maxNameLength);
}
