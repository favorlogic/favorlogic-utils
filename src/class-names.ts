import {flatten, compact, join} from 'lodash/fp';
import {pipe} from 'ts-opt';

type ClassNameOpt = string | null | undefined | false;

export const classNames = (...names: Array<ClassNameOpt | Array<ClassNameOpt>>): string =>
    pipe(names, flatten, compact, join(' '));
