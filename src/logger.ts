/* eslint-disable no-console */

export interface LoggerConfig {
    devMode: boolean;
}

export class Logger {
    protected devMode: boolean;

    constructor(config: LoggerConfig) {
        this.devMode = config.devMode;
    }

    public log(...params: Array<unknown>): void {
        if (this.devMode) {
            console.log(...params);
        }
    }

    public warn(...params: Array<unknown>): void {
        if (this.devMode) {
            console.warn(...params);
        }
    }

    public error(...params: Array<unknown>): void {
        if (this.devMode) {
            console.error(...params);
        }
    }
}
