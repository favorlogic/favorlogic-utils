/* eslint-disable no-console */

import * as t from 'io-ts';

import {excess} from './excess';
import {validateSchema} from './validate-schema';

const PersonSchema = t.exact(t.intersection([
    t.interface({
        name: t.string,
    }),
    t.partial({
        age: t.number,
    }),
]));

type Person = t.TypeOf<typeof PersonSchema>;

const PersonExcessSchema = excess(t.interface({
    name: t.string,
    age: t.union([t.number, t.null]),
}));

type PersonExcess = t.TypeOf<typeof PersonExcessSchema>;

describe('validate schema', () => {
    let validate: (value: unknown) => Person;
    let validateExcess: (value: unknown) => PersonExcess;

    beforeEach(() => {
        validate = validateSchema(PersonSchema, 'FE');
        validateExcess = validateSchema(PersonExcessSchema, 'FE');
        jest.spyOn(console, 'error').mockImplementation();
    });

    describe('given value is valid', () => {
        it('should return value', () => {
            expect(validate({name: 'a', age: 1})).toEqual({name: 'a', age: 1});
            expect(validate({name: 'a'})).toEqual({name: 'a'});
            expect(validateExcess({name: 'a', age: 1})).toEqual({name: 'a', age: 1});
            expect(validateExcess({name: 'a', age: null})).toEqual({name: 'a', age: null});
        });
    });

    describe('given value is not valid', () => {
        it('should throw error', () => {
            expect(() => validate({})).toThrow('Validation error; source=FE;');
            expect(() => validate({name: 'a', age: '1'})).toThrow('Validation error; source=FE;');
            expect(() => validateExcess({})).toThrow('Validation error; source=FE;');
            expect(() => validateExcess({name: 'a', age: '1'})).toThrow('Validation error; source=FE;');
            expect(() => validateExcess({name: 'a', age: 1, invalid: true})).toThrow('Validation error; source=FE;');
            expect(console.error).toBeCalledTimes(5);
        });
    });
});
