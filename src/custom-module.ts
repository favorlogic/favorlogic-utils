export interface CustomModule {
    hot?: {
        accept(_: string, __: () => void): void,
    };
}
