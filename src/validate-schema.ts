import {match} from 'fp-ts/Either';
import {pipe} from 'fp-ts/function';
import * as t from 'io-ts';
import {failure} from 'io-ts/lib/PathReporter';

type DataSource = 'FE' | 'BE';

const space = 2;

function getErrorMessage(errors: t.Errors, source: DataSource): string {
    const description = failure(errors).join('\n');

    return `Validation error; source=${source};\n${description}`;
}

function logError(value: unknown, errors: t.Errors): void {
    const valueString = JSON.stringify(value, null, space);
    const errorsString = failure(errors).map(x => `-> ${x}`).join('\n');

    // eslint-disable-next-line no-console
    console.error(`Validation error\n${errorsString}\n${valueString}`);
}

export class ValidationError extends Error {
    constructor(message: string, public readonly source: DataSource) {
        super(message);
    }
}

export const validateSchema = <A, O>(type: t.Type<A, O>, source: DataSource) =>
    (value: unknown): A =>
        pipe(
            type.decode(value),
            match(
                (errors: t.Errors) => {
                    logError(value, errors);
                    throw new ValidationError(getErrorMessage(errors, source), source);
                },
                () => value as A,
            ),
        );
