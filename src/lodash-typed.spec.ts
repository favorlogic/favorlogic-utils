import {sortBy} from 'lodash/fp';

import {without, filter, isArray, isObject, reject, toPairs} from './lodash-typed';

describe('lodash', () => {
    describe('without', () => {
        it('leaves out unwanted elements', () => {
            expect(without(4, 3, 9)([3, 9, 8, 5, 4, 2, 1])).toEqual([8, 5, 2, 1]);
        });
    });

    describe('filter', () => {
        it('fikters values', () => {
            expect(filter((x: number) => x > 0)([-5, 0, 5])).toEqual([5]);
        });
    });

    describe('isArray', () => {
        it('returns boolean', () => {
            expect(isArray(0)).toEqual(false);
            expect(isArray('')).toEqual(false);
            expect(isArray(false)).toEqual(false);
            expect(isArray(undefined)).toEqual(false);
            expect(isArray([])).toEqual(true);
        });
    });

    describe('isObject', () => {
        it('returns boolean', () => {
            expect(isObject({})).toBe(true);
            expect(isObject({test: 'test'})).toBe(true);
            expect(isObject([0, 1])).toBe(true);
            expect(isObject('test')).toBe(false);
            expect(isObject(123)).toBe(false);
        });
    });

    describe('reject', () => {
        it('rejects values', () => {
            expect(reject((x: number) => x > 0)([-5, 0, 5])).toEqual([-5, 0]);
        });
    });

    describe('toPairs', () => {
        const sortPairs = sortBy('[0]');

        it('returns key value pairs', () => {
            expect(toPairs({})).toEqual([]);
            expect(sortPairs(toPairs({test: 'value', some: 'other'}))).toEqual([
                ['some', 'other'],
                ['test', 'value'],
            ]);
        });
    });
});
