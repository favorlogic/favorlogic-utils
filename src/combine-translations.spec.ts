import {Resource} from 'i18next';

import {combineTranslations} from './combine-translations';

describe('combineTranslations', () => {
    it('merges translations', () => {
        const data: Array<Resource> = [
            {
                CS: {a: {aa: 'AA'}},
                EN: {},
            },
            {
                CS: {b: {bb: 'BB'}},
                EN: {c: {cc: 'CC'}},
            },
        ];
        const res = combineTranslations(data);
        const expected = {
            CS: {
                a: {
                    aa: 'AA',
                },
                b: {
                    bb: 'BB',
                },
            },
            EN: {
                c: {
                    cc: 'CC',
                },
            },
        };
        expect(res).toEqual(expected);
    });

    it('crashes on conflicts', () => {
        expect(() => {
            combineTranslations([
                {
                    CS: {a: {aa: 'AA'}},
                    EN: {},
                },
                {
                    CS: {a: {aa: 'AA2'}},
                    EN: {},
                },
            ]);
        }).toThrowError('Found conflicting translations: \'CS.a.aa\', \'EN\'.');
    });
});
