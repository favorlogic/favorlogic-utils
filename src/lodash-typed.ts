/* eslint-disable @fl/use-eta-reduction */
import {
    without as _without,
    filter as _filter,
    isArray as _isArray,
    isObject as _isObject,
    reject as _reject,
    toPairs as _toPairs,
} from 'lodash/fp';

export const without = <T>(...elems: Array<T>) =>
    (array: Array<T>): Array<T> => _without(elems)(array);

export const filter = <T>(pred: (_: T) => boolean) =>
    (xs: Array<T>): Array<T> => _filter(pred, xs);

export const isArray = (x: unknown): x is Array<unknown> => _isArray(x);

export const isObject = (x: unknown): x is object => _isObject(x);

export const reject = <T>(pred: (_: T) => boolean) =>
    (xs: Array<T>): Array<T> => _reject(pred, xs);

type Pair<T extends object> = [keyof T, T[keyof T]];

export const toPairs = <T extends object>(obj: T): Array<Pair<T>> =>
    _toPairs(obj) as Array<Pair<T>>;
