import {runSaga, SagaIterator} from 'redux-saga';
import {Effect, takeLatest, takeLeading, takeEvery, call, debounce} from 'redux-saga/effects';

import {putAll, takeLatestF, takeLeadingF, takeEveryF, forkRestartingOnError, debounceF} from './saga-helpers';

jest.mock('redux-saga/effects', () => ({
    ...jest.requireActual('redux-saga/effects'),
    takeLatest: jest.fn(),
    takeLeading: jest.fn(),
    takeEvery: jest.fn(),
    debounce: jest.fn(),
}));

const testAction = () => ({
    type: 'test/action',
}) as const;

const otherTestAction = (value: number) => ({
    type: 'test/other-action',
    payload: {value},
}) as const;

type TestAction = ReturnType<typeof testAction>;

// eslint-disable-next-line @typescript-eslint/no-empty-function
function* actionHandler(_: TestAction): SagaIterator {
}

const expectToBePut = (effect: Effect, action: unknown): void => {
    expect(effect.type).toEqual('PUT');
    expect(effect.payload).toHaveProperty('action', action);
};

const expectToBeAll = (effect: Effect, length: number): void => {
    expect(effect).toHaveProperty('type', 'ALL');
    expect(effect.payload).toBeInstanceOf(Array);
    expect(effect.payload).toHaveLength(length);
};

describe('sagaHelpers', () => {
    describe('putAll', () => {
        it('putAll with one action', () => {
            const action = testAction();
            const effect = putAll(action);

            expectToBePut(effect, action);
        });

        it('putAll with multiple actions', () => {
            const actions = [otherTestAction(1), testAction()];
            const effect = putAll(actions);

            expectToBeAll(effect, 2);
            expectToBePut(effect.payload[0], actions[0]);
            expectToBePut(effect.payload[1], actions[1]);
        });
    });

    describe('takeLatestF', () => {
        it('should call takeLatest', () => {
            takeLatestF('test/action', actionHandler);
            expect(takeLatest).toBeCalledTimes(1);
            expect(takeLatest).toBeCalledWith('test/action', actionHandler);
        });
    });

    describe('takeLeadingF', () => {
        it('should call takeLeading', () => {
            takeLeadingF('test/action', actionHandler);
            expect(takeLeading).toBeCalledTimes(1);
            expect(takeLeading).toBeCalledWith('test/action', actionHandler);
        });
    });

    describe('takeEveryF', () => {
        it('should call takeEvery', () => {
            takeEveryF('test/action', actionHandler);
            expect(takeEvery).toBeCalledTimes(1);
            expect(takeEvery).toBeCalledWith('test/action', actionHandler);
        });
    });

    describe('debounceF', () => {
        it('should call debounce', () => {
            debounceF(200, 'test/action', actionHandler);
            expect(debounce).toBeCalledTimes(1);
            expect(debounce).toBeCalledWith(200, 'test/action', actionHandler);
        });
    });

    describe('forkRestartingOnError', () => {
        let sagaMock: jest.Mock;
        let errorHandler: jest.Mock;
        let logError: jest.SpyInstance;

        const testSaga = function* (): SagaIterator {
            yield call(sagaMock);
        };

        const runForkTest = async () => {
            await runSaga({}, function* () {
                yield forkRestartingOnError(testSaga, errorHandler);
            }).toPromise();
        };

        beforeEach(() => {
            errorHandler = jest.fn();
            sagaMock = jest.fn();
            logError = jest.spyOn(console, 'error').mockImplementation();
        });

        describe('given saga works', () => {
            beforeEach(runForkTest);

            it('should execute saga', () => {
                expect(sagaMock).toBeCalledTimes(1);
            });

            it('should not log error', () => {
                expect(logError).not.toBeCalled();
            });

            it('should not call error handler', () => {
                expect(errorHandler).not.toBeCalled();
            });
        });

        describe('given saga throws', () => {
            let error: Error;

            const mockSagaThrows = () => {
                error = new Error('test error');
                sagaMock.mockImplementationOnce((() => {
                    throw error;
                }));
            };

            beforeEach(mockSagaThrows);
            beforeEach(runForkTest);

            it('should log error', () => {
                expect(logError).toBeCalledTimes(1);
                expect(logError).toBeCalledWith('Error in saga:', 'testSaga', error);
            });

            it('should call error handler', () => {
                expect(errorHandler).toBeCalledTimes(1);
                expect(errorHandler).toBeCalledWith('testSaga', error);
            });

            it('should execute saga again', () => {
                expect(sagaMock).toBeCalledTimes(2);
            });
        });
    });
});
