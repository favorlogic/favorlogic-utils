export const cinPattern = /^\d{8}$/;
export const vatNumberPattern = /^(CZ|SK)\d{8,10}$/;
export const accountNumberPattern = /^(\d{1,6}-)?\d{2,10}$/;
export const emailPattern = /^[\w%+.-]+@[\w.-]+\.[A-Za-z]{2,}$/;
