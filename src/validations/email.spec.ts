import {isEmailValid} from './email';

describe('Email', () => {
    describe('when asked if cin is valid', () => {
        it('should return true for valid email', () => {
            expect(isEmailValid('a@a.aa')).toEqual(true);
            expect(isEmailValid('mail@google.com')).toEqual(true);
            expect(isEmailValid('example@s.example')).toEqual(true);
            expect(isEmailValid('a+b@gg.amsterdam')).toEqual(true);
            expect(isEmailValid('a.b@example.com')).toEqual(true);
        });

        it('also returns true for some invalid email', () => {
            expect(isEmailValid('1234567890123456789012345678901234567890123456789012345678901234+x@example.com'))
                .toEqual(true);
            expect(isEmailValid('john..doe@example.com')).toEqual(true);
            expect(isEmailValid('john.doe@example..com')).toEqual(true);
        });

        it('should return false for string with invalid format', () => {
            expect(isEmailValid('a')).toEqual(false);
            expect(isEmailValid('@')).toEqual(false);
            expect(isEmailValid('@a')).toEqual(false);
            expect(isEmailValid('a@')).toEqual(false);
            expect(isEmailValid('a@a.aa@a.aa')).toEqual(false);
            expect(isEmailValid('just"not"right@example.com')).toEqual(false);
            expect(isEmailValid('this is"not\\allowed@example.com')).toEqual(false);
            expect(isEmailValid('this\\ still\\"not\\\\allowed@example.com')).toEqual(false);
        });

        it('also returns false for string with valid RFC 5322 format', () => {
            expect(isEmailValid('a@a.a')).toEqual(false);
            expect(isEmailValid('admin@mailserver1')).toEqual(false);
            expect(isEmailValid('" "@example.org')).toEqual(false);
            expect(isEmailValid('"john..doe"@example.org')).toEqual(false);
            expect(isEmailValid('john.smith(comment)@example.com')).toEqual(false);
            expect(isEmailValid('jsmith@[192.168.2.1]')).toEqual(false);
            expect(isEmailValid('jsmith@[IPv6:2001:db8::1]')).toEqual(false);
            expect(isEmailValid('"very.unusual.@.unusual.com"@example.com')).toEqual(false);
            expect(isEmailValid('"()<>[]:,;@\\\\"!#$%&\'-/=?^_`{}| ~.a"@example.org')).toEqual(false);
        });

        it('also returns false for string with valid RFC 6530 format', () => {
            expect(isEmailValid('δοκιμή@παράδειγμα.δοκιμή')).toEqual(false);
            expect(isEmailValid('我買@屋企.香港')).toEqual(false);
            expect(isEmailValid('二ノ宮@黒川.日本')).toEqual(false);
            expect(isEmailValid('чебурашка@ящик-с-апельсинами.рф')).toEqual(false);
            expect(isEmailValid('संपर्क@डाटामेल.भारत')).toEqual(false);
        });
    });
});
