import {isAccountNumberValid} from './account-number';

describe('Account number', () => {
    describe('when asked if account number is valid', () => {
        it('should return false for string with invalid format', () => {
            expect(isAccountNumberValid('23002403211')).toEqual(false);
            expect(isAccountNumberValid('115110719-77628621')).toEqual(false);
            expect(isAccountNumberValid('5')).toEqual(false);
            expect(isAccountNumberValid('-2801841305')).toEqual(false);
            expect(isAccountNumberValid('1234567-1234567890')).toEqual(false);
        });

        it('should return false for invalid control sum', () => {
            expect(isAccountNumberValid('2300240321')).toEqual(false);
            expect(isAccountNumberValid('2610233587')).toEqual(false);
            expect(isAccountNumberValid('2801841305')).toEqual(false);
            expect(isAccountNumberValid('43-7510400210')).toEqual(false);
            expect(isAccountNumberValid('10719-77628629')).toEqual(false);
        });

        it('should return true for valid account number', () => {
            expect(isAccountNumberValid('2300240320')).toEqual(true);
            expect(isAccountNumberValid('2600233587')).toEqual(true);
            expect(isAccountNumberValid('2801831305')).toEqual(true);
            expect(isAccountNumberValid('43-7510400217')).toEqual(true);
            expect(isAccountNumberValid('10719-77628621')).toEqual(true);
        });
    });
});
