import {testRe} from 'ts-opt';

import {emailPattern} from './patterns';

export const isEmailValid = testRe(emailPattern);
