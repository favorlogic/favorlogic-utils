import {accountNumberPattern} from './patterns';

// eslint-disable-next-line no-magic-numbers
const weights = [1, 2, 4, 8, 5, 10, 9, 7, 3, 6];

const isAccountNumberPartControlSumValid = (accountNumberPart: string): boolean => {
    const numbers = accountNumberPart.split('').map(Number).reverse();
    const weightedSum = numbers.map((digit, index) => digit * (weights[index] ?? 0))
        .reduce((sum, weightedDigit) => sum + weightedDigit, 0);

    // eslint-disable-next-line no-magic-numbers
    return weightedSum % 11 === 0;
};

export const isAccountNumberControlSumValid = (accountNumber: string): boolean =>
    accountNumber.split('-').every(isAccountNumberPartControlSumValid);

export const isAccountNumberValid = (accountNumber: string): boolean =>
    accountNumberPattern.test(accountNumber) && isAccountNumberControlSumValid(accountNumber);
