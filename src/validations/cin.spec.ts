import {isCinValid} from './cin';

describe('Cin', () => {
    describe('when asked if cin is valid', () => {
        it('should return false for string with non number characters', () => {
            expect(isCinValid('x')).toEqual(false);
            expect(isCinValid('yyy0zzz')).toEqual(false);
            expect(isCinValid('_2345678')).toEqual(false);
        });

        it('should return false for length other then 8', () => {
            expect(isCinValid('')).toEqual(false);
            expect(isCinValid('1234567')).toEqual(false);
            expect(isCinValid('123456789')).toEqual(false);
        });

        it('should return false for wrong control sum digit', () => {
            expect(isCinValid('25596640')).toEqual(false);
            expect(isCinValid('25596642')).toEqual(false);
            expect(isCinValid('25596643')).toEqual(false);
            expect(isCinValid('25596644')).toEqual(false);
            expect(isCinValid('25596645')).toEqual(false);
            expect(isCinValid('25596646')).toEqual(false);
            expect(isCinValid('25596647')).toEqual(false);
            expect(isCinValid('25596648')).toEqual(false);
            expect(isCinValid('25596649')).toEqual(false);
        });

        it('should return true for valid cin', () => {
            expect(isCinValid('25596641')).toEqual(true);
            expect(isCinValid('02213567')).toEqual(true);
            expect(isCinValid('03024130')).toEqual(true);
        });
    });
});
