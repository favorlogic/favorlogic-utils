import {cinPattern} from './patterns';

// eslint-disable-next-line no-magic-numbers
const weights = [8, 7, 6, 5, 4, 3, 2, 0];

const computeControlDigit = (numbers: Array<number>): number => {
    const weightedSum = numbers.map((digit, index) => digit * (weights[index] ?? 0))
        .reduce((sum, weightedDigit) => sum + weightedDigit, 0);

    // eslint-disable-next-line no-magic-numbers
    return (11 - weightedSum % 11) % 10;
};

export const isCinControlSumValid = (ico: string): boolean => {
    const numbers = ico.split('').map(Number);

    // eslint-disable-next-line no-magic-numbers
    return numbers[7] === computeControlDigit(numbers);
};

export const isCinValid = (ico: string): boolean =>
    cinPattern.test(ico) && isCinControlSumValid(ico);
