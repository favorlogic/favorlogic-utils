export type FieldError<V> =
    V extends Array<infer A>
        ? Array<FieldError<A>> | string
        : V extends object
            ? FormErrors<V> | string
            : string;

export type FormErrors<Values> = {
    [K in keyof Values]?: FieldError<Values[K]>
};
