import {formatSize} from './format-size';

const test = (bytes: number, exp: string): void => {
    it(`${bytes} -> ${exp}`, () => {
        expect(formatSize(bytes)).toEqual(exp);
    });
};

describe('formatSize', () => {
    describe('formats', () => {
        test(0, '0 B');
        test(1023, '1023 B');
        test(1024, '1.0 KiB');
        test(1024, '1.0 KiB');
        test(1126, '1.1 KiB');
        test(1024 * 1024, '1.0 MiB');
        test(1024 * 1024 * 1.5, '1.5 MiB');
        test(1024 * 1024 * 1024, '1.0 GiB');
        test(1024 * 1024 * 1024 * 1.5, '1.5 GiB');
        test(1024 * 1024 * 1024 * 9.99, '10.0 GiB');
        test(1024 * 1024 * 1024 * 1024 * 7.86, '7.9 TiB');
    });
});
