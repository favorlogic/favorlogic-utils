import {formatProbability} from './format-probability';

describe('formatProbability', () => {
    it('no decimals', () => {
        expect(formatProbability(1)).toBe('100.000');
        expect(formatProbability(0)).toBe('0.000');
    });
    it('decimals', () => {
        expect(formatProbability(0.204548568)).toBe('20.455');
        expect(formatProbability(0.001)).toBe('0.100');
    });
});
