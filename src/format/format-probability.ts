// eslint-disable-next-line no-magic-numbers
export const formatProbability = (value: number): string => (value * 100).toFixed(3);
