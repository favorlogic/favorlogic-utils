import {flow, isEmpty, join, map, reverse, split, reject} from 'lodash/fp';
import {optInfinity, pipe} from 'ts-opt';

import {chunksOf} from '../basic';

const lengthOfChunk = 3;
const fractionDigits = 2;

const processDecPart = (dec: string = ''): string => dec === '00' ? '' : dec;
const processIntPart = (int: string = ''): string =>
    pipe(
        int,
        split(''),
        reverse,
        chunksOf(lengthOfChunk),
        map(flow(reverse, join(''))),
        reverse,
        join(' '),
    );

export const formatCurrencyCzk = (num: number): string | null => optInfinity(num)
    .mapFlow(
        x => x.toFixed(fractionDigits),
        split('.'),
        ([int, dec]: Array<string>): Array<string> =>
            [processIntPart(int), processDecPart(dec)],
        reject(isEmpty),
        join(','),
        (x: string) => `${x} Kč`,
    )
    .orNull();
/**
 * @deprecated This function has been deprecated, use `formatCurrencyCzk` instead.
 */

export const formatCurrency = formatCurrencyCzk;
