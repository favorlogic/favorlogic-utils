const radix = 10;

export const formatTemperatureCs = (x: number): string => `${x.toString(radix).replace('.', ',')} °C`;

export const formatTemperatureEn = (x: number): string => `${x.toString(radix)} °C`;
