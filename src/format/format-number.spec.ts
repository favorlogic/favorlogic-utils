import {formatNumberCs, formatNumberEn, formatNumberGeneric, separateThousandsOfIntPart} from './format-number';

describe('formatNumber', () => {
    describe('formatNumberCs', () => {
        it('zero number', () => {
            expect(formatNumberCs(2)(0)).toEqual('0,00');
        });

        it('simple', () => {
            expect(formatNumberCs(2)(123)).toEqual('123,00');
            expect(formatNumberCs()(123)).toEqual('123');
            expect(formatNumberCs(0)(123)).toEqual('123');
            expect(formatNumberCs(1)(123)).toEqual('123,0');
            expect(formatNumberCs(5)(123.22354)).toEqual('123,22354');
        });

        it('thousands', () => {
            expect(formatNumberCs(0)(1000)).toEqual('1 000');
            expect(formatNumberCs(3)(1234567)).toEqual('1 234 567,000');
            expect(formatNumberCs(2)(1052.75)).toEqual('1 052,75');
        });

        it('decimal', () => {
            expect(formatNumberCs(2)(1.5)).toEqual('1,50');
            expect(formatNumberCs(0)(1.05)).toEqual('1');
            expect(formatNumberCs(2)(10.75)).toEqual('10,75');
            expect(formatNumberCs(1)(10.75)).toEqual('10,8');
        });

        it('all', () => {
            expect(formatNumberCs(2)(1001.5)).toEqual('1 001,50');
        });
    });

    describe('formatNumberEn', () => {
        it('zero number', () => {
            expect(formatNumberEn(2)(0)).toEqual('0.00');
        });

        it('simple', () => {
            expect(formatNumberEn(2)(123)).toEqual('123.00');
            expect(formatNumberEn(0)(123)).toEqual('123');
            expect(formatNumberEn()(123)).toEqual('123');
            expect(formatNumberEn(1)(123)).toEqual('123.0');
            expect(formatNumberEn(5)(123.22354)).toEqual('123.22354');
        });

        it('thousands', () => {
            expect(formatNumberEn(0)(1000)).toEqual('1,000');
            expect(formatNumberEn(3)(1234567)).toEqual('1,234,567.000');
            expect(formatNumberEn(0)(1052.75)).toEqual('1,053');
            expect(formatNumberEn()(1052.75)).toEqual('1,052.75');
            expect(formatNumberEn(2)(1052.75)).toEqual('1,052.75');
        });

        it('decimal', () => {
            expect(formatNumberEn(2)(1.5)).toEqual('1.50');
            expect(formatNumberEn(0)(1.05)).toEqual('1');
            expect(formatNumberEn(2)(10.75)).toEqual('10.75');
            expect(formatNumberEn(1)(10.75)).toEqual('10.8');
        });

        it('all', () => {
            expect(formatNumberEn(2)(1001.5)).toEqual('1,001.50');
        });
    });

    it('process int part', () => {
        expect(separateThousandsOfIntPart('123', ' ')).toEqual('123');
        expect(separateThousandsOfIntPart('1234', ' ')).toEqual('1 234');
        expect(separateThousandsOfIntPart(undefined, ',')).toEqual('');
    });

    describe('formatNumberGenerics with custom separators', () => {
        const customFormatter = formatNumberGeneric('T', 'D');
        it('zero number', () => {
            expect(customFormatter(2)(0)).toEqual('0D00');
        });

        it('simple', () => {
            expect(customFormatter(2)(123)).toEqual('123D00');
            expect(customFormatter(0)(123)).toEqual('123');
            expect(customFormatter(1)(123)).toEqual('123D0');
            expect(customFormatter(5)(123.22354)).toEqual('123D22354');
        });

        it('thousands', () => {
            expect(customFormatter(0)(1000)).toEqual('1T000');
            expect(customFormatter(3)(1234567)).toEqual('1T234T567D000');
            expect(customFormatter(0)(1052.75)).toEqual('1T053');
            expect(customFormatter()(1052.75)).toEqual('1T052D75');
            expect(customFormatter(2)(1052.75)).toEqual('1T052D75');
        });

        it('decimal', () => {
            expect(customFormatter(2)(1.5)).toEqual('1D50');
            expect(customFormatter(2)(10.75)).toEqual('10D75');
            expect(customFormatter(1)(10.75)).toEqual('10D8');
        });

        it('all', () => {
            expect(customFormatter(2)(1001.5)).toEqual('1T001D50');
        });
    });

    describe('separateThousandsOfIntPart - works correctly', () => {
        expect(separateThousandsOfIntPart('0', ',')).toEqual('0');
        expect(separateThousandsOfIntPart('100800', ',')).toEqual('100,800');
    });

    describe('separateThousandsOfIntPart - works correctly', () => {
        expect(separateThousandsOfIntPart('0', ',')).toEqual('0');
        expect(separateThousandsOfIntPart('100800', ',')).toEqual('100,800');
        separateThousandsOfIntPart(undefined, ',');
    });

    describe('Invalid number returns null', () => {
        expect(formatNumberCs(2)(NaN)).toBeNull();
        expect(formatNumberCs(2)(Infinity)).toBeNull();
        expect(formatNumberCs(2)(-Infinity)).toBeNull();
    });
});
