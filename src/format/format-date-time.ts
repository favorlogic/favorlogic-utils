import moment, {MomentBuiltinFormat, ISO_8601} from 'moment';
import {Opt, opt} from 'ts-opt';

export const dateFormat = 'D. M. YYYY';
export const dateFormatNoDayAndMonthSpaces = 'D.M.YYYY';
export const tableDateFormat = 'DD.MM.YYYY';
export const timeFormat = 'H:mm';
export const datetimeFormat = 'D. M. YYYY H:mm';
export const datetimeFormatNoDayAndMonthSpaces = 'D.M.YYYY H:mm';
export const tableDatetimeFormat = 'DD.MM.YYYY HH:mm';
export const yearmonthFormat = 'M/YYYY';

export const beDateFormat = 'YYYY-MM-DD';
export const beTimeFormat = 'HH:mm';
export const beDatetimeFormat = 'YYYY-MM-DDTHH:mm';
export const beYearmonthFormat = 'YYYY-MM';

export type DateTimeFormat =
    | 'date'
    | 'time'
    | 'datetime'
    | 'yearmonth'
    | 'tableDate'
    | 'tableDatetime'
    | 'dateNoDayAndMonthSpaces'
    | 'datetimeNoDayAndMonthSpaces'
    ;

export const reformatDate = (fromFormat: string | MomentBuiltinFormat, toFormat?: string, strict: boolean = true) =>
    (date = ''): Opt<string> =>
        opt(moment(date, fromFormat, strict))
            .filter(x => x.isValid())
            .map(x => x.format(toFormat));

const formatDateObjectAs = (format: string) =>
    (date: Date): Opt<string> =>
        opt(moment(date))
            .filter(x => x.isValid())
            .map(x => x.format(format));

const createDateObjectFrom = (format: string | MomentBuiltinFormat) =>
    (date = ''): Opt<Date> =>
        opt(moment(date, format, true))
            .filter(x => x.isValid())
            .map(x => x.toDate());

type DateFormatter = (value?: string) => Opt<string>;
type DateObjectFormatter = (value: Date) => Opt<string>;
type DateObjectFactory = (value?: string) => Opt<Date>;

const datetimeFormatters: Record<DateTimeFormat, DateFormatter> = {
    date: reformatDate(beDateFormat, dateFormat),
    dateNoDayAndMonthSpaces: reformatDate(beDateFormat, dateFormatNoDayAndMonthSpaces),
    tableDate: reformatDate(beDateFormat, tableDateFormat),
    time: reformatDate(beTimeFormat, timeFormat),
    datetime: reformatDate(ISO_8601, datetimeFormat),
    datetimeNoDayAndMonthSpaces: reformatDate(ISO_8601, datetimeFormatNoDayAndMonthSpaces),
    tableDatetime: reformatDate(ISO_8601, tableDatetimeFormat),
    yearmonth: reformatDate(beYearmonthFormat, yearmonthFormat),
};

const datetimeBeFormatters: Record<DateTimeFormat, DateFormatter> = {
    date: reformatDate(dateFormat, beDateFormat),
    dateNoDayAndMonthSpaces: reformatDate(dateFormatNoDayAndMonthSpaces, beDateFormat),
    tableDate: reformatDate(tableDateFormat, beDateFormat),
    time: reformatDate(timeFormat, beTimeFormat),
    datetime: reformatDate(datetimeFormat, beDatetimeFormat),
    datetimeNoDayAndMonthSpaces: reformatDate(datetimeFormatNoDayAndMonthSpaces, beDatetimeFormat),
    tableDatetime: reformatDate(tableDatetimeFormat, beDatetimeFormat),
    yearmonth: reformatDate(yearmonthFormat, beYearmonthFormat),
};

const dateIsoFormatters: Record<DateTimeFormat, DateFormatter> = {
    date: reformatDate(ISO_8601, beDateFormat),
    dateNoDayAndMonthSpaces: reformatDate(ISO_8601, beDateFormat),
    tableDate: reformatDate(ISO_8601, beDateFormat),
    time: reformatDate(ISO_8601, beTimeFormat),
    datetime: reformatDate(ISO_8601, beDatetimeFormat),
    datetimeNoDayAndMonthSpaces: reformatDate(ISO_8601, beDatetimeFormat),
    tableDatetime: reformatDate(ISO_8601, beDatetimeFormat),
    yearmonth: reformatDate(ISO_8601, beYearmonthFormat),
};

const dateAsIsoFormatters: Record<DateTimeFormat, DateFormatter> = {
    date: reformatDate(beDateFormat),
    dateNoDayAndMonthSpaces: reformatDate(beDateFormat),
    tableDate: reformatDate(beDateFormat),
    time: reformatDate(beTimeFormat),
    datetime: reformatDate(ISO_8601),
    datetimeNoDayAndMonthSpaces: reformatDate(ISO_8601),
    tableDatetime: reformatDate(ISO_8601),
    yearmonth: reformatDate(beYearmonthFormat),
};

const dateObjectFormatters: Record<DateTimeFormat, DateObjectFormatter> = {
    date: formatDateObjectAs(beDateFormat),
    dateNoDayAndMonthSpaces: formatDateObjectAs(beDateFormat),
    tableDate: formatDateObjectAs(beDateFormat),
    time: formatDateObjectAs(beTimeFormat),
    datetime: formatDateObjectAs(beDatetimeFormat),
    datetimeNoDayAndMonthSpaces: formatDateObjectAs(beDatetimeFormat),
    tableDatetime: formatDateObjectAs(beDatetimeFormat),
    yearmonth: formatDateObjectAs(beYearmonthFormat),
};

const dateObjectFactories: Record<DateTimeFormat, DateObjectFactory> = {
    date: createDateObjectFrom(beDateFormat),
    dateNoDayAndMonthSpaces: createDateObjectFrom(beDateFormat),
    tableDate: createDateObjectFrom(beDateFormat),
    time: createDateObjectFrom(beTimeFormat),
    datetime: createDateObjectFrom(ISO_8601),
    datetimeNoDayAndMonthSpaces: createDateObjectFrom(ISO_8601),
    tableDatetime: createDateObjectFrom(ISO_8601),
    yearmonth: createDateObjectFrom(beYearmonthFormat),
};

// formatting of BE formats for display
export const formatDate = datetimeFormatters.date;
export const formatTableDate = datetimeFormatters.tableDate;
export const formatTime = datetimeFormatters.time;
export const formatDateTime = datetimeFormatters.datetime;
export const formatTableDateTime = datetimeFormatters.tableDatetime;
export const formatYearMonth = datetimeFormatters.yearmonth;
export const formatDateNoDayAndMonthSpace = datetimeFormatters.dateNoDayAndMonthSpaces;
export const formatDatetimeNoDayAndMonthSpace = datetimeFormatters.datetimeNoDayAndMonthSpaces;

// conversion between BE and display formats
export const formatDatetime = (format: DateTimeFormat): DateFormatter => datetimeFormatters[format];
export const formatDatetimeForBe = (format: DateTimeFormat): DateFormatter => datetimeBeFormatters[format];

// conversion between BE formats and iso format
export const formatDatetimeIso = (format: DateTimeFormat): DateFormatter => dateIsoFormatters[format];
export const formatAsDatetimeIso = (format: DateTimeFormat): DateFormatter => dateAsIsoFormatters[format];

// conversion between BE formats and Date object
export const formatDateObject = (format: DateTimeFormat): DateObjectFormatter => dateObjectFormatters[format];
export const createDateObject = (format: DateTimeFormat): DateObjectFactory => dateObjectFactories[format];

export const todayDate = (): string => moment().format(beDateFormat);
