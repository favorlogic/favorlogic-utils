import {replace} from 'fp-ts/string';
import {isEmpty, join, map, reverse, split, reject, repeat, isUndefined} from 'lodash/fp';
import {pipe, flow, optInfinity} from 'ts-opt';

import {chunksOf, padEnd} from '../basic';

const digitsInThousand = 3;

export const separateThousandsOfIntPart = (int: string = '', thousandsSeparator: string): string =>
    pipe(
        int,
        split(''),
        reverse,
        chunksOf(digitsInThousand),
        map(flow(reverse, join(''))),
        reverse,
        join(thousandsSeparator),
    );

const createRequiredNumberOfDecPart = (dec: string | undefined = '', decimalPlaces: number): string => isEmpty(dec)
    ? repeat(decimalPlaces)('0')
    : padEnd('0')(decimalPlaces - dec.length)(dec);

const mergeIntAndDecStrings =
    (decimalPlaces: number | undefined, thousandsSeparator: string, decimalSeparator: string) =>
        ([int, dec]: Array<string>): string => {
            const intPart = separateThousandsOfIntPart(int, thousandsSeparator);

            if (isUndefined(decimalPlaces)) {
                if (!dec) return intPart;

                return [intPart, dec].join(decimalSeparator);
            }

            return decimalPlaces < 1
                ? intPart
                : `${intPart}${decimalSeparator}${createRequiredNumberOfDecPart(dec, decimalPlaces)}`;
        };

/**
 * Formats numbers with a specified thousands separator and decimal separator.
 * The function returns other functions to set the number and requested decimal places
 * and then format a specific number.
 *
 * @param {string} thousandsSeparator - The separator for thousands (e.g., "," for "1,000").
 * @param {string} decimalSeparator - The separator between the integer and decimal part (e.g., "." for "123.45").
 * @returns {(decimalPlaces?: number) => (num: number) => string} A function that formats a number according
 * to the provided parameters.
 *
 * @param {number} [decimalPlaces] - The number of decimal places (optional). If not provided,
 * the original decimal part of the number is used.
 * @param {number} num - The number to be formatted.
 * @throws {Error} If the number is `NaN` or infinite (`Infinity` or `-Infinity`), it throws an error.
 * @returns {string} A formatted string representing the number with thousands and decimal separators.
 *
 * @example
 * const formatCurrency = formatNumberGeneric(',', '.');
 * formatCurrency(2)(123456.789) => "123,456.79"
 * formatCurrency(0)(123456.789) => "123,457"
 *
 * @example
 * const formatNumberCs = formatNumberGeneric(' ', ',');
 * formatNumberCs(2)(123456.789) => "123 456,79"
 */
export const formatNumberGeneric =
    (thousandsSeparator: string, decimalSeparator: string) =>
        (decimalPlaces?: number) =>
            (num: number): string | null => optInfinity(num).mapFlow(
                x => !isUndefined(decimalPlaces) ? x.toFixed(decimalPlaces) : x.toString(),
                replace('.', decimalSeparator),
                split(decimalSeparator),
                mergeIntAndDecStrings(decimalPlaces, thousandsSeparator, decimalSeparator),
                reject(isEmpty),
                join(''),
            ).orNull();

/**
 * @param {number} [decimalPlaces] - The number of decimal places (optional). If not provided,
 * the original decimal part of the number is used.
 * @example
 * formatNumberCs()(123.456) => '123.456'
 * formatNumberCs(3)(1234567) => '1 234 567,000'
 * formatNumberCs(0)(1234567.75) => '1 234 568'
 */
export const formatNumberCs = formatNumberGeneric(' ', ',');

export const formatNumberEn = formatNumberGeneric(',', '.');
