import {formatTemperatureCs, formatTemperatureEn} from './format-temperature';

describe('formatTemperatureCs', () => {
    it('integer', () => {
        expect(formatTemperatureCs(23)).toEqual('23 °C');
    });

    it('decimal', () => {
        expect(formatTemperatureCs(23.5)).toEqual('23,5 °C');
    });
});

describe('formatTemperatureEn', () => {
    it('integer', () => {
        expect(formatTemperatureEn(23)).toEqual('23 °C');
    });

    it('decimal', () => {
        expect(formatTemperatureEn(23.5)).toEqual('23.5 °C');
    });
});
