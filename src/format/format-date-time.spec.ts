import {set, reset} from 'mockdate';

import {
    formatDatetime,
    formatAsDatetimeIso,
    formatDatetimeForBe,
    formatDatetimeIso,
    todayDate,
    formatDateObject,
    createDateObject,
    reformatDate,
    beDatetimeFormat,
    dateFormat,
} from './format-date-time';

describe('formatDateTimeUtils', () => {
    beforeAll(() => set('2019-10-24T04:20'));
    afterAll(() => reset());

    it('todayDate', () => {
        expect(todayDate()).toEqual('2019-10-24');
    });

    it('formatDatetime', () => {
        expect(formatDatetime('date')().orNull()).toEqual(null);
        expect(formatDatetime('date')('1999-12-14').orNull()).toEqual('14. 12. 1999');
        expect(formatDatetime('date')('x').orNull()).toEqual(null);
        expect(formatDatetime('tableDate')().orNull()).toEqual(null);
        expect(formatDatetime('tableDate')('1999-09-03').orNull()).toEqual('03.09.1999');
        expect(formatDatetime('tableDate')('x').orNull()).toEqual(null);
        expect(formatDatetime('time')().orNull()).toEqual(null);
        expect(formatDatetime('time')('12:30').orNull()).toEqual('12:30');
        expect(formatDatetime('time')('x').orNull()).toEqual(null);
        expect(formatDatetime('datetime')().orNull()).toEqual(null);
        expect(formatDatetime('datetime')('1999-12-14T16:00').orNull()).toEqual('14. 12. 1999 16:00');
        expect(formatDatetime('datetime')('x').orNull()).toEqual(null);
        expect(formatDatetime('yearmonth')().orNull()).toEqual(null);
        expect(formatDatetime('yearmonth')('1999-12').orNull()).toEqual('12/1999');
        expect(formatDatetime('yearmonth')('x').orNull()).toEqual(null);
        expect(formatDatetime('yearmonth')('x').orNull()).toEqual(null);
        expect(formatDatetime('datetimeNoDayAndMonthSpaces')('1999-12-14T16:00').orNull())
            .toEqual('14.12.1999 16:00');
        expect(formatDatetime('datetimeNoDayAndMonthSpaces')().orNull()).toEqual(null);
        expect(formatDatetime('datetimeNoDayAndMonthSpaces')('x').orNull()).toEqual(null);
        expect(formatDatetime('dateNoDayAndMonthSpaces')().orNull()).toEqual(null);
        expect(formatDatetime('dateNoDayAndMonthSpaces')('1999-12-14').orNull()).toEqual('14.12.1999');
        expect(formatDatetime('dateNoDayAndMonthSpaces')('x').orNull()).toEqual(null);
    });

    it('formatDatetimeForBe', () => {
        expect(formatDatetimeForBe('date')().orNull()).toEqual(null);
        expect(formatDatetimeForBe('date')('14. 12. 1999').orNull()).toEqual('1999-12-14');
        expect(formatDatetimeForBe('date')('x').orNull()).toEqual(null);
        expect(formatDatetimeForBe('tableDate')().orNull()).toEqual(null);
        expect(formatDatetimeForBe('tableDate')('03.08.1999').orNull()).toEqual('1999-08-03');
        expect(formatDatetimeForBe('tableDate')('x').orNull()).toEqual(null);
        expect(formatDatetimeForBe('time')().orNull()).toEqual(null);
        expect(formatDatetimeForBe('time')('12:30').orNull()).toEqual('12:30');
        expect(formatDatetimeForBe('time')('x').orNull()).toEqual(null);
        expect(formatDatetimeForBe('datetime')().orNull()).toEqual(null);
        expect(formatDatetimeForBe('datetime')('14. 12. 1999 16:00').orNull()).toEqual('1999-12-14T16:00');
        expect(formatDatetimeForBe('datetime')('x').orNull()).toEqual(null);
        expect(formatDatetimeForBe('yearmonth')().orNull()).toEqual(null);
        expect(formatDatetimeForBe('yearmonth')('12/1999').orNull()).toEqual('1999-12');
        expect(formatDatetimeForBe('yearmonth')('x').orNull()).toEqual(null);
        expect(formatDatetimeForBe('datetimeNoDayAndMonthSpaces')('14.12.1999 16:00').orNull())
            .toEqual('1999-12-14T16:00');
        expect(formatDatetimeForBe('datetimeNoDayAndMonthSpaces')().orNull()).toEqual(null);
        expect(formatDatetimeForBe('datetimeNoDayAndMonthSpaces')('x').orNull()).toEqual(null);
        expect(formatDatetimeForBe('dateNoDayAndMonthSpaces')().orNull()).toEqual(null);
        expect(formatDatetimeForBe('dateNoDayAndMonthSpaces')('14.12.1999').orNull()).toEqual('1999-12-14');
        expect(formatDatetimeForBe('dateNoDayAndMonthSpaces')('x').orNull()).toEqual(null);
    });

    it('formatDatetimeIso', () => {
        expect(formatDatetimeIso('date')().orNull()).toEqual(null);
        expect(formatDatetimeIso('date')('1999-12-31T14:00:00+04:00').orNull()).toEqual('1999-12-31');
        expect(formatDatetimeIso('date')('x').orNull()).toEqual(null);
        expect(formatDatetimeIso('tableDate')().orNull()).toEqual(null);
        expect(formatDatetimeIso('tableDate')('1999-12-31T14:00:00+04:00').orNull()).toEqual('1999-12-31');
        expect(formatDatetimeIso('tableDate')('x').orNull()).toEqual(null);
        expect(formatDatetimeIso('time')().orNull()).toEqual(null);
        expect(formatDatetimeIso('time')('1999-12-31T14:00:00+04:00').orNull()).toEqual('14:00');
        expect(formatDatetimeIso('time')('x').orNull()).toEqual(null);
        expect(formatDatetimeIso('datetime')().orNull()).toEqual(null);
        expect(formatDatetimeIso('datetime')('1999-12-31T14:00:00+04:00').orNull()).toEqual('1999-12-31T14:00');
        expect(formatDatetimeIso('datetime')('x').orNull()).toEqual(null);
        expect(formatDatetimeIso('yearmonth')().orNull()).toEqual(null);
        expect(formatDatetimeIso('yearmonth')('1999-12-31T14:00:00+04:00').orNull()).toEqual('1999-12');
        expect(formatDatetimeIso('yearmonth')('x').orNull()).toEqual(null);
        expect(formatDatetimeIso('datetimeNoDayAndMonthSpaces')('1999-12-31T14:00:00+04:00').orNull())
            .toEqual('1999-12-31T14:00');
        expect(formatDatetimeIso('datetimeNoDayAndMonthSpaces')().orNull()).toEqual(null);
        expect(formatDatetimeIso('datetimeNoDayAndMonthSpaces')('x').orNull()).toEqual(null);
        expect(formatDatetimeIso('dateNoDayAndMonthSpaces')().orNull()).toEqual(null);
        expect(formatDatetimeIso('dateNoDayAndMonthSpaces')('1999-12-31T14:00:00+04:00').orNull())
            .toEqual('1999-12-31');
        expect(formatDatetimeIso('dateNoDayAndMonthSpaces')('x').orNull()).toEqual(null);
    });

    it('formatASDatetimeIso', () => {
        expect(formatAsDatetimeIso('date')().orNull()).toEqual(null);
        expect(formatAsDatetimeIso('date')('1999-12-31').orNull()).toEqual('1999-12-31T00:00:00+04:00');
        expect(formatAsDatetimeIso('date')('x').orNull()).toEqual(null);
        expect(formatAsDatetimeIso('tableDate')().orNull()).toEqual(null);
        expect(formatAsDatetimeIso('tableDate')('1999-12-31').orNull()).toEqual('1999-12-31T00:00:00+04:00');
        expect(formatAsDatetimeIso('tableDate')('x').orNull()).toEqual(null);
        expect(formatAsDatetimeIso('time')().orNull()).toEqual(null);
        expect(formatAsDatetimeIso('time')('14:00').orNull()).toEqual('2019-10-24T14:00:00+04:00');
        expect(formatAsDatetimeIso('time')('x').orNull()).toEqual(null);
        expect(formatAsDatetimeIso('datetime')().orNull()).toEqual(null);
        expect(formatAsDatetimeIso('datetime')('1999-12-31T14:00').orNull()).toEqual('1999-12-31T14:00:00+04:00');
        expect(formatAsDatetimeIso('datetime')('x').orNull()).toEqual(null);
        expect(formatAsDatetimeIso('yearmonth')().orNull()).toEqual(null);
        expect(formatAsDatetimeIso('yearmonth')('1999-12').orNull()).toEqual('1999-12-01T00:00:00+04:00');
        expect(formatAsDatetimeIso('yearmonth')('x').orNull()).toEqual(null);
        expect(formatAsDatetimeIso('datetimeNoDayAndMonthSpaces')('1999-12-31T14:00').orNull())
            .toEqual('1999-12-31T14:00:00+04:00');
        expect(formatAsDatetimeIso('datetimeNoDayAndMonthSpaces')().orNull()).toEqual(null);
        expect(formatAsDatetimeIso('datetimeNoDayAndMonthSpaces')('x').orNull()).toEqual(null);
        expect(formatAsDatetimeIso('dateNoDayAndMonthSpaces')().orNull()).toEqual(null);
        expect(formatAsDatetimeIso('dateNoDayAndMonthSpaces')('1999-12-31').orNull())
            .toEqual('1999-12-31T00:00:00+04:00');
        expect(formatAsDatetimeIso('dateNoDayAndMonthSpaces')('x').orNull()).toEqual(null);
    });

    it('formatDateObject', () => {
        const date = new Date('1999-12-31T10:00:00.000');
        const invalidDate = new Date('x');
        expect(formatDateObject('date')(date).orNull()).toEqual('1999-12-31');
        expect(formatDateObject('date')(invalidDate).orNull()).toEqual(null);
        expect(formatDateObject('tableDate')(date).orNull()).toEqual('1999-12-31');
        expect(formatDateObject('tableDate')(invalidDate).orNull()).toEqual(null);
        expect(formatDateObject('time')(date).orNull()).toEqual('10:00');
        expect(formatDateObject('time')(invalidDate).orNull()).toEqual(null);
        expect(formatDateObject('datetime')(date).orNull()).toEqual('1999-12-31T10:00');
        expect(formatDateObject('datetime')(invalidDate).orNull()).toEqual(null);
        expect(formatDateObject('yearmonth')(date).orNull()).toEqual('1999-12');
        expect(formatDateObject('yearmonth')(invalidDate).orNull()).toEqual(null);
        expect(formatDateObject('datetimeNoDayAndMonthSpaces')(date).orNull()).toEqual('1999-12-31T10:00');
        expect(formatDateObject('datetimeNoDayAndMonthSpaces')(invalidDate).orNull()).toEqual(null);
        expect(formatDateObject('dateNoDayAndMonthSpaces')(date).orNull()).toEqual('1999-12-31');
        expect(formatDateObject('dateNoDayAndMonthSpaces')(invalidDate).orNull()).toEqual(null);
    });

    it('createDateObject', () => {
        expect(createDateObject('date')().orNull()).toEqual(null);
        expect(createDateObject('date')('1999-12-31').orNull()).toEqual(new Date('1999-12-31T00:00:00.000'));
        expect(createDateObject('date')('x').orNull()).toEqual(null);
        expect(createDateObject('tableDate')().orNull()).toEqual(null);
        expect(createDateObject('tableDate')('1999-12-31').orNull()).toEqual(new Date('1999-12-31T00:00:00.000'));
        expect(createDateObject('tableDate')('x').orNull()).toEqual(null);
        expect(createDateObject('time')().orNull()).toEqual(null);
        expect(createDateObject('time')('14:00').orNull()).toEqual(new Date('2019-10-24T14:00:00.000'));
        expect(createDateObject('time')('x').orNull()).toEqual(null);
        expect(createDateObject('datetime')().orNull()).toEqual(null);
        expect(createDateObject('datetime')('1999-12-31T14:00').orNull()).toEqual(new Date('1999-12-31T14:00:00.000'));
        expect(createDateObject('datetime')('x').orNull()).toEqual(null);
        expect(createDateObject('yearmonth')().orNull()).toEqual(null);
        expect(createDateObject('yearmonth')('1999-12').orNull()).toEqual(new Date('1999-12-01T00:00:00.000'));
        expect(createDateObject('yearmonth')('x').orNull()).toEqual(null);
        expect(createDateObject('datetimeNoDayAndMonthSpaces')().orNull()).toEqual(null);
        expect(createDateObject('datetimeNoDayAndMonthSpaces')('1999-12-31T14:00').orNull())
            .toEqual(new Date('1999-12-31T14:00:00.000'));
        expect(createDateObject('datetimeNoDayAndMonthSpaces')('x').orNull()).toEqual(null);
        expect(createDateObject('dateNoDayAndMonthSpaces')().orNull()).toEqual(null);
        expect(createDateObject('dateNoDayAndMonthSpaces')('1999-12-31').orNull())
            .toEqual(new Date('1999-12-31T00:00:00.000'));
        expect(createDateObject('dateNoDayAndMonthSpaces')('x').orNull()).toEqual(null);
    });

    it('reformat date', () => {
        expect(
            reformatDate(beDatetimeFormat, dateFormat, false)('2022-08-01T09:22:22.869707').orElse(''),
        ).toEqual('1. 8. 2022');
        expect(reformatDate(beDatetimeFormat, dateFormat)('2022-08-01T09:22:22.869707').orElse('')).toBe('');
    });
});
