import {flow, filter, last, isEmpty} from 'lodash/fp';

interface Unit {
    value: number;
    label: string;
    integer?: boolean;
}

const KiB = 1024;

const Byte: Unit = {
    value: 1,
    label: 'B',
    integer: true,
};

const units: Array<Unit> = [
    Byte,
    {
        value: KiB,
        label: 'KiB',
    },
    {
        value: KiB * KiB,
        label: 'MiB',
    },
    {
        value: KiB * KiB * KiB,
        label: 'GiB',
    },
    {
        value: KiB * KiB * KiB * KiB,
        label: 'TiB',
    },
];

export const formatSize = (bytes: number): string => {
    const format = (unit: Unit): string => {
        const dividedBySize = bytes / unit.value;
        const numPart = unit.integer ? dividedBySize : dividedBySize.toFixed(1);

        return `${numPart} ${unit.label}`;
    };

    return flow([
        filter<Unit>(x => x.value <= bytes),
        (x: Array<Unit>): Array<Unit> => isEmpty(x) ? [Byte] : x,
        last,
        format,
    ])(units);
};
