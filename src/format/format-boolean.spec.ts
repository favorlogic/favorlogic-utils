import {formatBoolYesNo} from './format-boolean';

describe('formatBoolean', () => {
    it('Format yes and no', () => {
        expect(formatBoolYesNo(true)).toEqual('Ano');
        expect(formatBoolYesNo(false)).toEqual('Ne');
    });
});
