import {formatFloat} from './format-float';

const testFloat1 = 0.4;
const testFloat2 = 1.111;

describe('formatFloat', () => {
    it('format', () => {
        expect(formatFloat(0)).toEqual('0');
        expect(formatFloat(testFloat1)).toEqual('0,4');
        expect(formatFloat(testFloat2)).toEqual('1,111');
    });
});
