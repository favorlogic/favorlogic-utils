import {formatCurrencyCzk} from './format-currency-czk';

const f = formatCurrencyCzk;

describe('formatCurrency', () => {
    it('zero', () => {
        expect(f(0)).toEqual('0 Kč');
    });

    it('simple', () => {
        expect(f(123)).toEqual('123 Kč');
        expect(f(0)).toEqual('0 Kč');
    });

    it('thousands', () => {
        expect(f(1000)).toEqual('1 000 Kč');
        expect(f(1234567)).toEqual('1 234 567 Kč');
    });

    it('decimal', () => {
        expect(f(1.5)).toEqual('1,50 Kč');
        expect(f(10.75)).toEqual('10,75 Kč');
        expect(f(0.00)).toEqual('0 Kč');
    });

    it('all', () => {
        expect(f(1001.5)).toEqual('1 001,50 Kč');
    });

    it('not accept NaN, Infinity', () => {
        expect(f(NaN)).toBeNull();
        expect(f(Infinity)).toBeNull();
        expect(f(-Infinity)).toBeNull();
    });
});
