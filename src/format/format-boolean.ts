export const formatBoolYesNo = (x: boolean): string => x ? 'Ano' : 'Ne';
