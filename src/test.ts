import {random} from 'lodash/fp';

export const buildTestSelector = (key: string, prefix?: string): string =>
    prefix ? `[data-test-id="${prefix}-${key}"]` : `[data-test-id="${key}"]`;

const max = 10000;

export const getRandomId = (): number => random(1, max);
