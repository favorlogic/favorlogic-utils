import {useParamFromRoute} from './use-param-from-route';

jest.mock('react-router', () => ({
    ...jest.requireActual('react-router'),
    useParams: () => ({
        param1: 'hello',
        param2: '',
    }),
}));

describe('useParamFromRoute', () => {
    it('should throw error', () => {
        expect(() => useParamFromRoute()).toThrow('Invalid param in route');
        expect(() => useParamFromRoute('param2')).toThrow('Invalid param in route');
        expect(() => useParamFromRoute('param3')).toThrow('Invalid param in route');
    });
    it('should return value of param', () => {
        expect(useParamFromRoute('param1')).toBe('hello');
    });
});
