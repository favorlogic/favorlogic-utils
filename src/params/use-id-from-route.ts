import {useParams} from 'react-router';
import {optEmptyString, parseInt} from 'ts-opt';

export const useIdFromRoute = (key: string = 'id'): number => {
    const params = useParams<Record<string, string>>();

    return optEmptyString(params[key]).chain(parseInt).orCrash('Invalid id in route');
};

export const useIdFromRouteOrNull = (key: string = 'id'): number | null => {
    const params = useParams<Record<string, string>>();

    return optEmptyString(params[key]).chain(parseInt).orNull();
};
