import {useIdFromRoute, useIdFromRouteOrNull} from './use-id-from-route';

jest.mock('react-router', () => ({
    ...jest.requireActual('react-router'),
    useParams: () => ({
        param1: '1',
        param2: '',
    }),
}));

describe('useIdFromRoute', () => {
    it('should throw error', () => {
        expect(() => useIdFromRoute()).toThrow('Invalid id in route');
        expect(() => useIdFromRoute('param2')).toThrow('Invalid id in route');
        expect(() => useIdFromRoute('param3')).toThrow('Invalid id in route');
    });
    it('should return value of param', () => {
        expect(useIdFromRoute('param1')).toBe(1);
    });
});

describe('useIdFromRouteOrNull', () => {
    it('should return null', () => {
        expect(useIdFromRouteOrNull()).toBeNull();
        expect(useIdFromRouteOrNull('param2')).toBeNull();
        expect(useIdFromRouteOrNull('param3')).toBeNull();
    });
    it('should return value of param', () => {
        expect(useIdFromRouteOrNull('param1')).toBe(1);
    });
});
