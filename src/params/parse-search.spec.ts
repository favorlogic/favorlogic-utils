import {parseSearch, splitSearchToStateAndRest} from './parse-search';

describe('parseSearch', () => {
    describe('parseSearch', () => {
        it('returns correctly parsed query', () => {
            expect(parseSearch(''))
                .toEqual({});
            expect(parseSearch('?'))
                .toEqual({});
            expect(parseSearch('?a=abc'))
                .toEqual({a: 'abc'});
            expect(parseSearch('?a=1&b=b&c[]=3&c[]=4&d=5&e[]=e6&e[]=e7&f=f'))
                .toEqual({a: '1', b: 'b', c: ['3', '4'], d: '5', e: ['e6', 'e7'], f: 'f'});
        });
    });

    describe('splitSearchToStateAndRest', () => {
        it('returns correctly splitted ParsedQuery', () => {
            const search = '?a=1&b=b&c[]=3&c[]=4&d=5&e[]=e6&e[]=e7&f=f';
            expect(splitSearchToStateAndRest(search, []))
                .toEqual([{}, {a: '1', b: 'b', c: ['3', '4'], d: '5', e: ['e6', 'e7'], f: 'f'}]);
            expect(splitSearchToStateAndRest(search, ['a', 'b', 'c']))
                .toEqual([{a: '1', b: 'b', c: ['3', '4']}, {d: '5', e: ['e6', 'e7'], f: 'f'}]);
            expect(splitSearchToStateAndRest(search, ['a', 'c', 'e']))
                .toEqual([{a: '1', c: ['3', '4'], e: ['e6', 'e7']}, {b: 'b', d: '5', f: 'f'}]);
            expect(splitSearchToStateAndRest(search, ['a', 'b', 'c', 'd', 'e', 'f']))
                .toEqual([{a: '1', b: 'b', c: ['3', '4'], d: '5', e: ['e6', 'e7'], f: 'f'}, {}]);
        });
    });
});
