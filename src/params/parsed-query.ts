export interface ParsedQuery {
    [key: string]: string | Array<string> | null | undefined;
}
