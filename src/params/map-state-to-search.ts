import {stringify} from 'query-string';

import {removeNullFieldsRecursively} from '../basic';

type Value = string | number | boolean;

export interface StateData {
    [key: string]: Value | Array<Value | null | undefined> | null | undefined;
}

export type ArrayFormat = 'bracket' | 'index' | 'none' | 'comma';

interface Options {
    arrayFormat?: ArrayFormat;
    removeNullFields?: boolean;
}

const serializeArrays = (data: StateData): StateData => {
    const dataCopy: StateData = {...data};

    Object.keys(dataCopy)
        .forEach(key => {
            const value = dataCopy[key];

            if (Array.isArray(value)) {
                dataCopy[key] = value.filter(x => x !== undefined).join(',');
            }
        });

    return dataCopy;
};

export const mapStateToSearch = (
    state: StateData,
    {arrayFormat, removeNullFields}: Options = {},
): string => {
    const data = removeNullFields
        ? removeNullFieldsRecursively(state) as StateData
        : state;

    if (arrayFormat === 'comma') {
        return stringify(serializeArrays(data), {arrayFormat: 'none'});
    }

    return stringify(data, {arrayFormat: arrayFormat ?? 'bracket'});
};

