import {ParseOptions, parse} from 'query-string';

import {ParsedQuery} from './parsed-query';

export const parseSearch = (search: string, options: ParseOptions = {arrayFormat: 'bracket'}): ParsedQuery =>
    parse(search, options);

export const splitSearchToStateAndRest = (search: string, stateKeys: Array<string>): [ParsedQuery, ParsedQuery] => {
    const searchState: ParsedQuery = {};
    const searchRest: ParsedQuery = {};
    const ps = parseSearch(search);
    Object.keys(ps).forEach(key => {
        if (stateKeys.includes(key)) {
            searchState[key] = ps[key];
        } else {
            searchRest[key] = ps[key];
        }
    });

    return [searchState, searchRest];
};
