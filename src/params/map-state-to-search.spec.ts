import {mapStateToSearch, StateData} from './map-state-to-search';

describe('mapStateToSearch', () => {
    const data: StateData = {
        paramA: 'testValue',
        paramB: ['value1', 'value2', undefined, null],
        paramC: null,
        paramD: undefined,
    };

    describe('when asked to serialize with no array format specified', () => {
        it('should serialize params with none array format', () => {
            expect(mapStateToSearch(data))
                .toEqual('paramA=testValue&paramB[]=value1&paramB[]=value2&paramB&paramC');
        });

        describe('and asked to remove null fields', () => {
            it('should remove null fields', () => {
                expect(mapStateToSearch(data, {removeNullFields: true}))
                    .toEqual('paramA=testValue&paramB[]=value1&paramB[]=value2');
            });
        });
    });

    describe('when asked to serialize with bracket array format', () => {
        it('should serialize params with bracket array format', () => {
            expect(mapStateToSearch(data, {arrayFormat: 'bracket'}))
                .toEqual('paramA=testValue&paramB[]=value1&paramB[]=value2&paramB&paramC');
        });

        describe('and asked to remove null fields', () => {
            it('should remove null fields', () => {
                expect(mapStateToSearch(data, {arrayFormat: 'bracket', removeNullFields: true}))
                    .toEqual('paramA=testValue&paramB[]=value1&paramB[]=value2');
            });
        });
    });

    describe('when asked to serialize with index array format', () => {
        it('should serialize params with index array format', () => {
            expect(mapStateToSearch(data, {arrayFormat: 'index'}))
                .toEqual('paramA=testValue&paramB[0]=value1&paramB[1]=value2&paramB[2]&paramC');
        });

        describe('and asked to remove null fields', () => {
            it('should remove null fields', () => {
                expect(mapStateToSearch(data, {arrayFormat: 'index', removeNullFields: true}))
                    .toEqual('paramA=testValue&paramB[0]=value1&paramB[1]=value2');
            });
        });
    });

    describe('when asked to serialize with none array format', () => {
        it('should serialize params with none array format', () => {
            expect(mapStateToSearch(data, {arrayFormat: 'none'}))
                .toEqual('paramA=testValue&paramB=value1&paramB=value2&paramB&paramC');
        });

        describe('and asked to remove null fields', () => {
            it('should remove null fields', () => {
                expect(mapStateToSearch(data, {arrayFormat: 'none', removeNullFields: true}))
                    .toEqual('paramA=testValue&paramB=value1&paramB=value2');
            });
        });
    });

    describe('when asked to serialize with comma array format', () => {
        it('should serialize params with commma array format', () => {
            expect(mapStateToSearch(data, {arrayFormat: 'comma'}))
                .toEqual('paramA=testValue&paramB=value1%2Cvalue2%2C&paramC');
        });

        describe('and asked to remove null fields', () => {
            it('should remove null fields', () => {
                expect(mapStateToSearch(data, {arrayFormat: 'comma', removeNullFields: true}))
                    .toEqual('paramA=testValue&paramB=value1%2Cvalue2');
            });
        });
    });
});
