import {useParams} from 'react-router';
import {optEmptyString} from 'ts-opt';

export const useParamFromRoute = (key: string = 'id'): string => {
    const params = useParams<Record<string, string>>();

    return optEmptyString(params[key]).orCrash('Invalid param in route');
};
