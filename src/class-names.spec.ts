import {classNames} from './class-names';

describe('classNames', () => {
    const f = classNames;

    it('empty', () => {
        expect(f()).toEqual('');
        expect(f(null)).toEqual('');
        expect(f([])).toEqual('');
    });

    it('one', () => {
        expect(f('a')).toEqual('a');
        expect(f(['a'])).toEqual('a');
    });

    it('multiple', () => {
        expect(f('a', 'b')).toEqual('a b');
        expect(f('a', null, 'b')).toEqual('a b');
        expect(f('a', undefined, 'b', null)).toEqual('a b');
    });

    it('nested', () => {
        expect(f(['a'], null, [null], ['b', 'c'], undefined)).toEqual('a b c');
    });
});
