/* eslint-disable no-console */
import {Logger} from './logger';

describe('Logger', () => {
    let logger: Logger;

    beforeEach(() => {
        jest.spyOn(console, 'log').mockReturnValue();
        jest.spyOn(console, 'warn').mockReturnValue();
        jest.spyOn(console, 'error').mockReturnValue();
        jest.clearAllMocks();
    });

    describe('given dev mode', () => {
        beforeEach(() => {
            logger = new Logger({devMode: true});
        });

        describe('when asked to log', () => {
            it('should write log', () => {
                logger.log('test message');
                expect(console.log).toBeCalledWith('test message');
                expect(console.log).toBeCalledTimes(1);
            });
        });

        describe('when asked to warn', () => {
            it('should write warning', () => {
                logger.warn('warn message');
                expect(console.warn).toBeCalledWith('warn message');
                expect(console.warn).toBeCalledTimes(1);
            });
        });

        describe('when asked to error', () => {
            it('should write error', () => {
                logger.error('error message');
                expect(console.error).toBeCalledWith('error message');
                expect(console.error).toBeCalledTimes(1);
            });
        });
    });

    describe('given no dev mode', () => {
        beforeEach(() => {
            logger = new Logger({devMode: false});
        });

        describe('when asked to log', () => {
            it('should not write log', () => {
                logger.log('test message');
                expect(console.log).not.toBeCalled();
            });
        });

        describe('when asked to warn', () => {
            it('should not write warning', () => {
                logger.warn('warn message');
                expect(console.warn).not.toBeCalled();
            });
        });

        describe('when asked to error', () => {
            it('should not write error', () => {
                logger.error('error message');
                expect(console.error).not.toBeCalled();
            });
        });
    });
});
