import axios, {AxiosError, AxiosInstance, AxiosResponse} from 'axios';
import {noop} from 'lodash/fp';
import {StatusCodes, ReasonPhrases} from 'http-status-codes';
import * as t from 'io-ts';

import {AxiosRequest} from './axios/request-builder';
import {ResponseError} from './error';
import {Logger} from '../logger';
import {Requester} from './requester';
import {Request} from './request-types';
import {ConnectionError, ErrorResponse, InvalidRequest, InvalidResponse, SuccessResponse} from './response';
import {ValidationError} from '../validate-schema';

const ItemSchema = t.interface({
    name: t.string,
});

const InstanceSchema = t.interface({
    instance: t.string,
});

interface Item extends t.TypeOf<typeof ItemSchema> {}
interface Instance extends t.TypeOf<typeof InstanceSchema> {}

const createRequest = (): Request<void, Item> => ({
    url: '/item/1',
    method: 'get',
    requestSchema: t.void,
    responseSchema: ItemSchema,
});

const createRequestWithInstanceSchema = (): Request<void, Instance> => ({
    url: '/item/1',
    method: 'get',
    requestSchema: t.void,
    responseSchema: InstanceSchema,
});

const createRequestWithJwtRefresh = (): Request<void, Item> => ({
    url: '/public/api/jwt/refresh',
    method: 'get',
    requestSchema: t.void,
    responseSchema: ItemSchema,
});


const createSuccessResponse = (): SuccessResponse<Item> =>
    new SuccessResponse(StatusCodes.OK, {name: 'Test item'});

const createInvalidResponse = (): InvalidResponse =>
    new InvalidResponse("Validation error; source=BE;\nInvalid value undefined supplied to : { name: string }");

const createSuccessJwtResponseWithInstance = (): SuccessResponse<Instance> =>
    new SuccessResponse(StatusCodes.OK, {instance: '/public/api/jwt/refresh'});

const createSuccessResponseWithNoData = (): SuccessResponse<Array<unknown>> =>
    new SuccessResponse(StatusCodes.OK, []);

const createErrorResponse = (): ErrorResponse =>
    new ErrorResponse(StatusCodes.NOT_FOUND, []);

const createUnAuthorizedErrorResponse = (): ErrorResponse =>
    new ErrorResponse(StatusCodes.UNAUTHORIZED, []);

const createInternalErrorResponse = (): ErrorResponse =>
    new ErrorResponse(StatusCodes.INTERNAL_SERVER_ERROR, []);

const createConnectionError = (): ErrorResponse =>
    new ConnectionError('Network Error');

const createValidationError = (): ErrorResponse =>
    new InvalidResponse('Validation failed');

const createAxiosRequest = (): AxiosRequest => ({
    url: '/item/1',
    method: 'get',
    headers: {
        Authorization: "Auth token"
    },
});

const createAxiosRequestJwt = (): AxiosRequest => ({
    url: '/item/1',
    method: 'get',
});

const createAxiosResponse = (): AxiosResponse => ({
    status: StatusCodes.OK,
    statusText: ReasonPhrases.OK,
    data: {name: 'Test item'},
    headers: {},
    config: createAxiosRequest(),
});

const createAxiosResponseWithNoData = (): AxiosResponse => ({
    status: StatusCodes.OK,
    statusText: ReasonPhrases.OK,
    data: undefined,
    headers: {},
    config: createAxiosRequest(),
});

const createAxiosResponseJwtInstance = (): AxiosResponse => ({
    status: StatusCodes.OK,
    statusText: ReasonPhrases.OK,
    data: {instance: '/public/api/jwt/refresh'},
    headers: {},
    config: createAxiosRequest(),
});

const createAxiosResponseJwtRefresh = (): AxiosResponse => ({
    status: StatusCodes.NO_CONTENT,
    statusText: ReasonPhrases.OK,
    data: [],
    headers: {},
    config: createAxiosRequestJwt(),
});

const createAxiosResponseJwtRefreshWithOk = (): AxiosResponse => ({
    status: StatusCodes.OK,
    statusText: ReasonPhrases.OK,
    data: [],
    headers: {},
    config: createAxiosRequestJwt(),
});

const createAxiosErrorResponse = (): AxiosResponse => ({
    status: StatusCodes.NOT_FOUND,
    statusText: ReasonPhrases.NOT_FOUND,
    data: [],
    headers: {},
    config: createAxiosRequest(),
});

const createAxiosUnauthorizedResponse = (): AxiosResponse => ({
    status: StatusCodes.UNAUTHORIZED,
    statusText: ReasonPhrases.UNAUTHORIZED,
    data: [],
    headers: {},
    config: createAxiosRequestJwt(),
});

const createAxiosJwtRefreshErrorResponse = (): AxiosResponse => ({
    status: StatusCodes.INTERNAL_SERVER_ERROR,
    statusText: ReasonPhrases.INTERNAL_SERVER_ERROR,
    data: [],
    headers: {},
    config: createAxiosRequestJwt(),
});

const createAxiosResponseError = (): ResponseError => ({
    name: 'response error',
    message: 'error message',
    isAxiosError: true,
    response: createAxiosErrorResponse(),
    config: createAxiosRequest(),
    toJSON: () => ({}),
});

const createAxiosUnauthorizedErrorResponse = (): ResponseError => ({
    name: 'response error',
    message: 'Unauthorized access',
    isAxiosError: true,
    response: createAxiosUnauthorizedResponse(),
    config: createAxiosRequestJwt(),
    toJSON: () => ({}),
});

const createJwtRefreshErrorResponse = (): ResponseError => ({
    name: 'response error',
    message: 'Internal error',
    isAxiosError: true,
    response: createAxiosJwtRefreshErrorResponse(),
    config: createAxiosRequestJwt(),
    toJSON: () => ({}),
});

const createAxiosConnectionError = (): AxiosError => ({
    name: 'connection error',
    message: 'Network Error',
    isAxiosError: true,
    config: createAxiosRequest(),
    toJSON: () => ({}),
});

const createAxiosValidationError = (): ValidationError => ({
    message: "Validation failed",
    name: "Validation error",
    source: "BE",
});

const createUnknownError = (): Error => ({
    message: "Unknown failed",
    name: "Unknwnon error",
});

const mockAxios = (): jest.Mock => {
    const instance = jest.fn();

    jest.spyOn(axios, 'create')
        .mockReturnValue(instance as unknown as AxiosInstance);

    return instance;
};

const mockLogging = (): void => {
    jest.spyOn(Logger.prototype, 'error').mockImplementation();
    jest.spyOn(Logger.prototype, 'warn').mockImplementation();
    jest.spyOn(Logger.prototype, 'log').mockImplementation();
    jest.spyOn(console, 'error').mockImplementation();

    jest.clearAllMocks();
};

describe('Requester', () => {
    let requester: Requester;
    let requesterJwt: Requester;
    let axiosInstance: jest.Mock;
    let onUnauthorized: jest.Mock;
    let onForbidden: jest.Mock;
    let onValidationError: jest.Mock;
    let onVersionMismatch: jest.Mock;


    beforeEach(() => {
        axiosInstance = mockAxios();
        onUnauthorized = jest.fn();
        onForbidden = jest.fn();
        onValidationError = jest.fn();
        onVersionMismatch = jest.fn();
        requester = new Requester({
            baseUrl: '/api',
            devMode: true,
            onUnauthorized,
            onForbidden,
            onValidationError,
            onVersionMismatch,
            onServerError: noop,
            getAuthToken: () => "Auth token",
        });
        requesterJwt = new Requester({
            baseUrl: '/api',
            devMode: true,
            onUnauthorized,
            onForbidden,
            onValidationError,
            onServerError: noop,
            authType: 'jwt'
        });
    });

    it('should create axios instance', () => {
        expect(axios.create).toBeCalledTimes(2);
        expect(axios.create).toBeCalledWith(
            expect.objectContaining({
                baseURL: '/api',
                headers: {
                    ['Cache-Control']: 'no-cache',
                },
                timeout: undefined,
                timeoutErrorMessage: 'Timeout Error',
                withCredentials: undefined,
                xsrfCookieName: 'XSRF-TOKEN',
                xsrfHeaderName: 'X-XSRF-TOKEN',
            }),
        );
    });

    describe('when asked to make request', () => {
        let request: Request<void, Item>;
        let axiosRequest: AxiosRequest;
        let axiosResponse: AxiosResponse;

        beforeEach(() => {
            request = createRequest();
            axiosRequest = createAxiosRequest();
            axiosResponse = createAxiosResponse();
            axiosInstance.mockResolvedValue(axiosResponse);
            mockLogging();
        });

        it('should return success response', async () => {
            expect(await requester.makeRequest(request)).toEqual(createSuccessResponse());
        });

        it('should execute axios request', async () => {
            await requester.makeRequest(request);
            expect(axiosInstance).toBeCalledTimes(1);
            expect(axiosInstance).toBeCalledWith(axiosRequest);
        });

        it('should return response with jwt refresh url in instance property', async () => {
            axiosInstance.mockResolvedValueOnce(createAxiosResponseJwtInstance());
            const response = await requester.makeRequest(createRequestWithInstanceSchema());
            expect(response).toEqual(createSuccessJwtResponseWithInstance())
        });

        it('should throw validate error when response dont have data and schema requires it', async () => {
            axiosInstance.mockResolvedValueOnce(createAxiosResponseWithNoData());
            const response = await requester.makeRequest(request);
            expect(response).toEqual(createInvalidResponse())
        });

        it('should log messages', async () => {
            await requester.makeRequest(request);
            expect(Logger.prototype.log).toBeCalledTimes(2);
            expect(Logger.prototype.log).toBeCalledWith('Sending request:', axiosRequest);
            expect(Logger.prototype.log).toBeCalledWith('Processing response:', axiosResponse);
            expect(Logger.prototype.error).not.toBeCalled();
        });

        describe('and request data are invalid', () => {
            beforeEach(() => {
                request.data = {test: 'error'} as unknown as undefined;
            });

            it('should return error response', async () => {
                expect(await requester.makeRequest(request)).toBeInstanceOf(InvalidRequest);
            });

            it('should not execute request', async () => {
                await requester.makeRequest(request);
                expect(axiosInstance).not.toBeCalled();
            });

            it('should call onValidationError', async () => {
                await requester.makeRequest(request);
                expect(onValidationError).toBeCalledTimes(1);
            });

            it('should log error', async () => {
                await requester.makeRequest(request);
                expect(Logger.prototype.error).toBeCalledTimes(1);
            });
        });

        describe('and request data schema is missing', () => {
            beforeEach(() => {
                request.requestSchema = null;
            });

            it('should return success response', async () => {
                expect(await requester.makeRequest(request)).toEqual(createSuccessResponse());
            });

            it('should execute request', async () => {
                await requester.makeRequest(request);
                expect(axiosInstance).toBeCalledTimes(1);
            });

            it('should log warning', async () => {
                await requester.makeRequest(request);
                expect(Logger.prototype.warn).toBeCalledTimes(1);
                expect(Logger.prototype.warn).toBeCalledWith('Missing request schema', request);
            });
        });

        describe('and request ends with response error', () => {
            let axiosResponseError: ResponseError;

            beforeEach(() => {
                axiosResponseError = createAxiosResponseError();
                axiosInstance.mockRejectedValue(axiosResponseError);
            });

            it('should return error response', async () => {
                expect(await requester.makeRequest(request)).toEqual(createErrorResponse());
            });

            it('should execute request', async () => {
                await requester.makeRequest(request);
                expect(axiosInstance).toBeCalledTimes(1);
            });

            it('should log messages', async () => {
                await requester.makeRequest(request);
                expect(Logger.prototype.log).toBeCalledTimes(1);
                expect(Logger.prototype.log).toBeCalledWith('Sending request:', axiosRequest);
                expect(Logger.prototype.error).toBeCalledTimes(1);
                expect(Logger.prototype.error).toBeCalledWith('Error response:', axiosResponseError.message);
            });

            describe('and error is unauthorized', () => {
                it('should call onUnauthorized', async () => {
                    axiosResponseError.response.status = StatusCodes.UNAUTHORIZED;
                    await requester.makeRequest(request);
                    expect(onUnauthorized).toBeCalledTimes(1);
                });
            });

            describe('and error is forbidden', () => {
                it('should call onForbidden', async () => {
                    axiosResponseError.response.status = StatusCodes.FORBIDDEN;
                    await requester.makeRequest(request);
                    expect(onForbidden).toBeCalledTimes(1);
                });
            });

            describe('and error is version mismatch', () => {
                it('should call onVersionMismatch', async () => {
                    axiosResponseError.response.status = StatusCodes.GONE;
                    await requester.makeRequest(request);
                    expect(onVersionMismatch).toBeCalledTimes(1);
                });
            });

        });

        describe('and request ends with conection error', () => {
            let axiosConnectionError: AxiosError;

            beforeEach(() => {
                axiosConnectionError = createAxiosConnectionError();
                axiosInstance.mockRejectedValue(axiosConnectionError);
            });

            it('should return error response', async () => {
                expect(await requester.makeRequest(request)).toEqual(createConnectionError());
            });

            it('should execute request', async () => {
                await requester.makeRequest(request);
                expect(axiosInstance).toBeCalledTimes(1);
            });

            it('should log messages', async () => {
                await requester.makeRequest(request);
                expect(Logger.prototype.log).toBeCalledTimes(1);
                expect(Logger.prototype.log).toBeCalledWith('Sending request:', axiosRequest);
                expect(Logger.prototype.error).toBeCalledTimes(1);
                expect(Logger.prototype.error).toBeCalledWith('Connection error:', axiosConnectionError.message);
            });
        });

        describe('and request ends with other error', () => {
            let otherError: Error;

            const catchMakeRequest = async (): Promise<unknown> => {
                try {
                    await requester.makeRequest(request);
                } catch (error: unknown) {
                    return error;
                }
            };

            beforeEach(() => {
                otherError = new Error();
                axiosInstance.mockRejectedValue(otherError);
            });

            it('should execute request', async () => {
                await catchMakeRequest();
                expect(axiosInstance).toBeCalledTimes(1);
            });

            it('should rethrow error', async () => {
                expect(await catchMakeRequest()).toEqual(otherError);
            });
        });

        describe('and response data are invalid', () => {
            beforeEach(() => {
                axiosResponse.data = {test: 'error'};
            });

            it('should return error response', async () => {
                expect(await requester.makeRequest(request)).toBeInstanceOf(InvalidResponse);
            });

            it('should execute request', async () => {
                await requester.makeRequest(request);
                expect(axiosInstance).toBeCalledTimes(1);
            });

            it('should call onValidationError', async () => {
                await requester.makeRequest(request);
                expect(onValidationError).toBeCalledTimes(1);
            });

            it('should log error', async () => {
                await requester.makeRequest(request);
                expect(Logger.prototype.error).toBeCalledTimes(1);
            });
        });

        describe('and response data schema is missing', () => {
            beforeEach(() => {
                request.responseSchema = null;
            });

            it('should return success response', async () => {
                expect(await requester.makeRequest(request)).toEqual(createSuccessResponse());
            });

            it('should execute request', async () => {
                await requester.makeRequest(request);
                expect(axiosInstance).toBeCalledTimes(1);
            });

            it('should log warning', async () => {
                await requester.makeRequest(request);
                expect(Logger.prototype.warn).toBeCalledTimes(1);
                expect(Logger.prototype.warn).toBeCalledWith('Missing response schema', request);
            });
        });

        describe('requester without optional properties', () => {
            it('should execute correctly', async () => {
                const requesterWithoutOptionalProperties  = new Requester({
                    baseUrl: '/api',
                    devMode: true,
                    onUnauthorized,
                    onServerError: noop,
                    getAuthToken: () => "Auth token",
                });
                await requesterWithoutOptionalProperties.makeRequest(request);
                expect(axiosInstance).toBeCalledTimes(1);
                expect(axiosInstance).toBeCalledWith(axiosRequest);
            });
        });
    });

    describe('jwt', () => {
        let request: Request<void, Item>;
        let axiosRequest: AxiosRequest;
        let axiosResponse: AxiosResponse;

        beforeEach(() => {
            request = createRequest();
            axiosRequest = createAxiosRequestJwt();
            axiosResponse = createAxiosResponse();
            axiosInstance.mockResolvedValue(axiosResponse);
            mockLogging();
        });

        it('should return error response', async () => {
            const axiosResponseError = createAxiosResponseError();
            axiosInstance.mockRejectedValueOnce(axiosResponseError);

            expect(await requesterJwt.makeRequest(request)).toEqual(createErrorResponse());
        });

        it('should return success response', async () => {
            expect(await requesterJwt.makeRequest(request)).toEqual(createSuccessResponse());
        });

        it('should execute axios request', async () => {
            await requesterJwt.makeRequest(request);
            expect(axiosInstance).toBeCalledTimes(1);
            expect(axiosInstance).toBeCalledWith(axiosRequest);
        });

        it('should log messages', async () => {
            await requesterJwt.makeRequest(request);
            expect(Logger.prototype.log).toBeCalledTimes(2);
            expect(Logger.prototype.log).toBeCalledWith('Sending request:', axiosRequest);
            expect(Logger.prototype.log).toBeCalledWith('Processing response:', axiosResponse);
            expect(Logger.prototype.error).not.toBeCalled();
        });
    })

    describe('refresh jwt', () => {
        let request: Request<void, Item>;
        let axiosRequest: AxiosRequest;

        beforeEach(() => {
            request = createRequestWithJwtRefresh();
            axiosRequest = createAxiosRequestJwt()
            mockLogging();

        });

        it('should return success response', async () => {
            axiosInstance.mockResolvedValueOnce(createSuccessResponse());

            const response = await requesterJwt.makeRequest(request);

            expect(response).toEqual(createSuccessResponse());
        });

        it('should execute axios request', async () => {
            axiosInstance.mockResolvedValueOnce(createSuccessResponse());
            await requesterJwt.makeRequest(request);
            expect(axiosInstance).toBeCalledTimes(1);
        });

        describe('when request fails with unauthorized error', () => {
            let axiosUnAuthorizedResponseError: ResponseError;
            let axiosResponseError: ResponseError;
            const unauthorizedResponse = {"data": [], "headers": {}, "status": 401};

            beforeEach(() => {
                request = createRequest();
                axiosUnAuthorizedResponseError = createAxiosUnauthorizedErrorResponse();
                axiosResponseError = createAxiosResponseError();
                axiosInstance.mockRejectedValueOnce(axiosUnAuthorizedResponseError);
            });

            it('should wait until isAccessTokenRefreshing is true', async () => {
                requester['isAccessTokenRefreshing'] = true;
                const handleUnauthorizedJwtErrorSpy = jest.spyOn(requester as any, 'handleUnauthorizedJwtError');
                const trySendRequestSpy = jest.spyOn(requester as any, 'trySendRequest').mockResolvedValue(createSuccessResponse());

                setTimeout(() => {
                    requester['isAccessTokenRefreshing'] = false;
                }, 100);

                const response = await requester['handleUnauthorizedJwtError'](request);
                expect(handleUnauthorizedJwtErrorSpy).toBeCalledTimes(1);
                expect(trySendRequestSpy).toBeCalledTimes(1);
                expect(response).toEqual(createSuccessResponse());
            });

            it('should attempt to refresh the JWT token and retry the original request success', async () => {
                axiosInstance.mockResolvedValueOnce(createAxiosResponseJwtRefresh())
                .mockResolvedValueOnce(createSuccessResponse());
                const response = await requesterJwt.makeRequest(request);
                expect(response).toEqual(createSuccessResponse());

                expect(axiosInstance).toBeCalledTimes(3);
                expect(Logger.prototype.log).toBeCalledWith('Sending request:', axiosRequest);
                expect(Logger.prototype.log).toBeCalledWith('Unauthorized:', unauthorizedResponse);
            });

            it('should attempt to refresh the JWT token and return code is diferent than No content', async () => {
                axiosInstance.mockResolvedValueOnce(createAxiosResponseJwtRefreshWithOk())
                const response = await requesterJwt.makeRequest(request);
                expect(response).toEqual(createSuccessResponseWithNoData());

                expect(axiosInstance).toBeCalledTimes(2);
                expect(Logger.prototype.log).toBeCalledWith('Sending request:', axiosRequest);
                expect(Logger.prototype.log).toBeCalledWith('Unauthorized:', unauthorizedResponse);
            });

            it('should attempt to refresh the JWT token and retry the original request fails', async () => {
                axiosInstance.mockResolvedValueOnce(createAxiosResponseJwtRefresh())
                .mockRejectedValueOnce(axiosResponseError)
                const response = await requesterJwt.makeRequest(request);
                expect(response).toEqual(createErrorResponse());

                expect(axiosInstance).toBeCalledTimes(3);
                expect(Logger.prototype.log).toBeCalledWith('Sending request:', axiosRequest);
                expect(Logger.prototype.log).toBeCalledWith('Unauthorized:', unauthorizedResponse);
                expect(Logger.prototype.error).toBeCalledWith('Error response:', axiosResponseError.message);
            });

            it('should return unauthorized error if JWT refresh fails', async () => {
                axiosInstance.mockRejectedValueOnce(axiosUnAuthorizedResponseError);

                const response = await requesterJwt.makeRequest(request);
                expect(response).toEqual(createUnAuthorizedErrorResponse())
                expect(onUnauthorized).toBeCalledTimes(1);
                expect(Logger.prototype.log).toBeCalledWith('Sending request:', axiosRequest);
                expect(Logger.prototype.log).toBeCalledWith('Unauthorized:', unauthorizedResponse);
            });

            it('should return connection error if JWT refresh fails dues to connection', async () => {
                axiosInstance.mockRejectedValueOnce(createAxiosConnectionError());

                const response = await requesterJwt.makeRequest(request);
                expect(response).toEqual(createConnectionError())
                expect(onUnauthorized).toBeCalledTimes(0);
                expect(Logger.prototype.log).toBeCalledWith('Sending request:', axiosRequest);
                expect(Logger.prototype.log).toBeCalledWith('Unauthorized:', unauthorizedResponse);
            });

            it('should return validation error if JWT refresh fails dues to validation', async () => {
                const validationError = createAxiosValidationError()
                axiosInstance.mockRejectedValueOnce(validationError);
                Object.setPrototypeOf(validationError, ValidationError.prototype);

                const response = await requesterJwt.makeRequest(request);
                expect(response).toEqual(createValidationError())
                expect(onUnauthorized).toBeCalledTimes(0);
                expect(Logger.prototype.log).toBeCalledWith('Sending request:', axiosRequest);
                expect(Logger.prototype.log).toBeCalledWith('Unauthorized:', unauthorizedResponse);
            });

            it('should return internal error if JWT refresh fails dues to internal error on back end', async () => {
                axiosInstance.mockRejectedValueOnce(createJwtRefreshErrorResponse());

                const response = await requesterJwt.makeRequest(request);
                expect(response).toEqual(createInternalErrorResponse())
                expect(onUnauthorized).toBeCalledTimes(0);
                expect(Logger.prototype.log).toBeCalledWith('Sending request:', axiosRequest);
                expect(Logger.prototype.log).toBeCalledWith('Unauthorized:', unauthorizedResponse);
            });

            it('should throw error if JWT refresh fails dues to unknown error', async () => {
                axiosInstance.mockRejectedValueOnce(createUnknownError());

                await expect(requesterJwt.makeRequest(request)).rejects.toEqual(createUnknownError());
                expect(onUnauthorized).toBeCalledTimes(0);
                expect(Logger.prototype.log).toBeCalledWith('Sending request:', axiosRequest);
                expect(Logger.prototype.log).toBeCalledWith('Unauthorized:', unauthorizedResponse);
            });

            it('should return unauthorized error if JWT login fails', async () => {
                const loginUrl = "/public/api/jwt/login"
                request.url = loginUrl
                axiosRequest.url = loginUrl
                axiosInstance.mockRejectedValueOnce(axiosUnAuthorizedResponseError);
                const response = await requesterJwt.makeRequest(request);
                expect(response).toEqual(createUnAuthorizedErrorResponse())
                expect(onUnauthorized).toBeCalledTimes(0);
                expect(Logger.prototype.log).toBeCalledWith('Sending request:', axiosRequest);
                expect(Logger.prototype.error).toBeCalledWith('Error response:', axiosUnAuthorizedResponseError.message);
            });
        });
    })
});
