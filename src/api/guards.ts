/* istanbul ignore file */
import {StatusCodes} from 'http-status-codes';

import {EmptyObject} from '../types';
import {Response, SuccessResponse, ErrorResponse} from './response';

/** @deprecated use response.isSuccess */
export const isOkResponse = <T>(response: Response<T>): response is SuccessResponse<T>  =>
    response.status === StatusCodes.OK;

/** @deprecated use response.isSuccess */
export const isNoContentResponse = (response: Response<EmptyObject>): response is SuccessResponse<EmptyObject>  =>
    response.status === StatusCodes.NO_CONTENT;

/** @deprecated use response.isSuccess */
export const isCreatedResponse = <T>(response: Response<T>): response is SuccessResponse<T>  =>
    response.status === StatusCodes.CREATED;

/** @deprecated use response.isSuccess */
export const isAcceptedResponse = <T>(response: Response<T>): response is SuccessResponse<T>  =>
    response.status === StatusCodes.ACCEPTED;

/** @deprecated use response.isUnauthorized */
export const isUnauthorizedResponse = <T>(response: Response<T>): response is ErrorResponse  =>
    response.status === StatusCodes.UNAUTHORIZED;

/** @deprecated use response.isForbidden */
export const isForbiddenResponse = <T>(response: Response<T>): response is ErrorResponse =>
    response.status === StatusCodes.FORBIDDEN;

/** @deprecated use response.isNotFound */
export const isNotFoundResponse = <T>(response: Response<T>): response is ErrorResponse =>
    response.status === StatusCodes.NOT_FOUND;

/** @deprecated use response.isBadRequest */
export const isBadRequestResponse = <T>(response: Response<T>): response is ErrorResponse =>
    response.status === StatusCodes.BAD_REQUEST;

/** @deprecated use response.isConflict */
export const isConflictResponse = <T>(response: Response<T>): response is ErrorResponse =>
    response.status === StatusCodes.CONFLICT;

