import {StatusCodes} from 'http-status-codes';

import {ConnectionError, ErrorResponse, InvalidRequest, InvalidResponse, SuccessResponse, Response} from './response';

describe('Response', () => {
    describe('SuccessResponse', () => {
        it('should have properties', () => {
            const response = new SuccessResponse(StatusCodes.OK, 'testData', {testHeader: 'testHederValue'});

            expect(response.isSuccess).toEqual(true);
            expect(response.status).toEqual(StatusCodes.OK);
            expect(response.data).toEqual('testData');
            expect(response.headers).toEqual({testHeader: 'testHederValue'});
        });

        it('type narrowing works', () => {
            const response: Response<string> = new SuccessResponse(StatusCodes.OK, 'testData') as Response<string>;

            if (response.isSuccess) {
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                const success: SuccessResponse<string> = response;
                // @ts-expect-error type is SuccessResponse
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                const error: ErrorResponse = response;
            }
        });

        describe('and headers not provided', () => {
            it('should return empty object', () => {
                expect(new SuccessResponse(StatusCodes.OK, 'testData')).toHaveProperty('headers', {});
            });
        });
    });

    describe('ErrorResponse', () => {
        it('should have properties', () => {
            const response = new ErrorResponse(StatusCodes.NOT_FOUND, 'testErrorData', {testHeader: 'testHederValue'});

            expect(response.isSuccess).toEqual(false);
            expect(response.status).toEqual(StatusCodes.NOT_FOUND);
            expect(response.data).toEqual('testErrorData');
            expect(response.headers).toEqual({testHeader: 'testHederValue'});
        });

        it('type narrowing works', () => {
            const response: Response<string> = new ErrorResponse(
                StatusCodes.INTERNAL_SERVER_ERROR,
                'testData',
            ) as Response<string>;

            if (!response.isSuccess) {
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                const error: ErrorResponse = response;
                // @ts-expect-error type is SuccessResponse
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                const success: SuccessResponse<string> = response;
            }
        });

        describe('and headers not provided', () => {
            it('should have headers set to empty object', () => {
                expect(new ErrorResponse(StatusCodes.NOT_FOUND, 'testData')).toHaveProperty('headers', {});
            });
        });

        describe('and error ia NotFound', () => {
            it('should have isNotFound set to true', () => {
                expect(new ErrorResponse(StatusCodes.NOT_FOUND, 'testData')).toHaveProperty('isNotFound', true);
            });
        });

        describe('and error ia BadRequest', () => {
            it('should have isBadRequest set to true', () => {
                expect(new ErrorResponse(StatusCodes.BAD_REQUEST, 'testData')).toHaveProperty('isBadRequest', true);
            });
        });

        describe('and error is Conflict', () => {
            it('should have isConflict set to true', () => {
                expect(new ErrorResponse(StatusCodes.CONFLICT, 'testData')).toHaveProperty('isConflict', true);
            });
        });

        describe('and error ia Unauthorized', () => {
            it('should have isUnauthorized set to true', () => {
                expect(new ErrorResponse(StatusCodes.UNAUTHORIZED, 'testData')).toHaveProperty('isUnauthorized', true);
            });
        });

        describe('and error ia Forbidden', () => {
            it('should have isForbidden set to true', () => {
                expect(new ErrorResponse(StatusCodes.FORBIDDEN, 'testData')).toHaveProperty('isForbidden', true);
            });
        });

        describe('and error ia ServiceUnavailable', () => {
            it('should have isServiceUnavailable set to true', () => {
                expect(new ErrorResponse(StatusCodes.SERVICE_UNAVAILABLE, 'testData'))
                    .toHaveProperty('isServiceUnavailable', true);
            });
        });

        describe('and error is connection error', () => {
            it('should have isConnectionError set to true', () => {
                expect(new ConnectionError('connection error'))
                    .toHaveProperty('isConnectionError', true);
            });
        });

        describe('and error is invalid request', () => {
            it('should have isInvalidRequest set to true', () => {
                expect(new InvalidRequest('invalid request'))
                    .toHaveProperty('isInvalidRequest', true);
            });
        });

        describe('and error is invalid response', () => {
            it('should have isInvalidResponse set to true', () => {
                expect(new InvalidResponse('invalid response'))
                    .toHaveProperty('isInvalidResponse', true);
            });
        });

        describe('and error is MisDirectedRequest', () => {
            it('should have isMisdirectedRequest set to true', () => {
                expect(new ErrorResponse(StatusCodes.MISDIRECTED_REQUEST, 'testData')).toHaveProperty('isMisdirectedRequest', true);
            });
        });

        describe('and error is InternalServerError', () => {
            it('should have isInternalServerError set to true', () => {
                expect(new ErrorResponse(StatusCodes.INTERNAL_SERVER_ERROR, 'testData')).toHaveProperty('isInternalServerError', true);
            });
        });

        describe('and error exists', () => {

            it('should have isResponseError set to true', () => {
                expect(new ErrorResponse(StatusCodes.MISDIRECTED_REQUEST, 'testData')).toHaveProperty('isResponseError', true);
            });

            it('should have isResponseError set to false', () => {
                expect(new ErrorResponse(null, 'testData')).toHaveProperty('isResponseError', false);
            });
        });

        describe('default values', () => {
            it('should have isInvalidRequest set to false by default', () => {
                expect(new ErrorResponse(StatusCodes.NOT_FOUND, 'testData')).toHaveProperty('isInvalidRequest', false);
            });
        
        
            it('should have isInvalidResponse set to false by default', () => {
                expect(new ErrorResponse(StatusCodes.NOT_FOUND, 'testData')).toHaveProperty('isInvalidResponse', false);
            });

            it('should have isConnectionError set to false by default', () => {
                expect(new ErrorResponse(StatusCodes.NOT_FOUND, 'testData')).toHaveProperty('isConnectionError', false);
            });
        });
    });
});
