import {ArrayFormat, mapStateToSearch, StateData} from '../params';

export const serializeParams = (arrayFormat: ArrayFormat = 'comma') =>
    (params: object): string => mapStateToSearch(params as StateData, {arrayFormat, removeNullFields: true});
