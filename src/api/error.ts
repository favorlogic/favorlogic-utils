import axios, {AxiosResponse, AxiosError} from 'axios';

export interface ResponseError<T = unknown> extends AxiosError<T> {
    response: AxiosResponse<T>;
}

export const isResponseError = (error: unknown): error is ResponseError =>
    axios.isAxiosError(error) && Boolean(error.response);

const connectionErrorMessages = ['Request aborted', 'Network Error', 'Timeout Error'];

export const isConnectionError = (error: unknown): error is AxiosError =>
    axios.isAxiosError(error) && connectionErrorMessages.includes(error.message);
