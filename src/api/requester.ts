import axios, {AxiosError, AxiosInstance} from 'axios';
import {identity, noop} from 'lodash/fp';

import {validateSchema, ValidationError} from '../validate-schema';
import {Logger} from '../logger';
import {AxiosRequestBuilder} from './axios/request-builder';
import {AxiosResponseAdapter} from './axios/response-adapter';
import {
    RequesterConfig,
    buildRequestDefaultsConfig,
    buildRequestBuilderConfig,
    buildResponseAdapterConfig,
} from './config';
import {isConnectionError, isResponseError, ResponseError} from './error';
import {AuthType, Request} from './request-types';
import {Response, ErrorResponse, ConnectionError, InvalidRequest, InvalidResponse} from './response';

axios.defaults.xsrfCookieName = undefined;
axios.defaults.xsrfHeaderName = undefined;

export class Requester {
    private readonly axios: AxiosInstance;

    private readonly axiosRequestBuilder: AxiosRequestBuilder;

    private readonly axiosResponseAdapter: AxiosResponseAdapter;

    private readonly logger: Logger;

    private readonly authType: AuthType;

    private readonly onUnauthorized: () => void;

    private readonly onForbidden: () => void;

    private readonly onValidationError: (error: ValidationError) => void;

    private readonly onVersionMismatch: () => void;

    private readonly onServerError: (error: ResponseError) => void;

    private isAccessTokenRefreshing = false;
    private isRefreshTokenValid = true; // optimistic assumption, if not, code will handle it

    constructor(config: RequesterConfig) {
        this.logger = new Logger({devMode: config.devMode});
        this.axios = axios.create(buildRequestDefaultsConfig(config));
        this.axiosRequestBuilder = new AxiosRequestBuilder(buildRequestBuilderConfig(config));
        this.axiosResponseAdapter = new AxiosResponseAdapter(buildResponseAdapterConfig(config));
        this.authType = config.authType;
        this.onUnauthorized = config.onUnauthorized;
        this.onForbidden = config.onForbidden || noop;
        this.onValidationError = config.onValidationError || noop;
        this.onVersionMismatch = config.onVersionMismatch || noop;
        this.onServerError = config.onServerError;
    }

    public async makeRequest<Req, Res>(request: Request<Req, Res>): Promise<Response<Res>> {
        let response: Response<Res>;

        try {
            this.validateRequest(request);
            response = await this.trySendRequest(request);
            if (this.authType === 'jwt' && request.url === '/public/api/jwt/login') this.setRefreshTokenIsValid(true);
            if (request.url !== '/public/api/jwt/refresh') this.validateResponse(response, request);
        } catch (error) {
            response = await this.handleError(error, request);
        }

        return response;
    }

    private setAccessTokenIsRefreshing(isRefreshing: boolean): void {
        this.isAccessTokenRefreshing = isRefreshing;
    }

    private setRefreshTokenIsValid(isValid: boolean): void {
        this.isRefreshTokenValid = isValid;
    }

    private validateRequest<Res, Req>(request: Request<Res, Req>): void {
        if (request.requestSchema) {
            validateSchema(request.requestSchema, 'FE')(request.data);
        } else {
            this.logger.warn('Missing request schema', request);
        }
    }

    private async trySendRequest<Req, Res>(request: Request<Req, Res>): Promise<Response<Res>> {
        const axiosRequest = this.axiosRequestBuilder.buildRequest(request, this.authType);
        this.logger.log('Sending request:', axiosRequest);
        const axiosResponse = await this.axios(axiosRequest);
        this.logger.log('Processing response:', axiosResponse);

        return this.axiosResponseAdapter.buildResponse(axiosResponse, request.expectedStatus);
    }

    private validateResponse<Res, Req>(response: Response<unknown>, request: Request<Res, Req>): void {
        if (request.responseSchema ) {
            // @ts-ignore in refresh res we have instance
            if (!response.data?.instance || response.data.instance === '/public/api/jwt/refresh') {
                validateSchema(request.responseSchema, 'BE')(response.data);
            }
        } else {
            this.logger.warn('Missing response schema', request);
        }
    }
    private async handleError<Req, Res>(error: unknown, request: Request<Req, Res>): Promise<Response<Res>> {
        if (isResponseError(error))  return this.handleResponseError(error, request);
        if (isConnectionError(error)) return this.handleConnectionError(error);
        if (error instanceof ValidationError) return this.handleValidationError(error);

        throw error;
    }

    private async handleUnauthorizedJwtError<Req, Res>(originalRequest: Request<Req, Res>): Promise<Response<Res>> {
        // wait until access token is refreshing
        if (this.isAccessTokenRefreshing) {
            while (this.isAccessTokenRefreshing) {
                await new Promise(resolve => setTimeout(resolve, 50));
            }
            return this.isRefreshTokenValid
            ? this.trySendRequest(originalRequest)
            // way to not send original request if refresh token is not valid
            : new Promise(identity)
        } else {
            this.setAccessTokenIsRefreshing(true);
            const restoreAuthToken: Request<Req, Res> = {
                url: '/public/api/jwt/refresh',
                method: 'post',
                baseUrl: originalRequest.baseUrl,
                requestSchema: null,
                responseSchema: null,
                headers: originalRequest.headers,
                responseType: 'json',
                timeout: originalRequest.timeout,
                expectedStatus: 200,
            };

            const restoreAuthTokenReq =  this.axiosRequestBuilder.buildRequest(restoreAuthToken, this.authType);
            try {
                this.logger.log('Sending request:', restoreAuthTokenReq);
                const restoreAuthTokenResponse = await this.axios(restoreAuthTokenReq);
                this.logger.log('Processing response:', restoreAuthTokenResponse);
                this.setAccessTokenIsRefreshing(false);
                if (restoreAuthTokenResponse.status === 204) {
                    let resp: Response<Res>;

                    try {
                        this.validateRequest(originalRequest);
                        resp = await this.trySendRequest(originalRequest);
                        this.validateResponse(resp, originalRequest);
                    } catch (error) {
                        resp = await this.handleError(error, originalRequest);
                    }

                    return resp;
                }
               return this.axiosResponseAdapter.buildResponse(restoreAuthTokenResponse, restoreAuthToken.expectedStatus);
            } catch (error) {
                if (isResponseError(error)) {
                    const response = this.axiosResponseAdapter.buildErrorResponse(error.response);
                        this.setRefreshTokenIsValid(false); // need to log in
                        this.setAccessTokenIsRefreshing(false);
                    if (response.isUnauthorized)  {  // if refresh token is expired
                        this.onUnauthorized();
                    } else {
                        await this.handleError(error, restoreAuthToken);
                    }
                    return response;
                }
                if (isConnectionError(error)) return this.handleConnectionError(error);
                if (error instanceof ValidationError) return this.handleValidationError(error);

                throw error;
            }
        }
    }

    private async handleResponseError<Req, Res>(
        error: ResponseError,
        originalRequest: Request<Req, Res>
    ): Promise<Response<Res>> {
        const response = this.axiosResponseAdapter.buildErrorResponse(error.response);
        let authResponse: Response<Res> | undefined = undefined;

        if (response.isUnauthorized && originalRequest.url !== '/public/api/jwt/login') {
            if (this.authType === 'jwt') {
                this.logger.log('Unauthorized:', response);
                if (this.isRefreshTokenValid) authResponse = await this.handleUnauthorizedJwtError(originalRequest);
            } else {
                this.logger.error('Error response:', error.message);
                this.onUnauthorized();
            }
        } else {
            this.logger.error('Error response:', error.message);
            if (response.isForbidden) this.onForbidden();
            if (response.isServerError) this.onServerError(error);
            if (response.isVersionMismatch) this.onVersionMismatch();
        }

        return authResponse ?? response;
    }

    private handleConnectionError(error: AxiosError): ErrorResponse {
        this.logger.error('Connection error:', error.message);

        return new ConnectionError(error.message);
    }

    private handleValidationError(error: ValidationError): ErrorResponse {
        const errorMessage = error.source === 'FE' ?
            `Invalid request data: ${error.message}` :
            `Invalid response data: ${error.message}`;

        this.logger.error(errorMessage);
        this.onValidationError(error);

        return error.source === 'FE' ?
            new InvalidRequest(error.message) :
            new InvalidResponse(error.message);
    }
}
