import {AxiosError, AxiosResponse} from 'axios';
import {ReasonPhrases, StatusCodes} from 'http-status-codes';

import {isConnectionError, isResponseError} from './error';

const creteError = () => new Error('test error');

const creteAxiosError = (message: string, response?: AxiosResponse) => {
    const error = new Error(message) as AxiosError;

    error.isAxiosError = true;
    error.response = response;

    return error;
};

const testResponse: AxiosResponse = {
    statusText: ReasonPhrases.OK,
    status: StatusCodes.OK,
    data: {},
    config: {},
    headers: {},
};

const createResponseError = () => creteAxiosError('Response Error', testResponse);
const createRequestAbortedError = () => creteAxiosError('Request aborted');
const createTimoeutError = () => creteAxiosError('Timeout Error');
const createNetworkError = () => creteAxiosError('Network Error');
const createOtherAxiosError = () => creteAxiosError('Other Axios Error');

describe('Error', () => {
    it('ResponseError', () => {
        expect(isResponseError(creteError())).toEqual(false);
        expect(isResponseError(createRequestAbortedError())).toEqual(false);
        expect(isResponseError(createTimoeutError())).toEqual(false);
        expect(isResponseError(createNetworkError())).toEqual(false);
        expect(isResponseError(createOtherAxiosError())).toEqual(false);
        expect(isResponseError(createResponseError())).toEqual(true);
    });

    it('ConnectionError', () => {
        expect(isConnectionError(creteError())).toEqual(false);
        expect(isConnectionError(createRequestAbortedError())).toEqual(true);
        expect(isConnectionError(createTimoeutError())).toEqual(true);
        expect(isConnectionError(createNetworkError())).toEqual(true);
        expect(isConnectionError(createOtherAxiosError())).toEqual(false);
        expect(isConnectionError(createResponseError())).toEqual(false);
    });
});
