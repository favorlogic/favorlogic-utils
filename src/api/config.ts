import {AxiosRequestConfig} from 'axios';

import {ArrayFormat} from '../params';
import {ValidationError} from '../validate-schema';
import {AxiosRequestBuilderConfig, VersionInfo} from './axios/request-builder';
import {AxiosResponseAdapterConfig} from './axios/response-adapter';
import {ResponseError} from './error';
import {serializeParams} from './params';
import {AuthType} from './request-types';

export interface RequesterConfig {
    baseUrl: string;
    devMode: boolean;
    convertEmptyStringResponse?: boolean;
    removeNullFieldsFromResponse?: boolean;
    withCredentials?: boolean;
    timeout?: number;
    arrayFormat?: ArrayFormat;
    xsrf?: boolean;
    noCache?: boolean;
    versionInfo?: VersionInfo;
    authType?: AuthType;
    onUnauthorized(): void;
    getAcceptLanguage?(): string;
    getAuthToken?(): string | undefined;
    onForbidden?(): void;
    onValidationError?(error: ValidationError): void;
    onVersionMismatch?(): void;
    onServerError(error: ResponseError): void;
}

const buildDefaultHeaders = (config: RequesterConfig): Record<string, string> => {
    const headers: Record<string, string> = {};

    if (config.noCache !== false) {
        headers['Cache-Control'] = 'no-cache';
    }

    return headers;
};

export const buildRequestDefaultsConfig = (config: RequesterConfig): AxiosRequestConfig => ({
    baseURL: config.baseUrl,
    withCredentials: config.withCredentials,
    timeout: config.timeout,
    timeoutErrorMessage: 'Timeout Error',
    paramsSerializer: serializeParams(config.arrayFormat),
    xsrfCookieName: config.xsrf === false ? undefined : 'XSRF-TOKEN',
    xsrfHeaderName: config.xsrf === false ? undefined : 'X-XSRF-TOKEN',
    headers: buildDefaultHeaders(config),
});

export const buildRequestBuilderConfig = (config: RequesterConfig): AxiosRequestBuilderConfig => ({
    versionInfo: config.versionInfo,
    getAcceptLanguage: config.getAcceptLanguage,
    getAuthToken: config.getAuthToken,
});

export const buildResponseAdapterConfig = (config: RequesterConfig): AxiosResponseAdapterConfig => ({
    convertEmptyStringResponse: config.convertEmptyStringResponse,
    removeNullFieldsFromResponse: config.removeNullFieldsFromResponse,
});
