/* eslint-disable max-classes-per-file */
import {StatusCodes} from 'http-status-codes';
import {isNull} from 'lodash/fp';

type ResponseHeaders = Partial<Record<string, string>>;

export class SuccessResponse<T> {
    constructor(
        public readonly status: StatusCodes,
        public readonly data: T,
        public readonly headers: ResponseHeaders = {},
    ) {
    }

    public get isSuccess(): true {
        return true;
    }
}

export class ErrorResponse<T = unknown> {
    constructor(
        public readonly status: StatusCodes | null,
        public readonly data: T,
        public readonly headers: ResponseHeaders = {},
    ) {
    }

    public get isSuccess(): false {
        return false;
    }

    public get isNotFound(): boolean {
        return this.status === StatusCodes.NOT_FOUND;
    }

    public get isMisdirectedRequest(): boolean {
        return this.status === StatusCodes.MISDIRECTED_REQUEST;
    }

    public get isBadRequest(): boolean {
        return this.status === StatusCodes.BAD_REQUEST;
    }

    public get isConflict(): boolean {
        return this.status === StatusCodes.CONFLICT;
    }

    public get isUnauthorized(): boolean {
        return this.status === StatusCodes.UNAUTHORIZED;
    }

    public get isForbidden(): boolean {
        return this.status === StatusCodes.FORBIDDEN;
    }

    public get isServiceUnavailable(): boolean {
        return this.status === StatusCodes.SERVICE_UNAVAILABLE;
    }

    public get isResponseError(): boolean {
        return this.status !== null;
    }

    public get isConnectionError(): boolean {
        return false;
    }

    public get isInvalidRequest(): boolean {
        return false;
    }

    public get isInvalidResponse(): boolean {
        return false;
    }

    public get isInternalServerError(): boolean {
        return this.status === StatusCodes.INTERNAL_SERVER_ERROR;
    }

    public get isServerError(): boolean {
        return !isNull(this.status) && this.status >= StatusCodes.INTERNAL_SERVER_ERROR;
    }

    public get isVersionMismatch(): boolean {
        return this.status === StatusCodes.GONE;
    }
}

export class ConnectionError extends ErrorResponse<string> {
    constructor(message: string) {
        super(null, message);
    }

    public get isConnectionError(): boolean {
        return true;
    }
}

export class InvalidRequest extends ErrorResponse<string> {
    constructor(message: string) {
        super(null, message);
    }

    public get isInvalidRequest(): boolean {
        return true;
    }
}

export class InvalidResponse extends ErrorResponse<string> {
    constructor(message: string) {
        super(null, message);
    }

    public get isInvalidResponse(): boolean {
        return true;
    }
}

export type Response<T> = SuccessResponse<T> | ErrorResponse;

export type PResponse<T> = Promise<Response<T>>;

