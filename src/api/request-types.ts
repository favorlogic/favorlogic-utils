import {StatusCodes} from 'http-status-codes';
import {Type} from 'io-ts';

export type RequestMethod = 'get' | 'post' | 'put' | 'delete' | 'patch' | 'head';
type ResponseType = 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | 'stream';
type RequestHeaders = Record<string, string>;

export interface Request<Req, Res> {
    url: string;
    method?: RequestMethod;
    baseUrl?: string;
    requestSchema: Type<Req, unknown> | null;
    responseSchema: Type<Res, unknown> | null;
    headers?: RequestHeaders;
    params?: object;
    data?: Req;
    responseType?: ResponseType;
    withCredentials?: boolean;
    timeout?: number;
    expectedStatus?: StatusCodes;
}

export type AuthType = 'basic' | 'jwt' | undefined;
