import {mapStateToSearch} from '../params';
import {serializeParams} from './params';

jest.mock('../params', () => ({
    mapStateToSearch: jest.fn().mockReturnValue('serializedParams'),
}));

describe('params', () => {
    const params = {
        paramA: 'testValue',
        paramB: ['value1', 'value2'],
    };

    beforeEach(() => jest.clearAllMocks());

    describe('when asked to serialize with bracket array format', () => {
        it('should call mapStateToSearch', () => {
            expect(serializeParams('bracket')(params)).toEqual('serializedParams');
            expect(mapStateToSearch).toBeCalledTimes(1);
            expect(mapStateToSearch).toBeCalledWith(params, {arrayFormat: 'bracket', removeNullFields: true});
        });
    });
});
