import {AxiosResponse} from 'axios';
import {StatusCodes, ReasonPhrases} from 'http-status-codes';

import {removeNullFieldsRecursively} from '../../basic';
import {ErrorResponse, SuccessResponse, Response} from '../response';
import {AxiosResponseAdapter} from './response-adapter';

jest.mock('../../basic', () => ({
    removeNullFieldsRecursively: jest.fn().mockReturnValue('testDataWithoutNullFields'),
}));

const expectedStatus = StatusCodes.OK;
const notExpectedStatus = StatusCodes.NO_CONTENT;

const createMockAxiosResponse = (): AxiosResponse => ({
    statusText: ReasonPhrases.OK,
    status: StatusCodes.OK,
    data: 'testData',
    config: {},
    headers: {testHeader: 'testHeaderValue'},
});

const expectSuccessResponse = (response: Response<unknown>) => {
    expect(response).toBeInstanceOf(SuccessResponse);
    expect(response.status).toEqual(200);
    expect(response.data).toEqual('testData');
    expect(response.headers).toEqual({testHeader: 'testHeaderValue'});
};

const expectErrorResponse = (response: Response<unknown>) => {
    expect(response).toBeInstanceOf(ErrorResponse);
    expect(response.status).toEqual(200);
    expect(response.data).toEqual('testData');
    expect(response.headers).toEqual({testHeader: 'testHeaderValue'});
};

const expectToRemoveNullFields = (response: Response<unknown>) => {
    expect(response.data).toEqual('testDataWithoutNullFields');
    expect(removeNullFieldsRecursively).toBeCalledTimes(1);
    expect(removeNullFieldsRecursively).toBeCalledWith('testData');
};

const expectNotToRemoveNullFields = (response: Response<unknown>) => {
    expect(response.data).toEqual('testData');
    expect(removeNullFieldsRecursively).not.toBeCalled();
};

describe('AxiosResponseAdapter', () => {
    let responseAdapter: AxiosResponseAdapter;
    let axiosResponse: AxiosResponse;

    beforeEach(() => {
        jest.clearAllMocks();
        responseAdapter = new AxiosResponseAdapter({});
        axiosResponse = createMockAxiosResponse();
    });

    describe('when asked to build response', () => {
        describe('and expcted status parameter is not set', () => {
            it('should build success response', () => {
                expectSuccessResponse(responseAdapter.buildResponse(axiosResponse));
            });
        });

        describe('and status is expected', () => {
            it('should build success response', () => {
                expectSuccessResponse(responseAdapter.buildResponse(axiosResponse, expectedStatus));
            });
        });

        describe('and status is not expected', () => {
            it('should build error response', () => {
                expectErrorResponse(responseAdapter.buildResponse(axiosResponse, notExpectedStatus));
            });
        });
    });

    describe('when asked to build error response', () => {
        it('should build error response', () => {
            expectErrorResponse(responseAdapter.buildErrorResponse(axiosResponse));
        });
    });

    describe('and configured to remove null fields', () => {
        beforeEach(() => {
            responseAdapter = new AxiosResponseAdapter({removeNullFieldsFromResponse: true});
        });

        describe('when asked to build response', () => {
            describe('and expcted status parameter is not set', () => {
                it('should remove null fields', () => {
                    expectToRemoveNullFields(responseAdapter.buildResponse(axiosResponse));
                });
            });

            describe('and status is expected', () => {
                it('should remove null fields', () => {
                    expectToRemoveNullFields(responseAdapter.buildResponse(axiosResponse, expectedStatus));
                });
            });

            describe('and status is not expected', () => {
                it('should not remove null fields', () => {
                    expectNotToRemoveNullFields(responseAdapter.buildResponse(axiosResponse, notExpectedStatus));
                });
            });
        });
    });

    describe('and configured to convert empty string response to object', () => {
        beforeEach(() => {
            responseAdapter = new AxiosResponseAdapter({convertEmptyStringResponse: true});
            axiosResponse.data = '';
        });

        describe('when asked to build response', () => {
            describe('and expcted status parameter is not set', () => {
                it('should convert empty string response to object', () => {
                    expect(responseAdapter.buildResponse(axiosResponse))
                        .toHaveProperty('data', {});
                });
            });

            describe('and status is expected', () => {
                it('should convert empty string response to object', () => {
                    expect(responseAdapter.buildResponse(axiosResponse, expectedStatus))
                        .toHaveProperty('data', {});
                });
            });

            describe('and status is not expected', () => {
                it('should not convert empty string response to object', () => {
                    expect(responseAdapter.buildResponse(axiosResponse, notExpectedStatus))
                        .toHaveProperty('data', '');
                });
            });
        });
    });
});
