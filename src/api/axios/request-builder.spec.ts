import {Request} from '../request-types';
import {AxiosRequestBuilder} from './request-builder';

interface TestRequestData {
    test: string;
    other: string | null;
}

interface TestResponseData {
    test: string;
    other: string | null;
}

const getAuthToken = () => 'auth-token';
const getAcceptLanguage = () => 'acceptLanguage';
const getUndefinedAuthToken = () => undefined;

describe('AxiosRequestBuilder', () => {
    let requestBuilder: AxiosRequestBuilder;
    let request: Request<TestRequestData, TestResponseData>;

    beforeEach(() => {
        request = {
            url: '/test',
            method: 'get',
            baseUrl: '/api',
            headers: {testHeader: 'headerValue'},
            params: {testParam: 'paramValue'},
            data: {test: 'requestValue', other: null},
            responseType: 'json',
            withCredentials: true,
            requestSchema: null,
            responseSchema: null,
            timeout: 1000,
        };
    });

    it('should build request', () => {
        const withoutVersionRequestBuilder = new AxiosRequestBuilder({});

        expect(withoutVersionRequestBuilder.buildRequest(request)).toEqual({
            url: '/test',
            method: 'get',
            baseURL: '/api',
            headers: {testHeader: 'headerValue'},
            params: {testParam: 'paramValue'},
            data: {test: 'requestValue', other: null},
            responseType: 'json',
            withCredentials: true,
            timeout: 1000,
        });
    });

    describe("requester with versionInfo", () => {

        it('should build request with version', () => {
            requestBuilder = new AxiosRequestBuilder({versionInfo: {key: "FE-Version", version: "1.0.0"}});
            
            expect(requestBuilder.buildRequest(request)).toEqual({
                url: '/test',
                method: 'get',
                baseURL: '/api',
                headers: {testHeader: 'headerValue', "FE-Version": "1.0.0"},
                params: {testParam: 'paramValue'},
                data: {test: 'requestValue', other: null},
                responseType: 'json',
                withCredentials: true,
                timeout: 1000,
            });
        });
        
        it('should have only FE-Version header, when headers not provided', () => {
            requestBuilder = new AxiosRequestBuilder({versionInfo: {key: "FE-Version", version: "1.0.0"}});
            
            expect(requestBuilder.buildRequest({
                url: '/test',
                method: 'get',
                baseUrl: '/api',
                params: {testParam: 'paramValue'},
                data: {test: 'requestValue', other: null},
                responseType: 'json',
                withCredentials: true,
                requestSchema: null,
                responseSchema: null,
                timeout: 1000,
            }))
            .toHaveProperty('headers.FE-Version', '1.0.0');
        });
    });

    describe("auth header", () => {
        it('should add auth header', () => {
            requestBuilder = new AxiosRequestBuilder({getAuthToken});
    
            expect(requestBuilder.buildRequest(request))
                .toHaveProperty('headers.Authorization', 'auth-token');
        });
    
        it('should add empty auth header', () => {
            requestBuilder = new AxiosRequestBuilder({getAuthToken: getUndefinedAuthToken});
    
            expect(requestBuilder.buildRequest(request))
                .toHaveProperty('headers.Authorization', '');
        });
    })

    describe('getAcceptLanguage is provided', () => {
        it('should set result of getAcceptLanguage as Accept-Language header', () => {
            requestBuilder = new AxiosRequestBuilder({getAcceptLanguage});

            expect(requestBuilder.buildRequest(request))
                .toHaveProperty('headers.Accept-Language', 'acceptLanguage');
        });
        it('should have only Accept-Language header, when headers not provided', () => {
            requestBuilder = new AxiosRequestBuilder({getAcceptLanguage});

            expect(requestBuilder.buildRequest({
                url: '/test',
                method: 'get',
                baseUrl: '/api',
                params: {testParam: 'paramValue'},
                data: {test: 'requestValue', other: null},
                responseType: 'json',
                withCredentials: true,
                requestSchema: null,
                responseSchema: null,
                timeout: 1000,
            }))
                .toHaveProperty('headers.Accept-Language', 'acceptLanguage');
        });
    });
});
