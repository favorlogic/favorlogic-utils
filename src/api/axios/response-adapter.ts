import {AxiosResponse} from 'axios';
import {StatusCodes} from 'http-status-codes';

import {removeNullFieldsRecursively} from '../../basic';
import {Response, ErrorResponse, SuccessResponse} from '../response';

export interface AxiosResponseAdapterConfig {
    convertEmptyStringResponse?: boolean;
    removeNullFieldsFromResponse?: boolean;
}

export class AxiosResponseAdapter {
    constructor(private readonly config: AxiosResponseAdapterConfig) {
    }

    public buildResponse<Res>(
        {status, data, headers}: AxiosResponse<Res>,
        expectedStatus?: StatusCodes,
    ): Response<Res> {
        if (expectedStatus !== undefined && expectedStatus !== status) {
            return new ErrorResponse(status, data, headers);
        }

        return new SuccessResponse(status, this.processResponseData(data), headers);
    }

    public buildErrorResponse<Res>({status, data, headers}: AxiosResponse<Res>): ErrorResponse {
        return new ErrorResponse(status, data, headers);
    }

    private processResponseData<Res>(rawData: Res): Res {
        const {convertEmptyStringResponse, removeNullFieldsFromResponse} = this.config;

        if (rawData as unknown === '' && convertEmptyStringResponse) {
            return {} as unknown as Res;
        }

        if (!(rawData instanceof ArrayBuffer) && removeNullFieldsFromResponse) {
            return removeNullFieldsRecursively(rawData) as Res;
        }

        return rawData;
    }
}
