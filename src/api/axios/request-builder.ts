import {AxiosRequestConfig} from 'axios';

import {AuthType, Request} from '../request-types';

export interface AxiosRequest extends AxiosRequestConfig {
    headers?: Record<string, string>;
}

export interface VersionInfo {
    key: string;
    version: string;
}

export interface AxiosRequestBuilderConfig {
    versionInfo?: VersionInfo;

    getAcceptLanguage?(): string;
    getAuthToken?(): string | undefined;
}

export class AxiosRequestBuilder {
    constructor(private readonly config: AxiosRequestBuilderConfig) {
    }

    public buildRequest<Req, Res>(request: Request<Req, Res>, authType?: AuthType): AxiosRequest {
        const axiosRequest: AxiosRequest = {
            url: request.url,
            method: request.method,
            baseURL: request.baseUrl,
            headers: request.headers,
            params: request.params,
            data: request.data,
            responseType: request.responseType,
            withCredentials: request.withCredentials,
            timeout: request.timeout,
        };

        if (authType !== 'jwt') this.addAuth(axiosRequest); // otherwise JWT handle it in secret cookies
        this.addLanguage(axiosRequest);
        this.addVersion(axiosRequest);

        return axiosRequest;
    }

    private addAuth(request: AxiosRequest): void {
        const {getAuthToken} = this.config;

        if (getAuthToken) {
            request.headers = request.headers ?? {};
            // Dot notation can not be used here due to error: TS4111: Property 'Authorization' comes from an index
            // signature, so it must be accessed with ['Authorization'].
            request.headers['Authorization'] = getAuthToken() ?? ''; // eslint-disable-line dot-notation
        }
    }

    private addLanguage(request: AxiosRequest): void {
        const {getAcceptLanguage} = this.config;

        if (getAcceptLanguage) {
            request.headers = request.headers ?? {};
            request.headers['Accept-Language'] = getAcceptLanguage();
        }
    }

    private addVersion(request: AxiosRequest): void {
        const {versionInfo} = this.config;

        if (versionInfo) {
            request.headers = request.headers ?? {};
            request.headers[versionInfo.key] = versionInfo.version;
        }
    }
}
