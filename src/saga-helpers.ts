/* eslint-disable @fl/use-eta-reduction */
import {noop} from 'lodash/fp';
import {Action} from 'redux';
import {SagaIterator} from 'redux-saga';
import {
    all,
    Effect,
    ForkEffect,
    put,
    takeLatest,
    takeLeading,
    takeEvery,
    call,
    fork,
    debounce,
} from 'redux-saga/effects';

export const putAll = (action: Action | Array<Action>): Effect =>
    Array.isArray(action)
        ? all(action.map(a => put(a)))
        : put(action);

export const takeLatestF = <A extends Action>(
    type: A['type'],
    handler: (_: A) => SagaIterator,
): ForkEffect<never> =>
    takeLatest(type, handler);

export const takeLeadingF = <A extends Action>(
    type: A['type'],
    handler: (_: A) => SagaIterator,
): ForkEffect<never> =>
    takeLeading(type, handler);

export const takeEveryF = <A extends Action>(
    type: A['type'],
    handler: (_: A) => SagaIterator,
): ForkEffect<never> =>
    takeEvery(type, handler);

export const debounceF = <A extends Action>(
    ms: number,
    type: A['type'],
    handler: (_: A) => SagaIterator,
): ForkEffect<never> =>
    debounce(ms, type, handler);

export type Saga = () => SagaIterator;

export type SagaErrorHandler = (sagaName: string, error: Error) => SagaIterator;

export const forkRestartingOnError = (saga: Saga, handleError?: SagaErrorHandler): ForkEffect =>
    fork(function* () {
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
        while (true) {
            try {
                yield call(saga);
                break;
            } catch (error) {
                // eslint-disable-next-line no-console
                console.error('Error in saga:', saga.name, error);
                yield call(handleError || noop, saga.name, error);
            }
        }
    });
