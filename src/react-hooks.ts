import {isEmpty} from 'lodash/fp';
import {useEffect, useRef} from 'react';

import {isUnknownObject} from './basic';
import {UnknownObject} from './types';

/** Hooks from https://usehooks.com **/

export function usePrevious<T>(value: T): T | undefined {
    // The ref object is a generic container whose current property is mutable ...
    // ... and can hold any value, similar to an instance property on a class
    const ref = useRef<T>();

    // Store current value in ref
    useEffect(() => {
        ref.current = value;
    }, [value]); // Only re-run if value changes

    // Return previous value (happens before update in useEffect above)
    return ref.current;
}

export function useMemoCompare<T>(next: T, compare: (prev: T | undefined, next: T) => boolean): T | undefined {
    // Ref for storing previous value
    const previousRef = useRef<T>();
    const previous = previousRef.current;

    // Pass previous and next value to compare function
    // to determine whether to consider them equal.
    const isEqual = compare(previous, next);

    // If not equal update previousRef to next value.
    // We only update if not equal so that this hook continues to return
    // the same old value if compare keeps returning true.
    useEffect(() => {
        if (!isEqual) {
            previousRef.current = next;
        }
    });

    // Finally, if equal then return the previous value
    return isEqual ? previous : next;
}

/** Custom debug Hooks **/

const noChange = '$$$_NO_CHANGE';
const objRefChange = '$$$_ONLY_OBJECT_REFERENCE_CHANGE';

function recursiveComputeChange(prev: unknown, next: unknown): object | string {
    if (prev === next) return noChange;

    if (isUnknownObject(prev) && isUnknownObject(next)) {
        const changes: UnknownObject = {};
        const prevKeys = Object.keys(prev);
        const nextKeys = Object.keys(next);

        for (const key of prevKeys) {
            const diff = recursiveComputeChange(prev[key], next[key]);
            if (diff !== noChange) changes[key] = diff;
        }

        for (const key of nextKeys) {
            const diff = recursiveComputeChange(prev[key], next[key]);
            if (diff !== noChange) changes[key] = diff;
        }

        return isEmpty(changes) ? objRefChange : changes;
    } else {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        return {$$$_PREV_VALUE: prev, $$$_NEXT_VALUE: next};
    }
}

function debugComputeChange(prevValues: unknown, nextValue: unknown): object | string {
    try {
        return recursiveComputeChange(prevValues, nextValue);
    } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);

        return `Error in computing change: ${String(error)}`;
    }
}

export function useDebugPrintDifference(obj: object, debugName?: string, showCurrent?: boolean): void {
    const prev = usePrevious(obj);
    // eslint-disable-next-line no-console
    if (prev) console.info(debugName, debugComputeChange(prev, obj), showCurrent && obj);
    // eslint-disable-next-line no-console
    else console.info(debugName, 'First render', obj);
}
