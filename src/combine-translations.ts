import {Resource} from 'i18next';
import {merge, reduce, intersection, isEmpty} from 'lodash/fp';

import {allObjectPaths} from './basic';

/**
 * Merges an array of translations (objects).
 */
export const combineTranslations = (translations: Array<Resource>): Resource =>
    reduce((acc, item) => {
        const accPaths = allObjectPaths(acc);
        const itemPaths = allObjectPaths(item);
        const conflicts = intersection(accPaths, itemPaths);

        if (!isEmpty(conflicts)) {
            const conflictsString = conflicts.map(x => `'${x}'`).join(', ');
            throw new Error(`Found conflicting translations: ${conflictsString}.`);
        }

        return merge(acc, item);
    }, {}, translations);
