import {concat, join, split, tail, toLower} from 'lodash/fp';
import {pipe, head} from 'ts-opt';

export const decapitalizeWord = (word: string): string =>
    pipe(
        word,
        split(''),
        xs => concat(
            head(xs).map(toLower).orElse(''),
            tail(xs),
        ),
        join(''),
    );
