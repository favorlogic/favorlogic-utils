import {mergeObjects} from './merge-objects';

interface FinalObjectA {
    a: string;
    b: number;
    c: boolean;
}
interface FinalObjectB {
    a: string;
    b: string;
}
interface FinalObjectC {
    a: number;
    b: number;
    c: boolean;
}
interface InputObjectA {
    a: number;
}
interface InputObjectB {
    b: string;
    a: string;
}
interface InputObjectC {
    b: number;
    c: boolean;
}

describe('merge objects', () => {
    const a: InputObjectA = {a: 42};
    const b: InputObjectB = {b: 'foo', a: 'bar'};
    const c: InputObjectC = {c: true, b: 123};

    it('expected values', () => {
        const resultA: FinalObjectA = {a: 'bar', b: 123, c: true};
        const resultB: FinalObjectB = {a: 'bar', b: 'foo'};
        const resultC: FinalObjectC = {a: 42, b: 123, c: true};
        const merge1: FinalObjectA = mergeObjects(a, b, c);
        const merge2: FinalObjectB = mergeObjects(a, b);
        const merge3: FinalObjectC = mergeObjects(a, c);
        expect(merge1).toEqual(resultA);
        expect(merge2).toEqual(resultB);
        expect(merge3).toEqual(resultC);
    });
});
