import {intersection} from 'lodash/fp';

// array subset check
export const isSubset = (small: Array<unknown>, big: Array<unknown>): boolean => {
    const common = intersection(small, big);

    return small.length === common.length;
};
