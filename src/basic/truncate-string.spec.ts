import {truncateString} from './truncate-string';

describe('truncateString', () => {
    it('truncated', () => {
        const value = truncateString('This is some text', 7);
        expect(value).toEqual('This is...');
    });

    it('not truncated', () => {
        const value = truncateString('This is some text', 20);
        expect(value).toEqual('This is some text');
    });
});
