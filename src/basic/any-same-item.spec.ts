import {cloneDeep} from 'lodash/fp';

import {anySameItem} from './any-same-item';

const f = anySameItem;

describe('anySameItem', () => {
    it('should return true when there are duplicates in both arrays', () => {
        expect(f([1, 2, 3])([2, 4, 5])).toBe(true);
        expect(f(['apple', 'lemon', 'banana', 'lemon'])(['peach', 'lemon'])).toBe(true);
    });

    it('should return false when there are no duplicates in both arrays', () => {
        expect(f([1, 2, 3])([4, 5, 6])).toBe(false);
        expect(f([5, 6, 8])([1, 2, 3])).toBe(false);
        expect(f(['apple', 'banana', 'fig', 'cherry'])(['lemon', 'peach', 'kiwi'])).toBe(false);
    });

    it('should return false when either of the arrays is empty', () => {
        expect(f([1, 2, 3])([])).toBe(false);
        expect(f([])([])).toBe(false);
    });

    it('should return false when both arrays have only one element and they are different', () => {
        expect(f([1])([2])).toBe(false);
        expect(f(['apple'])(['banana'])).toBe(false);
    });

    it('should return true when both arrays have only one element and they are the same', () => {
        expect(f([1])([1])).toBe(true);
        expect(f(['apple'])(['apple'])).toBe(true);
    });

    it('should handle array(s) with duplicated elements', () => {
        expect(f([1, 2, 2, 3, 3])([])).toBe(false);
        expect(f(['apple', 'lemon', 'lemon'])([])).toBe(false);
        expect(f(['apple', 'lemon', 'lemon'])(['peach', 'lemon'])).toBe(true);
        expect(f([1, 2, 2, 3, 3])([2, 3, 3, 4])).toBe(true);
        expect(f(['apple', 'lemon', 'lemon'])(['peach', 'lemon'])).toBe(true);

        expect(f([1, 2, 2, 3, 3])([4, 5, 6, 7, 8, 9])).toBe(false);
        expect(f(['apple', 'banana', 'lemon'])(['cherry', 'strawberry', 'fig', 'strawberry'])).toBe(false);
    });

    it('should handle arrays with different types of elements', () => {
        expect(f([1, 'apple', true, null])(['banana', 2, null])).toBe(true);
        expect(f([1, 'apple', true, null])(['banana', 'apple', false])).toBe(true);
    });

    it('should return true when there are duplicates in both arrays with objects', () => {
        const obj1 = {id: 1, name: 'John'};
        const obj2 = {id: 2, name: 'Jane'};
        const obj3 = {id: 3, name: 'Bob'};
        const obj4 = {id: 4, name: 'Alice'};
        const obj5 = {id: 5, name: 'John'};

        expect(f([obj1, obj2, obj3])([obj3, obj4, obj5])).toBe(true);
    });

    it('should compare via strict equality', () => {
        const john1 = {id: 1, name: 'John', address: {city: 'New York', state: 'NY'}};
        const bob = {id: 3, name: 'Bob', address: {city: 'Boston', state: 'MA'}};
        const john2 = cloneDeep(john1);

        expect(f([john1, bob])([bob])).toBe(true);
        expect(f([john1, bob])([john2])).toBe(false);
    });

    it('should not use type coersion', () => {
        const numberInField: Array<number | string> = [1];
        const stringInField: Array<number | string> = ['1'];
        const numberInObject: { a: number | string } = {a: 1};
        const stringInObject: { a: number | string } = {a: '1'};

        expect(f(numberInField)(stringInField)).toBe(false);
        expect(f([numberInObject])([stringInObject])).toBe(false);
    });
});
