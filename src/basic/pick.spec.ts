import {pick} from './pick';

interface FinalObject1 {
    a: number;
    c: number;
}

interface InputObject {
    a: number;
    b: number;
    c: number;
}

interface InputObject1 {
    a?: number;
    b: number;
    c?: number;
}

interface FinalObject2 {c: number}

describe('pick', () => {
    const inputObject: InputObject = {a: 1, b: 2, c: 3};
    const inputObject1: InputObject1 = {b: 2};
    const blankObject = {};
    const finalObject1: FinalObject1 = {a: 1, c: 3};
    const finalObject2: FinalObject2 = {c: 3};

    it('expected values', () => {
        expect(pick(inputObject, ['a', 'c'])).toEqual(finalObject1);
        expect(pick({}, [])).toEqual({});
        expect(pick(inputObject, [])).toEqual(blankObject);
        expect(pick(inputObject, [])).toEqual(blankObject);
    });
    it('expected type errors', () => {
        // @ts-expect-error type error
        expect(pick(inputObject, ['d', 'c'])).toEqual(finalObject2);
        // @ts-expect-error type error
        expect(pick(inputObject, ['d'])).toEqual(blankObject);
        // @ts-expect-error type error
        expect(pick(blankObject, ['d'])).toEqual(blankObject);
    });
    it('optional fields', () => {
        expect(pick(inputObject1, ['a', 'c'])).toEqual(blankObject);
    });
});
