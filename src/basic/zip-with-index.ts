import {isArray, range, zip} from 'lodash/fp';

export const zipWithIndex = <A>(arr: Array<A>): Array<[A, number]> => {
    if (!isArray(arr)) throw new Error('Array expected.');

    return zip(arr, range(0, arr.length)) as Array<[A, number]>; // better types?
};
