import {isArray as _isArray, isString} from 'lodash/fp';

export const isStringArray = (x: unknown): x is Array<string> => _isArray(x) && x.every(isString);
