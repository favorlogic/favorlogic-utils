import {Opt, parseInt} from 'ts-opt';

export const convertStringToInt = (x: string = ''): Opt<number> => parseInt(x);
