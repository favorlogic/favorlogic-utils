import {padEnd} from './pad-end';

describe('padEnd', () => {
    it('pads with default padding', () => {
        expect(padEnd()(0)('0')).toEqual('0');
        expect(padEnd()(3)('0')).toEqual('0  ');
    });
    it('pads with custom padding', () => {
        expect(padEnd('-')(0)('0')).toEqual('0');
        expect(padEnd('-')(3)('0')).toEqual('0--');
    });
});
