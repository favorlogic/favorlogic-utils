import {emptyArrayToUndefined} from './empty-array-to-undefined';

const f = emptyArrayToUndefined;
describe('emptyArrayToUndefined', () => {
    it('empty', () => {
        expect(f([])).toEqual(undefined);
    });

    it('undefined', () => {
        expect(f(undefined)).toEqual(undefined);
    });

    it('filled', () => {
        expect(f([undefined])).toEqual([undefined]);
        expect(f([1, 2])).toEqual([1, 2]);
    });
});
