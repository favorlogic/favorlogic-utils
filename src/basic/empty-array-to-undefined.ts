export const emptyArrayToUndefined = <T>(xs: Array<T> | undefined): Array<T> | undefined => xs?.length ? xs : undefined;
