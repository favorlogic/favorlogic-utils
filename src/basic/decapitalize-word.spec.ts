import {decapitalizeWord} from './decapitalize-word';

const f = decapitalizeWord;

describe('decapitalizeWord', () => {
    it('empty', () => {
        expect(f('')).toEqual('');
    });

    it('one', () => {
        expect(f('A')).toEqual('a');
    });

    it('more', () => {
        expect(f('Abc')).toEqual('abc');
    });
});
