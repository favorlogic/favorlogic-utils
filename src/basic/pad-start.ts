// eslint-disable-next-line no-restricted-imports
import {padStart as _padStart} from 'lodash';

/**
 * Similar to `padStart` from `lodash/fp`, but also supports custom padding.
 *
 * @example
 * ```ts
 * padStart()(3)('0') // '  0'
 * padStart('-')(3)('0') // '--0'
 * ```
 *
 * @param padding
 */
export const padStart =
    (padding: string = ' ') => (length: number) => (x: string): string =>
        _padStart(x, length, padding);
