import {orderBy} from 'lodash/fp';

/**
 * Sort an array in ascending order based on given mapping function.
 * Immutable.
 * @example
 * ```ts
 * orderByAsc(id)([2, 1, 3]) // [1, 2, 3]
 *
 * interface Test {id: number;}
 * orderByAsc<Test>(x => x.id)([{id: 1}, {id: 0}, {id: -1}]) // [{id: -1}, {id: 0}, {id: 1}]
 * ```
 * @param f
 */
export const orderByAsc = <T>(f: (_: T) => unknown) => (xs: Array<T>): Array<T> => orderBy(f, 'asc', xs);
