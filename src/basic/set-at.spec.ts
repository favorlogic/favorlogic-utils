/* eslint-disable @typescript-eslint/no-explicit-any */

import {setAt} from './set-at';

describe('setAt', () => {
    describe('works with array', () => {
        const T = true;
        const F = false;
        it('empty', () => {
            expect(() => setAt(0)(T)([])).toThrow();
        });
        it('one', () => {
            expect(setAt(0)(T)([F])).toEqual([T]);
        });
        it('head', () => {
            expect(setAt(0)(T)([F, F])).toEqual([T, F]);
            expect(setAt(0)(T)([F, F, F])).toEqual([T, F, F]);
        });
        it('last', () => {
            expect(setAt(1)(T)([F, F])).toEqual([F, T]);
            expect(setAt(2)(T)([F, F, F])).toEqual([F, F, T]);
        });
        it('mid', () => {
            expect(setAt(1)(T)([F, F, F])).toEqual([F, T, F]);
            expect(setAt(2)(T)([F, F, F, F])).toEqual([F, F, T, F]);
        });
        it('out of bounds', () => {
            expect(() => setAt(1)(T)([F])).toThrow();
        });
        it('example', () => {
            expect(setAt(0)(true)([false, false])).toEqual([true, false]);
        });
    });
    describe('works with string', () => {
        const X = 'X';
        it('empty', () => {
            expect(() => setAt(0)(X)('')).toThrow();
        });
        it('one', () => {
            expect(setAt(0)(X)('a')).toEqual('X');
        });
        it('head', () => {
            expect(setAt(0)(X)('ab')).toEqual('Xb');
            expect(setAt(0)(X)('abc')).toEqual('Xbc');
        });
        it('last', () => {
            expect(setAt(1)(X)('ab')).toEqual('aX');
            expect(setAt(2)(X)('abc')).toEqual('abX');
        });
        it('mid', () => {
            expect(setAt(1)(X)('abc')).toEqual('aXc');
            expect(setAt(2)(X)('abcd')).toEqual('abXd');
        });
        it('replacement longer than one character', () => {
            expect(setAt(1)('XY')('abc')).toEqual('aXYc');
        });
        it('empty replacement', () => {
            expect(setAt(1)('')('abc')).toEqual('ac');
        });
        it('out of bounds', () => {
            expect(() => setAt(1)(X)('a')).toThrow();
        });
    });
    it('invalid input type', () => {
        expect(() => setAt(1)(true)(undefined as any)).toThrow();
    });
    it('typecheck', () => {
        const a: Array<number> = setAt(0)(7)([1]);
        expect(a).toEqual([7]);
        const b: string = setAt(1)('X')('abc');
        expect(b).toEqual('aXc');
    });
});
