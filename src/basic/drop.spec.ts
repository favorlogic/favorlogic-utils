/* eslint-disable @typescript-eslint/no-unused-vars */
import {drop} from './drop';

describe('drop', () => {
    it('drops from array', () => {
        expect(drop(0)([0, 1, 2])).toEqual([0, 1, 2]);
        expect(drop(2)([])).toEqual([]);
        expect(drop(2)([0, 1, 2])).toEqual([2]);
        const x: Array<unknown> = drop(2)([]);
        // @ts-expect-error array should not produce string
        const y: string = drop(2)([]);
    });
    it('drops from string', () => {
        expect(drop(0)('abc')).toEqual('abc');
        expect(drop(2)('')).toEqual('');
        expect(drop(2)('abc')).toEqual('c');
        const x: string = drop(2)('');
        // @ts-expect-error string should not produce array
        const y: Array<unknown> = drop(2)('');
    });
});
