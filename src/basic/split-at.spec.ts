/* eslint-disable @typescript-eslint/no-unused-vars */
import {splitAt} from './split-at';

describe('splitAt', () => {
    it('splits arrays', () => {
        expect(splitAt(1)([])).toEqual([[], []]);
        expect(splitAt(1)([0, 1, 2])).toEqual([[0], [1, 2]]);
        expect(splitAt(0)([0, 1, 2])).toEqual([[], [0, 1, 2]]);
        expect(splitAt(3)([0, 1, 2])).toEqual([[0, 1, 2], []]);
        expect(splitAt(10)([0, 1, 2])).toEqual([[0, 1, 2], []]);
        const x: [Array<unknown>, Array<unknown>] = splitAt(1)([]);
        // @ts-expect-error array should not produce string
        const y: [string, string] = splitAt(1)([]);
    });
    it('splits strings', () => {
        expect(splitAt(1)('')).toEqual(['', '']);
        expect(splitAt(1)('abc')).toEqual(['a', 'bc']);
        expect(splitAt(0)('abc')).toEqual(['', 'abc']);
        expect(splitAt(3)('abc')).toEqual(['abc', '']);
        expect(splitAt(10)('abc')).toEqual(['abc', '']);
        const x: [string, string] = splitAt(1)('');
        // @ts-expect-error string should not produce array
        const y: [Array<unknown>, Array<unknown>] = splitAt(1)('');
    });
});
