import {all} from 'lodash/fp';

import {isArray, isObject} from '../lodash-typed';

export const isArrayOfObjects = (x: unknown): x is Array<object> => isArray(x) && all(isObject, x);
