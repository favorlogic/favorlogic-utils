import {padStart} from './pad-start';

describe('padStart', () => {
    it('pads with default padding', () => {
        expect(padStart()(0)('0')).toEqual('0');
        expect(padStart()(3)('0')).toEqual('  0');
    });
    it('pads with custom padding', () => {
        expect(padStart('-')(0)('0')).toEqual('0');
        expect(padStart('-')(3)('0')).toEqual('--0');
    });
});
