import {isFinite, isNumber} from 'lodash/fp';

export const isFiniteNumber = (x: unknown): x is number =>
    isNumber(x) && isFinite(x);
