import {isEmpty, reduce} from 'lodash/fp';

const nonEmptyCase = <A>(sep: A, xs: Array<A>): Array<A> =>
    reduce((acc: Array<A> | null, x: A): Array<A> => acc ? [...acc, sep, x] : [x], null)(xs) as Array<A>;

export const intersperse = <A>(sep: A) => (xs: Array<A>): Array<A> => isEmpty(xs) ? [] : nonEmptyCase(sep, xs);
