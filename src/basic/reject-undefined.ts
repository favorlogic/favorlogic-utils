import {reject} from '../lodash-typed';

export function rejectUndefined<T>(xs: Array<T | undefined>): Array<T> {
    return reject<T | undefined>(x => x === undefined)(xs) as Array<T>;
}
