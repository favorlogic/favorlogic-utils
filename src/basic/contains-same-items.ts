/* eslint-disable @fl/use-eta-reduction */

export function containsSameItems(first: Array<unknown>, second: Array<unknown>): boolean {
    return first.every(x => second.includes(x))
        && second.every(x => first.includes(x));
}
