export function toArray<T>(value: T | Array<T> | null | undefined): Array<T> {
    if (value === undefined || value === null) {
        return [];
    }

    return Array.isArray(value) ? value : [value];
}
