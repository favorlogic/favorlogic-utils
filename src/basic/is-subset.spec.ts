import {isSubset} from './is-subset';

describe('isSubset', () => {
    it('empty', () => {
        expect(isSubset([], [])).toEqual(true);
        expect(isSubset([], [1])).toEqual(true);
    });

    it('equal', () => {
        expect(isSubset([1, 2], [2, 1])).toEqual(true);
    });

    it('subset', () => {
        expect(isSubset([1, 2], [3, 2, 1])).toEqual(true);
        expect(isSubset([1, 2], [3, 1])).toEqual(false);
    });
});
