import {id} from 'ts-opt';

import {orderByDesc} from './order-by-desc';

interface A {
    id: number;
    name: string;
}

interface Test {id: number}

describe('order-by-desc', () => {
    it('sorts descending', () => {
        expect(orderByDesc(id)([2, 1, 3])).toEqual([3, 2, 1]);
        const testData: Array<A> = [
            {id: 1, name: 'z'},
            {id: 0, name: 'a'},
            {id: -1, name: 'g'},
        ];
        expect(orderByDesc<A>(x => x.id)(testData)).toEqual([
            {id: 1, name: 'z'},
            {id: 0, name: 'a'},
            {id: -1, name: 'g'},
        ]);
        expect(orderByDesc<A>(x => x.name)(testData)).toEqual([
            {id: 1, name: 'z'},
            {id: -1, name: 'g'},
            {id: 0, name: 'a'},
        ]);
        expect(orderByDesc<Test>(x => x.id)([{id: 1}, {id: 0}, {id: -1}])).toEqual([{id: 1}, {id: 0}, {id: -1}]);
    });
});
