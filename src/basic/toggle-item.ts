import {setItem} from './set-item';

/**
 * Allows working with an array as a set.
 * When {@param item} is not present in given array, {@param item} will be appended to the input array.
 * When {@param item} is present in given array, all instances of {@param item} will be removed from the input array.
 * Immutable.
 *
* @example
 * ```ts
 * toggleItem(2)([0, 1]) // [0, 1, 2]
 * toggleItem(0)([0, 1]) // [1]
 * toggleItem(0)([0, 1, 0, 0]) // [1]
 * ```
 *
 * @param item
 */
// eslint-disable-next-line @fl/use-eta-reduction
export const toggleItem = <T>(item: T) => (xs: Array<T>): Array<T> => setItem(!xs.includes(item))(item)(xs);
