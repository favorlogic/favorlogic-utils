import {isArray, isEmpty, isString} from 'lodash/fp';

import {drop} from './drop';
import {splitAt} from './split-at';

/**
 * Set (replace) an item on a given index at array/string.
 * Throws on empty input or invalid index.
 * Immutable.
 *
 * @example
 * ```ts
 * setAt(0)(true)([false, false]) // [true, false]
 * setAt(2)('X')('abc') // 'abX'
 * setAt(1)('')('abc') // 'ac'
 * setAt(1)('XY')('abc') // 'aXYc'
 * ```
 *
 * @param idx
 */
export const setAt =
    (idx: number) =>
        <T>(x: T) =>
            <I, R extends (I extends string ? string : (I extends Array<T> ? Array<T> : never))>(xs: I): R => {
                if (isArray(xs) || isString(xs)) {
                    if (isEmpty(xs)) throw new Error('Empty array or string.');
                    if (idx < 0 || idx >= xs.length) throw new Error('Invalid index.');
                    if (isArray(xs)) {
                        return xs.map((y: T, i: number) => i === idx ? x : y) as R;
                    } else if (isString(xs) && isString(x)) {
                        const [before, afterIncluding] = splitAt(idx)(xs);
                        const after = drop(1)(afterIncluding);

                        return before + x + after as R;
                    }
                }
                throw new Error('Only arrays and strings are supported.');
            };
