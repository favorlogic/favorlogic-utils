import {split} from 'fp-ts/string';
import {tail} from 'lodash/fp';
import {flow, head} from 'ts-opt';

// TODO: should be removed once all libraries have been migrated to the npm registry
export const extractVersion = flow(split('#'), tail, head);
