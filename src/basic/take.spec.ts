/* eslint-disable @typescript-eslint/no-unused-vars */
import {take} from './take';

describe('take', () => {
    it('takes from array', () => {
        expect(take(0)([0, 1, 2])).toEqual([]);
        expect(take(2)([])).toEqual([]);
        expect(take(2)([0, 1, 2])).toEqual([0, 1]);
        const x: Array<unknown> = take(2)([]);
        // @ts-expect-error array should not produce string
        const y: string = take(2)([]);
    });
    it('takes from string', () => {
        expect(take(0)('abc')).toEqual('');
        expect(take(2)('')).toEqual('');
        expect(take(2)('abc')).toEqual('ab');
        const x: string = take(2)('');
        // @ts-expect-error string should not produce array
        const y: Array<unknown> = take(2)('');
    });
});
