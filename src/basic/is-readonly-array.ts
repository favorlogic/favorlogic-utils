import {isArray} from 'lodash/fp';

// eslint-disable-next-line @fl/use-eta-reduction
export const isReadonlyArray = (x: unknown): x is ReadonlyArray<unknown> => isArray(x);
