import {Opt, opt} from 'ts-opt';

export const convertStringToFloat = (x: string = ''): Opt<number> =>
    opt(Number.parseFloat(x.replace(',', '.')));
