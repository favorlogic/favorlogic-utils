import {zip as _zip, reject} from 'lodash/fp';

// import {reject} from '../lodash';

/**
 * Similar to zip from lodash, but doesn't inject undefined when inputs have different length.
 */
export const zip = <T, U>(xs: Array<T>) => (ys: Array<U>): Array<[T, U]> =>
    reject<[T | undefined, U | undefined]>(
        ([a, b]) => a === undefined || b === undefined,
    )(_zip(xs, ys)) as Array<[T, U]>;
