import {isObject} from 'lodash/fp';

import {UnknownObject} from '../types';

// eslint-disable-next-line @fl/use-eta-reduction
export const isUnknownObject = (x: unknown): x is UnknownObject => isObject(x);
