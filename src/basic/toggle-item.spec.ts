import {toggleItem} from './toggle-item';

describe('toggleItem', () => {
    it('appends the item when the item is not already present', () => {
        expect(toggleItem(2)([0, 1])).toEqual([0, 1, 2]);
    });
    it('removes the item when the item is present', () => {
        expect(toggleItem(0)([0, 1])).toEqual([1]);
    });
    it('removes all instances of the item when the item is present multiple times', () => {
        expect(toggleItem(0)([0, 1, 0, 0])).toEqual([1]);
    });
});
