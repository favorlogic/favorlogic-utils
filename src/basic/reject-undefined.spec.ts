import {rejectUndefined} from './reject-undefined';

const f = rejectUndefined;
describe('rejectUndefined', () => {
    it('empty', () => {
        expect(f([])).toEqual([]);
    });

    it('filters out undefined', () => {
        const xs = [0, undefined, 2];
        const ys: Array<number> = f(xs);
        const ysExpected = [0, 2];
        expect(ys).toEqual(ysExpected);
    });
});
