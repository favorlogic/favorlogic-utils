import {chunksOf} from './chunks-of';

const f = chunksOf;

describe('chunksOf', () => {
    it('empty', () => {
        expect(f(1)([])).toEqual([]);
    });

    it('exactly one group', () => {
        expect(f(2)([1, 2])).toEqual([[1, 2]]);
    });

    it('one group', () => {
        expect(f(3)([1, 2])).toEqual([[1, 2]]);
    });

    it('more groups', () => {
        expect(f(2)([1, 2, 3, 4, 5])).toEqual([[1, 2], [3, 4], [5]]);
    });

    it('negative length', () => {
        expect(f(-1)([1, 2])).toEqual([]);
    });
});
