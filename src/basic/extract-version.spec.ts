import {extractVersion} from './extract-version';

describe('correct version', () => {
    const correctLibraryVersion = 'git+https://git@bitbucket.org/favorlogic/favorlogic-utils.git#v0.10.2';
    const correctVersionExtraction = 'v0.10.2';

    it('correct extraction', () => {
        expect(extractVersion(correctLibraryVersion).orCrash('Failed to extract fl-utils version'))
            .toEqual(correctVersionExtraction);
    });
});
