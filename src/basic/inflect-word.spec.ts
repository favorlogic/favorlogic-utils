import {inflectWord} from './inflect-word';

describe('inflectWord', () => {
    it('returns correct word', () => {
        const firstLevelWord = 'kolo';
        const secondLevelWord = 'kola';
        const thirdLevelWord = 'kol';

        const inflectWordCall = (length: number) =>
            inflectWord(firstLevelWord, secondLevelWord, thirdLevelWord, length);

        expect(inflectWordCall(-50)).toBe(thirdLevelWord);
        expect(inflectWordCall(-5)).toBe(thirdLevelWord);

        expect(inflectWordCall(-4)).toBe(secondLevelWord);
        expect(inflectWordCall(-2)).toBe(secondLevelWord);

        expect(inflectWordCall(-1)).toBe(firstLevelWord);

        expect(inflectWordCall(0)).toBe(thirdLevelWord);

        expect(inflectWordCall(1)).toBe(firstLevelWord);

        expect(inflectWordCall(2)).toBe(secondLevelWord);
        expect(inflectWordCall(4)).toBe(secondLevelWord);

        expect(inflectWordCall(5)).toBe(thirdLevelWord);
        expect(inflectWordCall(50)).toBe(thirdLevelWord);
    });
});
