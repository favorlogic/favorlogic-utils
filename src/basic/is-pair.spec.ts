import {isPair} from './is-pair';

describe('isPair', () => {
    it('isPair', () => {
        expect(isPair(['', ''])).toBe(true);
    });
    it('isPair', () => {
        expect(isPair([1, 1])).toBe(true);
    });
    it('isPair', () => {
        expect(isPair(['test', 'rest'])).toBe(true);
    });
    it('isPair', () => {
        expect(isPair([1, 'rest'])).toBe(true);
    });
    it('isEmptyArray', () => {
        expect(isPair([])).toBe(false);
    });
    it('isExcessiveArrayLength', () => {
        expect(isPair([1, 2, 3])).toBe(false);
    });
    it('isInsufficientArrayLength', () => {
        expect(isPair(['test'])).toBe(false);
    });
    it('compare types', () => {
        const a: Array<number> = [1, 2];
        if (isPair(a)) {
            const b: [number, number] = a;
            expect(b).toBeTruthy();
        }
    });
});
