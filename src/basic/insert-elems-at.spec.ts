/* eslint-disable @typescript-eslint/no-unused-vars */
import {insertElemsAt} from './insert-elems-at';

describe('insertElemsAt', () => {
    it('inserts array into array', () => {
        expect(insertElemsAt(1)([])([])).toEqual([]);
        expect(insertElemsAt(1)([1, 3])([2])).toEqual([1, 2, 3]);
        expect(insertElemsAt(0)([1, 3])([2])).toEqual([2, 1, 3]);
        expect(insertElemsAt(10)([1, 3])([2])).toEqual([1, 3, 2]);
        const x: Array<unknown> = insertElemsAt(0)([])([]);
        // @ts-expect-error array should not produce string
        const y: string = insertElemsAt(0)([])([]);
    });
    it('inserts string into string', () => {
        expect(insertElemsAt(1)('')('')).toEqual('');
        expect(insertElemsAt(1)('ac')('b')).toEqual('abc');
        expect(insertElemsAt(0)('ac')('b')).toEqual('bac');
        expect(insertElemsAt(10)('ac')('b')).toEqual('acb');
        const x: string = insertElemsAt(0)('')('');
        // @ts-expect-error string should not produce array
        const y: Array<unknown> = insertElemsAt(0)('')('');
    });
});
