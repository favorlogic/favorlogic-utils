import {stringTuple} from './string-tuple';

describe('stringTuple', () => {
    it('strings to array', () => {
        expect(stringTuple('item1', 'item2', 'item3')).toEqual(['item1', 'item2', 'item3']);
    });

    it('no parameters to empty array', () => {
        expect(stringTuple()).toEqual([]);
    });

    it('compare types', () => {
        const x: ['a', 'b'] = stringTuple('a', 'b'); // if will be compiled successfully, test passed
        expect(x).toBeTruthy();
    });
});
