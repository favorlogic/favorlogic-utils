export const truncateString = (value: string, maxChars: number): string => {
    if (value.length <= maxChars) {
        return value;
    }

    return `${value.slice(0, maxChars)}...`;
};
