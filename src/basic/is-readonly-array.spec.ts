import {isReadonlyArray} from './is-readonly-array';

describe('isReadonlyArray', () => {
    it('should work', () => {
        expect(isReadonlyArray(null)).toBe(false);
        expect(isReadonlyArray(undefined)).toBe(false);
        expect(isReadonlyArray(1)).toBe(false);
        expect(isReadonlyArray('a')).toBe(false);
        expect(isReadonlyArray(true)).toBe(false);
        expect(isReadonlyArray({})).toBe(false);

        expect(isReadonlyArray([])).toBe(true);
        expect(isReadonlyArray([1])).toBe(true);
    });
});
