import {map} from 'lodash/fp';

export const setBy = <T>(fieldName: keyof T) => (x: T) => (xs: Array<T>): Array<T> =>
    map(a => a[fieldName] === x[fieldName] ? x : a, xs);

export const setById = setBy('id');
