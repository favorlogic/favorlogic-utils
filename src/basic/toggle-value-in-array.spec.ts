/* eslint-disable deprecation/deprecation */
import {toggleValueInArray} from './toggle-value-in-array';

describe('toggleValueInArray', () => {
    it('should remove value if exists in array', () => {
        expect(toggleValueInArray([1, 2, 3], 2)).toEqual([1, 3]);
    });

    it('should add value if not exists in array', () => {
        expect(toggleValueInArray([1, 2], 3)).toEqual([1, 2, 3]);
    });
});
