import {concat, take} from 'lodash/fp';

export const limitStringArrayWithEllipses = (maxItems: number) => (xs: Array<string>): Array<string> =>
    xs.length > maxItems ? concat(take(maxItems, xs), '…') : xs;
