import {validateKey, validateKeyGen} from './validate-key';

interface Test {
    a: number;
}

describe('validateKey', () => {
    it('pos', () => {
        expect(validateKey<Test>('a')).toEqual('a');
    });

    it('neg', () => {
        // @ts-expect-error should not compile
        validateKey<Test>('b');
    });
});

describe('validateKeyGen', () => {
    const validate = validateKeyGen<Test>();

    it('pos', () => {
        expect(validate('a')).toEqual('a');
    });

    it('neg', () => {
        // @ts-expect-error should not compile
        validate('b');
    });
});

