import {orderBy} from 'lodash/fp';

/**
 * Sort an array in descending order based on given mapping function.
 * Immutable.
 * @example
 * ```ts
 * orderByDesc(id)([2, 1, 3]) // [3, 2, 1]
 *
 * interface Test {id: number;}
 * orderByDesc<Test>(x => x.id)([{id: 1}, {id: 0}, {id: -1}]) // [{id: 1}, {id: 0}, {id: -1}]
 * ```
 * @param f
 */
export const orderByDesc = <T>(f: (_: T) => unknown) => (xs: Array<T>): Array<T> => orderBy(f, 'desc', xs);
