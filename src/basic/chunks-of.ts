import {concat, drop, take} from 'lodash/fp';

export const chunksOf = (length: number) => <T>(arr: Array<T>): Array<Array<T>> => {
    const loop = (xs: Array<T>, res: Array<Array<T>>): Array<Array<T>> => {
        const group = take(length)(xs);

        return group.length === 0 ? res : loop(drop(length)(xs), concat(res)([take(length)(xs)]));
    };

    return loop(arr, []);
};
