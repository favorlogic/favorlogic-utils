// TODO make it curried with object as second call
export const pick = <T extends object, K extends Array<keyof T>>(object: T, keysToPick: K): {
    [K2 in Extract<keyof T, K[number]>]: T[K2]
} => (Object.keys(object) as K)
    // eslint-disable-next-line @fl/use-eta-reduction
    .filter(x => keysToPick.includes(x))
    .reduce(
        (acc, currKey) => ({...acc, [currKey]: object[currKey]}),
        {} as {[K1 in keyof typeof object]: typeof object[K1]},
    );
