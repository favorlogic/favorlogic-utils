interface TypedMemoize {
    <A1, Result>(fn: (arg1: A1) => Result): (arg1: A1) => Result;
    <A1, A2, Result>(fn: (arg1: A1, arg2: A2) => Result): (arg1: A1, arg2: A2) => Result;
    <A1, A2, A3, Result>(fn: (arg1: A1, arg2: A2, arg3: A3) => Result): (arg1: A1, arg2: A2, arg3: A3) => Result;
    <A1, A2, A3, A4, Result>(fn: (arg1: A1, arg2: A2, arg3: A3, arg4: A4) => Result):
    (arg1: A1, arg2: A2, arg3: A3, arg4: A4) => Result;
    <A1, A2, A3, A4, A5, Result>(fn: (arg1: A1, arg2: A2, arg3: A3, arg4: A4, arg5: A5) => Result):
    (arg1: A1, arg2: A2, arg3: A3, arg4: A4, arg5: A5) => Result;
}

function sameValuesInArray(a: Array<unknown>, b: Array<unknown>): boolean {
    if (a.length !== b.length) {
        return false;
    }
    for (let i = 0; i < a.length; i++) {
        if (a[i] !== b[i]) {
            return false;
        }
    }

    return true;
}

export const memoize: TypedMemoize = <Result>(fnToMemoize: (...args: Array<unknown>) => Result) => {
    let previousArgs: Array<unknown> | undefined;
    let cachedValue: Result;

    return (...args: Array<unknown>): Result => {
        if (!previousArgs || !sameValuesInArray(args, previousArgs)) {
            previousArgs = args;
            cachedValue = fnToMemoize(...args);
        }

        return cachedValue;
    };
};
