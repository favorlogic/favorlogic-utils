import {setItem} from './set-item';

describe('setItem', () => {
    it('appends the item when addOrRemove is true', () => {
        expect(setItem(true)(2)([0, 1])).toEqual([0, 1, 2]);
        expect(setItem(true)(2)([])).toEqual([2]);
    });
    it(`doesn't append the item when addOrRemove is true and the item is already present`, () => {
        expect(setItem(true)(0)([0, 1])).toEqual([0, 1]);
    });

    it('removes the item when addOrRemove is false', () => {
        expect(setItem(false)(0)([0, 1])).toEqual([1]);
    });
    it(`doesn't remove anything when addOrRemove is false and the item is not already present`, () => {
        expect(setItem(false)(2)([0, 1])).toEqual([0, 1]);
        expect(setItem(false)(2)([])).toEqual([]);
    });
    it('removes all instances of the item when addOrRemove is false and the item is present multiple times', () => {
        expect(setItem(false)(0)([0, 1, 0, 0])).toEqual([1]);
    });
    it('typecheck', () => {
        // @ts-expect-error item and xs must be of same type
        expect(setItem(true)('a')([false])).toEqual([false, 'a']);
    });
});
