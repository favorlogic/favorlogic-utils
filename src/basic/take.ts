import {take as _take, isString, join, identity} from 'lodash/fp';
import {pipe} from 'ts-opt';

/**
 * Take {@param n} first elements from an array, or {@param n} characters from a string.
 * In the case of a string input, the output is also a string.
 *
 * @example
 * ```ts
 * take(2)([0, 1, 2]) // [0, 1]
 * take(2)('abc') // 'ab'
 * ```
 *
 * @param n
 */
export const take =
    (n: number) => <T = unknown, R extends string | Array<T> = string | Array<T>>(xs: R): R =>
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        pipe(xs, _take(n) as any, isString(xs) ? join('') : identity) as R;
