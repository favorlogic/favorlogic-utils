export const stringTuple = <T extends [string] | Array<string>>(...data: T): T => data;
