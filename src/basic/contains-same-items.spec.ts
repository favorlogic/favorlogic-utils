import {containsSameItems} from './contains-same-items';

describe('containsSameItems', () => {
    const mainArray = ['1', '2', '3'];
    const arr1 = ['3', '1', '2'];
    const arr2 = ['3', '1', '0'];
    const arr3 = ['1', '2', '3', '4'];
    const arr4 = ['1', '2'];
    const arr5 = [1, 2, 3];
    const arr6: Array<unknown> = [];
    const arr7 = [undefined];

    it('containsSameItems', () => {
        expect(containsSameItems(mainArray, arr1)).toBe(true);
    });
    it('containsSameItems', () => {
        expect(containsSameItems(mainArray, arr2)).toBe(false);
    });
    it('containsSameItems', () => {
        expect(containsSameItems(mainArray, arr3)).toBe(false);
    });
    it('containsSameItems', () => {
        expect(containsSameItems(mainArray, arr4)).toBe(false);
    });
    it('containsSameItems', () => {
        expect(containsSameItems(mainArray, arr5)).toBe(false);
    });
    it('containsSameItems', () => {
        expect(containsSameItems(mainArray, arr6)).toBe(false);
    });
    it('containsSameItems', () => {
        expect(containsSameItems(mainArray, arr7)).toBe(false);
    });
});
