import {memoize} from './memoize';

type FnType = (a: {val: string}, b: number, c?: string) => string;

const fn: FnType = (obj: {val: string}, num: number, extra?: string) => obj.val + String(num) + (extra || '');

describe('memoize', () => {
    it('returns last memoized value if same arguments or computes it', () => {
        const mockFn = jest.fn(fn);
        const memoizedFn: FnType = memoize(mockFn);

        const obj1 = {val: 'foo'};
        const obj2 = {val: 'bar'};

        expect(memoizedFn(obj1, 0)).toBe('foo0');
        expect(mockFn.mock.calls.length).toBe(1);
        expect(memoizedFn(obj1, 0)).toBe('foo0');
        expect(mockFn.mock.calls.length).toBe(1);

        expect(memoizedFn(obj2, 0)).toBe('bar0');
        expect(mockFn.mock.calls.length).toBe(2);
        expect(memoizedFn(obj2, 0)).toBe('bar0');
        expect(mockFn.mock.calls.length).toBe(2);

        expect(memoizedFn(obj1, 1)).toBe('foo1');
        expect(mockFn.mock.calls.length).toBe(3);

        expect(memoizedFn(obj1, 2)).toBe('foo2');
        expect(mockFn.mock.calls.length).toBe(4);

        expect(memoizedFn(obj1, 1, 'asd')).toBe('foo1asd');
        expect(mockFn.mock.calls.length).toBe(5);
    });
});
