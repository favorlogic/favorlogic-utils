import {removeNullFields, removeNullFieldsRecursively} from './remove-null-fields';

describe('removeNullFields', () => {
    it('empty', () => {
        expect(removeNullFields({})).toEqual({});
    });

    it('non-null', () => {
        expect(removeNullFields({a: 1})).toEqual({a: 1});
    });

    it('removes null', () => {
        expect(removeNullFields({a: null})).toEqual({});
    });
});

describe('removeNullFieldsRecursively', () => {
    const f = removeNullFieldsRecursively;

    it('empty', () => {
        expect(f({})).toEqual({});
        expect(f([])).toEqual([]);
    });

    it('primitives', () => {
        expect(f(0)).toEqual(0);
        expect(f('Alara')).toEqual('Alara');
        expect(f(true)).toEqual(true);
    });

    it('non-null', () => {
        expect(f(['a'])).toEqual(['a']);
        expect(f({a: 1, b: 'b', c: false})).toEqual({a: 1, b: 'b', c: false});
    });

    it('removes null', () => {
        expect(f(['a', null])).toEqual(['a']);
        expect(f({a: 1, b: 'b', c: null})).toEqual({a: 1, b: 'b'});
    });

    it('non-null deep', () => {
        expect(f({a: {x: 1}, b: 'b', c: ['c']})).toEqual({a: {x: 1}, b: 'b', c: ['c']});
    });

    it('removes null deep', () => {
        expect(f({a: 1, b: {c: null, x: false}})).toEqual({a: 1, b: {x: false}});
        expect(f({a: 1, b: ['b', null]})).toEqual({a: 1, b: ['b']});
        expect(f({a: 1, b: [{c: null, x: false}]})).toEqual({a: 1, b: [{x: false}]});
    });
});
