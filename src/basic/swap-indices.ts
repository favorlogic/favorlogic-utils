import {pipe} from 'ts-opt';

import {setAt} from './set-at';

export const swapIndices = ([indexA, indexB]: [number, number]) =>
    <A>(xs: Array<A>): Array<A> => {
        const [valA, valB] = [xs[indexA], xs[indexB]] as [A, A];

        return pipe(
            xs,
            setAt(indexB)(valA),
            setAt(indexA)(valB),
        );
    };
