// TODO make it curried with object as second call
export const omit = <T extends object, K extends Array<keyof T>>(object: T, keysToExclude: K): {
    [K2 in Exclude<keyof T, K[number]>]: T[K2]
} => (Object.keys(object) as K)
    .filter(x => !keysToExclude.includes(x))
    .reduce(
        (acc, currKey) => ({...acc, [currKey]: object[currKey]}),
        {} as {[K1 in keyof typeof object]: typeof object[K1]},
    );
