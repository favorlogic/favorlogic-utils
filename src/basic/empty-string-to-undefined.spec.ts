import {emptyStringToUndefined} from './empty-string-to-undefined';

const f = emptyStringToUndefined;

describe('emptyStringToUndefined', () => {
    it('pos', () => {
        expect(f('')).toEqual(undefined);
    });

    it('neg', () => {
        expect(f('x')).toEqual('x');
    });
});
