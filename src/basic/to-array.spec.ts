import {toArray} from './to-array';

describe('toArray', () => {
    it('string toArray', () => {
        expect(toArray('x')).toEqual(['x']);
    });

    it('strings toArray', () => {
        expect(toArray(['x', 'y', 'z'])).toEqual(['x', 'y', 'z']);
    });

    it('numbers toArray', () => {
        expect(toArray([1, 2, 3])).toEqual([1, 2, 3]);
    });

    it('null toArray', () => {
        expect(toArray(null)).toEqual([]);
    });

    it('undefined toArray', () => {
        expect(toArray(undefined)).toEqual([]);
    });

    it('number toArray', () => {
        expect(toArray(5)).toEqual([5]);
    });

    it('object toArray', () => {
        expect(toArray({a: 5})).toEqual([{a: 5}]);
    });
});
