import {isUnknownObject} from './is-unknown-object';

describe('isUnknownObject', () => {
    it('is object', () => {
        expect(isUnknownObject({})).toBe(true);
        expect(isUnknownObject({test: 'test'})).toBe(true);
        expect(isUnknownObject([0, 1])).toBe(true);
    });
    it('is not object', () => {
        expect(isUnknownObject('test')).toBe(false);
        expect(isUnknownObject(123)).toBe(false);
    });
});
