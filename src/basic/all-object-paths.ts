import {toPairs, isObject, map, flatMap, isEmpty, isArray, sortBy, identity} from 'lodash/fp';
import {pipe} from 'ts-opt';

export const allObjectPaths = (obj: object): Array<string> => pipe(
    obj,
    toPairs,
    flatMap(([k, v]: [string, unknown]) =>
        (isObject(v) && !isEmpty(v) && !isArray(v) ? map(a => `${k}.${a}`, allObjectPaths(v)) : [k])),
    sortBy(identity),
);
