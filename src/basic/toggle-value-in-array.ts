import {without, concat} from 'lodash/fp';

/** @deprecated use {@link toggleItem} */
export function toggleValueInArray<T>(list: Array<T>, value: T): Array<T> {
    if (list.includes(value)) {
        return without([value])(list);
    } else {
        return concat(list)([value]);
    }
}
