import {setBy, setById} from './set-by';

interface T {
    a: number;
    b: string;
}

interface U {
    id: number;
    b: string;
}

describe('setBy', () => {
    const f = setBy;

    it('empty', () => {
        const z: T = {a: 1, b: 'z'};
        expect(f('a')(z)([])).toEqual([]);
    });

    it('replace', () => {
        const x: T = {a: 0, b: 'x'};
        const y: T = {a: 1, b: 'y'};
        const z: T = {a: 1, b: 'z'};
        expect(f('a')(z)([x, y])).toEqual([{a: 0, b: 'x'}, {a: 1, b: 'z'}]);
    });
});

describe('setById', () => {
    const f = setById;

    it('replace', () => {
        const x: U = {id: 0, b: 'x'};
        const y: U = {id: 1, b: 'y'};
        const z: U = {id: 1, b: 'z'};
        expect(f(z)([x, y])).toEqual([{id: 0, b: 'x'}, {id: 1, b: 'z'}]);
    });
});
