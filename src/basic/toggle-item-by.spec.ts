import {toggleItemBy} from './toggle-item-by';

describe('toggleItemBy', () => {
    interface Num {
        id: number;
        name: string;
    }

    const gen0 = (): Num => ({id: 0, name: 'zero'});
    const gen1 = (): Num => ({id: 1, name: 'one'});

    it('appends the item when the item is not already present', () => {
        expect(toggleItemBy('id')(gen1())([gen0()])).toEqual([gen0(), gen1()]);
    });
    it('removes the item when the item is present', () => {
        expect(toggleItemBy('id')(gen1())([gen0(), gen1()])).toEqual([gen0()]);
    });
    it('removes all instances of the item when the item is present multiple times', () => {
        expect(toggleItemBy('id')(gen0())([gen0(), gen1(), gen0(), gen0()])).toEqual([gen1()]);
    });
    it('example', () => {
        const zero = {id: 0, name: 'zero'};
        const one = {id: 1, name: 'one'};
        expect(toggleItemBy('id')(one)([zero])).toEqual([zero, one]);
        expect(toggleItemBy('name')(one)([zero, one])).toEqual([zero]);
    });
});
