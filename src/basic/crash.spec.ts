import {crash} from './crash';

describe('crash', () => {
    it('crashes', () => {
        expect(() => crash('Red Alert!')).toThrowError('Red Alert!');
    });
});
