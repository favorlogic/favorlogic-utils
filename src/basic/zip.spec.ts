import {zip} from './zip';

describe('zip', () => {
    it('zips values', () => {
        expect(zip([])([])).toEqual([]);
        expect(zip([1])([])).toEqual([]);
        expect(zip([])([1])).toEqual([]);
        expect(zip([1])([2])).toEqual([[1, 2]]);
        expect(zip([1, 2])([10, 20])).toEqual([[1, 10], [2, 20]]);
        expect(zip([1, 2, 3, 4, 5])([10, 20])).toEqual([[1, 10], [2, 20]]);
    });
});
