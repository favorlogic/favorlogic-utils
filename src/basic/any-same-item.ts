import {intersection} from 'lodash/fp';
import {nonEmpty} from 'ts-opt';

/**
 * Returns true if both input arrays contain at least one common element,
 * otherwise returns false.
 *
 * @template T
 * @param {Array<T>} xs - The first input array.
 * @param {Array<T>} ys - The second input array.
 * @returns {boolean} - True if both arrays contain at least one common element, otherwise false.
 * Default comparator function is `===` (strict equality).
 *
 * @example
 * ```ts
 * anySameItem([1, 2, 3])([2, 4, 4, 5]); // returns true
 * anySameItem(['apple', 'banana'])(['cherry', 'banana']); // returns true
 * anySameItem([1, 2, 3])([4, 5, 6]) // returns false
 * anySameItem(['1', '2', '3'])([1, 2, 3]) // returns false
 * ```
 */
export const anySameItem = <T>(xs: Array<T>) => (ys: Array<T>): boolean => nonEmpty(intersection(xs, ys));
