import {appendUniq} from './append-uniq';
import {without} from '../lodash-typed';

/**
 * Allows working with an array as a set.
 * When {@param addOrRemove} is set to `true`, `item` is appended to the input array (unless already present).
 * When {@param addOrRemove} is set to `false`, `item` is removed from the input array (all instances of the item).
 * Immutable.
 *
 * It is meant to shorten a common pattern:
 *   `cond ? appendUniq(id)(xs) : without([id])(xs)` → `setItem(cond)(id)(xs)`
 *
 * @example
 * ```ts
 * setItem(true)(2)([0, 1]) // [0, 1, 2]
 * setItem(false)(0)([0, 1]) // [1]
 * setItem(false)(0)([0, 1, 0, 0]) // [1]
 * ```
 *
 * @param addOrRemove
 */
export const setItem =
    (addOrRemove: boolean): <T>(item: T) => (xs: Array<T>) => Array<T> => addOrRemove ? appendUniq : without;
