import {swapIndices} from './swap-indices';

describe('swapIndices', () => {
    it('should swap indices', () => {
        expect(swapIndices([0, 2])([1, 2, 3, 4])).toEqual([3, 2, 1, 4]);
    });
});
