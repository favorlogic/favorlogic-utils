import {map, pickBy, mapValues} from 'lodash/fp';
import {pipe} from 'ts-opt';

import {isArray, isObject, filter} from '../lodash-typed';

/**
 * Removes properties with value of `null`.
 * Non-recursive, if you want recursive behaviour use {@link removeNullFieldsRecursively}.
 * @param obj - Input object.
 */
export const removeNullFields = <T extends object>(obj: T): Partial<T> =>
    pickBy<T>((x: unknown) => x !== null, obj);

/**
 * Removes properties with value of `null` and null values in an array, recursively.
 * If other type than object or array is passed, it will be returned without any change.
 * Non-recursive variant is {@link removeNullFields}.
 * @param x
 */
export const removeNullFieldsRecursively = (x: unknown): unknown => {
    if (isArray(x)) {
        return pipe(
            x,
            filter(y => y !== null),
            map(removeNullFieldsRecursively),
        );
    } else if (isObject(x)) {
        return pipe(
            x,
            pickBy(y => y !== null),
            mapValues(removeNullFieldsRecursively),
        );
    } else {
        return x;
    }
};
