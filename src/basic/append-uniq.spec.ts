import {appendUniq} from './append-uniq';

describe('appendUniq', () => {
    it('adds element if not present and returns same array if present', () => {
        expect(appendUniq(4)([1, 2, 3, 5, 6])).toEqual([1, 2, 3, 5, 6, 4]);

        const arrayWithElem = [1, 2, 3, 4, 5, 6];
        expect(appendUniq(4)(arrayWithElem)).toBe(arrayWithElem);
    });
});
