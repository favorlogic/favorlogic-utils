import {isStringArray} from './is-string-array';

const num1 = 1;
const num2 = 2;
const num3 = 3;

describe('isStringArray', () => {
    it('isStringArray', () => {
        const arr = ['1', '2', '3'];
        expect(isStringArray(arr)).toBe(true);
    });

    it('isOneStringItemArray', () => {
        const arr = ['1'];
        expect(isStringArray(arr)).toBe(true);
    });

    it('isEmptyArray', () => {
        const arr: Array<string> = [];
        expect(isStringArray(arr)).toBe(true);
    });

    it('isNotStringArray', () => {
        const arr = [num1, num2, num3];
        expect(isStringArray(arr)).toBe(false);
    });
});
