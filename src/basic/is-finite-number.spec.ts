import {isFiniteNumber} from './is-finite-number';

describe('isFiniteNumber', () => {
    it('is finite number', () => {
        expect(isFiniteNumber(0)).toBe(true);
        expect(isFiniteNumber(888)).toBe(true);
        expect(isFiniteNumber(-111)).toBe(true);
        expect(isFiniteNumber(5.789)).toBe(true);
    });
    it('is not finite number', () => {
        expect(isFiniteNumber('0')).toBe(false);
        expect(isFiniteNumber(NaN)).toBe(false);
        expect(isFiniteNumber(Number.POSITIVE_INFINITY)).toBe(false);
        expect(isFiniteNumber(Number.NEGATIVE_INFINITY)).toBe(false);
    });
});
