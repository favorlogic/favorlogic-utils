import {isNumberArray} from './is-number-array';

describe('isNumberArray', () => {
    it('isOneNumberItemArray', () => {
        expect(isNumberArray([1])).toBe(true);
    });

    it('isNumberArray', () => {
        expect(isNumberArray([1, 2, 3])).toBe(true);
    });

    it('isEmptyArray', () => {
        expect(isNumberArray([])).toBe(true);
    });

    it('isStringNumberArray', () => {
        expect(isNumberArray(['1', '2', '3'])).toBe(false);
    });
});
