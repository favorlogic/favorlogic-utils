export const emptyStringToUndefined = (x: string | undefined): string | undefined => x === '' ? undefined : x;
