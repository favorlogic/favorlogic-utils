// eslint-disable-next-line no-restricted-imports
import {padEnd as _padEnd} from 'lodash';

/**
 * Similar to `padEnd` from `lodash/fp`, but also supports custom padding.
 *
 * @example
 * ```ts
 * padEnd()(3)('0') // '0  '
 * padEnd('-')(3)('0') // '0--'
 * ```
 *
 * @param padding
 */
export const padEnd =
    (padding: string = ' ') => (length: number) => (x: string): string =>
        _padEnd(x, length, padding);
