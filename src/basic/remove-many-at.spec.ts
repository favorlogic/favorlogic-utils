/* eslint-disable @typescript-eslint/no-unused-vars */
import {removeManyAt} from './remove-many-at';

describe('removeManyAt', () => {
    it('removes at given position from array', () => {
        expect(removeManyAt(0)(1)([0, 1, 2])).toEqual([1, 2]);
        expect(removeManyAt(1)(2)([0, 1, 2])).toEqual([0]);
        expect(removeManyAt(2)(1)([0, 1, 2])).toEqual([0, 1]);
        expect(removeManyAt(2)(2)([0, 1, 2])).toEqual([0, 1]);
        expect(removeManyAt(3)(5)([0, 1, 2])).toEqual([0, 1, 2]);
        expect(removeManyAt(1)(2)([0, 1, 2, 4])).toEqual([0, 4]);
        // @ts-expect-error array should not produce string
        const y: string = removeManyAt(0)(0)([]);
    });
    it('removes at given position from string', () => {
        expect(removeManyAt(0)(1)('abc')).toEqual('bc');
        expect(removeManyAt(1)(2)('abc')).toEqual('a');
        expect(removeManyAt(2)(5)('abc')).toEqual('ab');
        expect(removeManyAt(3)(5)('abc')).toEqual('abc');
        expect(removeManyAt(2)(3)('abcdef')).toEqual('abf');
        // @ts-expect-error string should not produce array
        const y: Array<unknown> = removeManyAt(0)(0)('');
    });
});
