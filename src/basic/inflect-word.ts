const inflictSecondLevelMinLength = 2;
const inflictThirdLevelMinLength = 5;

export function inflectWord(firstLevel: string, secondLevel: string, thirdLevel: string, length: number): string {
    const absLength = Math.abs(length);
    if (absLength === 0) {
        return thirdLevel;
    } else if (absLength < inflictSecondLevelMinLength) {
        return firstLevel;
    } else if (absLength < inflictThirdLevelMinLength) {
        return secondLevel;
    } else {
        return thirdLevel;
    }
}
