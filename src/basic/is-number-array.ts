import {isArray as _isArray, isNumber} from 'lodash/fp';

export const isNumberArray = (x: unknown): x is Array<number> => _isArray(x) && x.every(isNumber);
