import {drop as _drop, isString, join, identity} from 'lodash/fp';
import {pipe} from 'ts-opt';

/**
 * Drops {@param n} first elements from an array, or {@param n} characters from a string.
 * In the case of a string input, the output is also a string.
 *
 * @example
 * ```ts
 * drop(2)([0, 1, 2]) // [2]
 * drop(2)('abc') // 'c'
 * ```
 *
 * @param n
 */
export const drop =
    (n: number) => <T = unknown, R extends string | Array<T> = string | Array<T>>(xs: R): R =>
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        pipe(xs, _drop(n) as any, isString(xs) ? join('') : identity) as R;
