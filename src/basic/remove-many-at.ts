import {flatten, isString, join} from 'lodash/fp';
import {pipe} from 'ts-opt';

import {drop} from './drop';
import {take} from './take';

interface RemoveManyAt1Fn {
    <T>(xs: Array<T>): Array<T>;

    (xs: string): string;
}

/**
 * Remove `length` of items/characters after first {@param n} items/characters.
 *
 * @example
 * ```ts
 * removeManyAt(1)(2)([0, 1, 2, 4]) // [0, 4]
 * removeManyAt(2)(3)('abcdef') // 'abf'
 * ```
 *
 * @param n
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const removeManyAt: (n: number) => (length: number) => RemoveManyAt1Fn = n => length => (xs: any): any => pipe(
    [take(n)(xs), drop(n + length)(xs)],
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    isString(xs) ? join('') : flatten as any,
);
