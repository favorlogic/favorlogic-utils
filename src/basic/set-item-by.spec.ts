import {setItemBy} from './set-item-by';

describe('setItemBy', () => {
    interface Num {
        id: number;
        name: string;
    }

    const gen0 = (): Num => ({id: 0, name: 'zero'});
    const gen1 = (): Num => ({id: 1, name: 'one'});

    it('appends the item when addOrRemove is true', () => {
        expect(setItemBy(true)('id')(gen0())([])).toEqual([gen0()]);
        expect(setItemBy(true)('id')(gen0())([gen1()])).toEqual([gen1(), gen0()]);
    });

    it(`doesn't append the item when addOrRemove is true and the item is already present`, () => {
        expect(setItemBy(true)('id')(gen0())([gen0()])).toEqual([gen0()]);
    });

    it('removes the item when addOrRemove is false', () => {
        expect(setItemBy(false)('id')(gen0())([gen0(), gen1()])).toEqual([gen1()]);
        expect(setItemBy(false)('name')(gen0())([gen0(), gen1()])).toEqual([gen1()]);
    });

    it(`doesn't remove anything when addOrRemove is false and the item is not already present`, () => {
        expect(setItemBy(false)('id')(gen0())([gen1()])).toEqual([gen1()]);
        expect(setItemBy(false)('id')(gen0())([])).toEqual([]);
    });

    it('removes all instances of the item when addOrRemove is false and the item is present multiple times', () => {
        expect(setItemBy(false)('id')(gen0())([gen0(), gen1(), gen0(), gen0()])).toEqual([gen1()]);
    });

    it('typecheck', () => {
        // @ts-expect-error item and xs must be of same type
        expect(setItemBy(false)('id')(gen0())([false])).toEqual([false]);
        // @ts-expect-error non-existing key
        expect(setItemBy(false)('x')(gen0())([])).toEqual([]);
    });

    it('examples', () => {
        const zero = {id: 0, name: 'zero'};
        const one = {id: 1, name: 'one'};
        expect(setItemBy(true)('id')(zero)([])).toEqual([zero]);
        expect(setItemBy(true)('id')(zero)([one])).toEqual([one, zero]);
        expect(setItemBy(false)('id')(zero)([zero, one])).toEqual([one]);
    });
});
