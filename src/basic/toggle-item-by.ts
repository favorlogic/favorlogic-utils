import {setItemBy} from './set-item-by';

/**
 * Allows working with an array as a set.
 * Objects are considered equal when they have same value in `key` field.
 * When {@param item} is not present in a given array, {@param item} will be appended to the input array.
 * When {@param item} is present in a given array, all instances of {@param item} will be removed from the input array.
 * Immutable.
 *
 * @example
 * ```ts
 * const zero = {id: 0, name: 'zero'};
 * const one = {id: 1, name: 'one'};
 * toggleItemBy('id')(one)([zero]) // [zero, one]
 * toggleItemBy('name')(one)([zero, one]) // [zero]
 * ```
 *
 * @param key
 */
// eslint-disable-next-line @fl/use-eta-reduction
export const toggleItemBy = <T extends object>(key: keyof T) => (item: T) => (xs: Array<T>): Array<T> =>
    setItemBy(!xs.some(x => x[key] === item[key]))(key)(item)(xs);
