export const validateKey = <T extends object, K extends keyof T = keyof T>(x: K): K => x;

export const validateKeyGen = <T extends object>() => <K extends keyof T>(x: K): K => x;
