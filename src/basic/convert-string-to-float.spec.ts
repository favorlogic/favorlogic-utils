import {convertStringToFloat} from './convert-string-to-float';

const f = convertStringToFloat;

describe('convertStringToFloat', () => {
    it('pos', () => {
        expect(f('1').orNull()).toEqual(1);
        expect(f('1.1').orNull()).toEqual(1.1);
    });
    it('neg', () => {
        expect(f().orNull()).toEqual(null);
        expect(f('').orNull()).toEqual(null);
        expect(f('a').orNull()).toEqual(null);
        expect(f(' ').orNull()).toEqual(null);
    });
});
