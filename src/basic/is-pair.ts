const pairLength = 2;

export const isPair = <T>(x: Array<T>): x is [T, T] => x.length === pairLength;
