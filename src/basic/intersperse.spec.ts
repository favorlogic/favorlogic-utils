import {intersperse} from './intersperse';

describe('intersperse', () => {
    it('empty', () => {
        expect(intersperse(0)([])).toEqual([]);
    });

    it('one', () => {
        expect(intersperse(0)([1])).toEqual([1]);
    });

    it('more', () => {
        expect(intersperse(0)([1, 2, 3])).toEqual([1, 0, 2, 0, 3]);
    });
});
