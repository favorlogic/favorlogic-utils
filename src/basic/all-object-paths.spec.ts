import {allObjectPaths} from './all-object-paths';

describe('allObjectPaths', () => {
    it('empty', () => {
        expect(allObjectPaths({})).toEqual([]);
    });

    it('flat', () => {
        expect(allObjectPaths({a: 1, b: 2})).toEqual(['a', 'b']);
    });

    it('empty object', () => {
        expect(allObjectPaths({a: {}})).toEqual(['a']);
    });

    it('one deep', () => {
        expect(allObjectPaths({a: {b: 2}})).toEqual(['a.b']);
    });

    it('multiple', () => {
        expect(allObjectPaths({a: {b: 2, c: 3}})).toEqual(['a.b', 'a.c']);
    });

    it('sorted', () => {
        expect(allObjectPaths({a: {x: 2, a: 3}, [0]: 0})).toEqual(['0', 'a.a', 'a.x']);
    });

    it('deep', () => {
        const value = {a: {b: {y: {z: 'Z'}}, c: {x: [1, 2]}}};
        expect(allObjectPaths(value)).toEqual(['a.b.y.z', 'a.c.x']);
    });

    it('multiple deep', () => {
        const value = {a: {b: {y: {z: 'Z'}, f: () => undefined}}};
        expect(allObjectPaths(value)).toEqual(['a.b.f', 'a.b.y.z']);
    });
});
