/* eslint-disable @typescript-eslint/no-unused-vars */
import {removeAt} from './remove-at';

describe('removeAt', () => {
    it('removes at given position from array', () => {
        expect(removeAt(0)([0, 1, 2])).toEqual([1, 2]);
        expect(removeAt(1)([0, 1, 2])).toEqual([0, 2]);
        expect(removeAt(2)([0, 1, 2])).toEqual([0, 1]);
        // @ts-expect-error array should not produce string
        const y: string = removeAt(0)([]);
    });
    it('removes at given position from string', () => {
        expect(removeAt(0)('abc')).toEqual('bc');
        expect(removeAt(1)('abc')).toEqual('ac');
        expect(removeAt(2)('abc')).toEqual('ab');
        // @ts-expect-error string should not produce array
        const y: Array<unknown> = removeAt(0)('');
    });
});
