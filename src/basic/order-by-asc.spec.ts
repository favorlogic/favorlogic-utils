import {id} from 'ts-opt';

import {orderByAsc} from './order-by-asc';

interface A {
    id: number;
    name: string;
}

interface Test {id: number}

describe('order-by-asc', () => {
    it('sorts ascending', () => {
        expect(orderByAsc(id)([2, 1, 3])).toEqual([1, 2, 3]);
        const testData: Array<A> = [
            {id: 1, name: 'z'},
            {id: 0, name: 'a'},
            {id: -1, name: 'g'},
        ];
        expect(orderByAsc<A>(x => x.id)(testData)).toEqual([
            {id: -1, name: 'g'},
            {id: 0, name: 'a'},
            {id: 1, name: 'z'},
        ]);
        expect(orderByAsc<A>(x => x.name)(testData)).toEqual([
            {id: 0, name: 'a'},
            {id: -1, name: 'g'},
            {id: 1, name: 'z'},
        ]);
        expect(orderByAsc<Test>(x => x.id)([{id: 1}, {id: 0}, {id: -1}])).toEqual([{id: -1}, {id: 0}, {id: 1}]);
    });
});
