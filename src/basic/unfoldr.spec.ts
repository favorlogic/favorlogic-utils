import {unfoldr} from './unfoldr';

describe('unfoldr', () => {
    it('unfolds', () => {
        const res = unfoldr((s: number) => s <= 5 ? [s * 10, s + 1] : null, 2);
        expect(res).toEqual([20, 30, 40, 50]);
    });
});
