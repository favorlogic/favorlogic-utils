/**
 * Allows working with an array of objects as a set.
 * Objects are considered equal when they have same value in `key` field.
 * When {@param addOrRemove} is set to `true`, `item` is appended to the input array (unless already present).
 * When {@param addOrRemove} is set to `false`, `item` is removed from the input array (all instances of the item).
 * Immutable.
 *
 * @example
 * ```ts
 * const zero = {id: 0, name: 'zero'};
 * const one = {id: 1, name: 'one'};
 * setItemBy(true)('id')(zero)([]) // [zero]
 * setItemBy(true)('id')(zero)([one]) // [one, zero]
 * setItemBy(false)('id')(zero)([zero, one]) // [one]
 * ```
 *
 * @param addOrRemove
 */
export const setItemBy =
    (addOrRemove: boolean) => <T extends object>(key: keyof T) => (item: T) => (xs: Array<T>): Array<T> => {
        if (addOrRemove) {
            return xs.some(x => x[key] === item[key]) ? xs : [...xs, item];
        } else {
            return xs.filter(x => x[key] !== item[key]);
        }
    };
