import {isArrayOfObjects} from './is-array-of-objects';

describe('isArrayOfObjects', () => {
    it('positive', () => {
        expect(isArrayOfObjects([])).toEqual(true);
        expect(isArrayOfObjects([{}])).toEqual(true);
        expect(isArrayOfObjects([{a: 1}, {a: 2}])).toEqual(true);
        expect(isArrayOfObjects([{a: 1}, {b: 2}])).toEqual(true);
    });

    it('negative', () => {
        expect(isArrayOfObjects({})).toEqual(false);
        expect(isArrayOfObjects(0)).toEqual(false);
        expect(isArrayOfObjects('')).toEqual(false);
        expect(isArrayOfObjects(false)).toEqual(false);
        expect(isArrayOfObjects(undefined)).toEqual(false);
        expect(isArrayOfObjects(null)).toEqual(false);
        expect(isArrayOfObjects([null])).toEqual(false);
        expect(isArrayOfObjects([''])).toEqual(false);
        expect(isArrayOfObjects([{}, ''])).toEqual(false);
        expect(isArrayOfObjects([false, {}])).toEqual(false);
    });
});
