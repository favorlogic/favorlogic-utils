import {omit} from './omit';

interface FinalObject1 {
    b: number;
}
interface FinalObject2 {
    a: number;
    b: number;
}

interface InputObject {
    a: number;
    b: number;
    c: number;
}

interface InputObject1 {
    a?: number;
    b: number;
    c?: number;
}

interface FinalObject3 {
    a: number;
    b: number;
    c: number;
}

interface BlankObject1 {a?: number}

describe('omit', () => {
    const blankObject = {};
    const blankObject1: BlankObject1 = {};
    const inputObject: InputObject = {a: 1, b: 2, c: 3};
    const inputObject1: InputObject1 = {b: 2};
    const finalObject1: FinalObject1 = {b: 2};
    const finalObject2: FinalObject2 = {a: 1, b: 2};
    const finalObject3: FinalObject3 = {a: 1, b: 2, c: 3};

    it('expected values', () => {
        expect(omit(inputObject, ['a', 'c'])).toEqual(finalObject1);
        expect(omit(inputObject, ['a', 'c', 'b'])).toEqual(blankObject);
        expect(omit(blankObject, [])).toEqual(blankObject);
    });
    it('type errors', () => {
        // @ts-expect-error type break
        expect(omit(inputObject, ['d'])).toEqual(finalObject3);
        // @ts-expect-error type break
        expect(omit(inputObject, ['d', 'c'])).toEqual(finalObject2);
        // @ts-expect-error type break
        expect(omit(blankObject, ['d'])).toEqual(blankObject);
    });
    it('optional fields', () => {
        expect(omit(inputObject1, ['a'])).toEqual(finalObject1);
        expect(omit(blankObject1, ['a'])).toEqual(blankObject);
    });
});
