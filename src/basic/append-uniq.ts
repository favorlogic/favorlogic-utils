export const appendUniq = <T>(elem: T) => (array: Array<T>): Array<T> =>
    array.includes(elem) ? array : [...array, elem];
