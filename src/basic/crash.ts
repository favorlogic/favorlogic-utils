export const crash = (message: string): never => {
    throw new Error(message);
};
