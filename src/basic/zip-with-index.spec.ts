/* eslint-disable @typescript-eslint/no-explicit-any */

import {zipWithIndex} from './zip-with-index';

describe('zipWithIndex', () => {
    it('empty', () => {
        expect(zipWithIndex([])).toEqual([]);
    });

    it('fails on non-array', () => {
        expect(() => zipWithIndex(undefined as any)).toThrowError('Array expected.');
        expect(() => zipWithIndex(null as any)).toThrowError('Array expected.');
        expect(() => zipWithIndex(1 as any)).toThrowError('Array expected.');
        expect(() => zipWithIndex('a' as any)).toThrowError('Array expected.');
        expect(() => zipWithIndex({a: '◇'} as any)).toThrowError('Array expected.');
    });

    it('zips with index', () => {
        expect(zipWithIndex([1, 2, 3])).toEqual([[1, 0], [2, 1], [3, 2]]);
        expect(zipWithIndex([{a: 1}])).toEqual([[{a: 1}, 0]]);
    });
});
