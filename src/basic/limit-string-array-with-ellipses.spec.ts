import {limitStringArrayWithEllipses} from './limit-string-array-with-ellipses';

const f = limitStringArrayWithEllipses;

describe('limitStringArrayWithEllipses', () => {
    it('empty', () => {
        expect(f(1)([])).toEqual([]);
    });

    it('positive', () => {
        expect(f(0)(['a', 'b'])).toEqual(['…']);
        expect(f(1)(['a', 'b'])).toEqual(['a', '…']);
        expect(f(2)(['a', 'b', 'c', 'd'])).toEqual(['a', 'b', '…']);
    });

    it('negative', () => {
        expect(f(2)(['a', 'b'])).toEqual(['a', 'b']);
        expect(f(4)(['a', 'b', 'c', 'd'])).toEqual(['a', 'b', 'c', 'd']);
    });
});
