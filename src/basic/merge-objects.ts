type OptionalPropertyNames<T> =
    { [K in keyof T]-?: (object extends { [P in K]: T[K] } ? K : never) }[keyof T];

type SpreadProperties<L, R, K extends keyof L & keyof R> =
    { [P in K]: L[P] | Exclude<R[P], undefined> };

type Id<T> = T extends infer U ? { [K in keyof U]: U[K] } : never;

type SpreadTwo<L, R> = Id<
    & Pick<L, Exclude<keyof L, keyof R>>
    & Pick<R, Exclude<keyof R, OptionalPropertyNames<R>>>
    & Pick<R, Exclude<OptionalPropertyNames<R>, keyof L>>
    & SpreadProperties<L, R, OptionalPropertyNames<R> & keyof L>
    >;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Spread<A extends readonly [...any]> = A extends [infer L, ...infer R] ?
    SpreadTwo<L, Spread<R>> : unknown;

/**
 *  mergeObjects(
 *      { a: 42 },
 *      { b: "foo", a: "bar" },
 *      { c: true, b: 123 }
 *  ) => { a: 'bar', b: 123, c: true} with theirs types
 */
export const mergeObjects = <A extends Array<object>>(...a: [...A]): Spread<A> => Object.assign({}, ...a) as Spread<A>;
