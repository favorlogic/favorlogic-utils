import {none} from 'ts-opt';

import {convertStringToInt} from './convert-string-to-int';

describe('convertStringToInt', () => {
    it('should return integer', () => {
        expect(convertStringToInt('123').orNull()).toEqual(123);
    });
    it('should not return integer', () => {
        expect(convertStringToInt()).toEqual(none);
        expect(convertStringToInt('')).toEqual(none);
    });
});
