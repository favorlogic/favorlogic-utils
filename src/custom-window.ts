import {StoreEnhancer} from 'redux';

export interface ReduxDevtoolsExtensionOptions {
    serialize: {
        replacer(key: unknown, value: unknown): unknown,
        reviver(key: unknown, value: unknown): unknown,
    };
    actionsBlacklist?: Array<string>;
    actionsWhitelist?: Array<string>;
}

type Compose<R> = (a: R) => R;
type ComposeFactory = (options: ReduxDevtoolsExtensionOptions) => Compose<StoreEnhancer>;

export interface CustomWindow {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: ComposeFactory & Compose<StoreEnhancer>;
}
