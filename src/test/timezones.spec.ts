import moment from 'moment';

const minutesInHour = 60;

describe('timezone', () => {
    it('TZ should be Europe/Samara', () => {
        expect(process.env.TZ).toBe('Europe/Samara');
    });

    it('Date timezoneOffset should be UTC+4', () => {
        const hourOffset = -4;
        expect(new Date().getTimezoneOffset()).toBe(hourOffset * minutesInHour);
    });

    it('moment UTC offset should be UTC+4', () => {
        const hourOffset = 4;
        expect(moment().utcOffset()).toBe(hourOffset * minutesInHour);
    });
});
