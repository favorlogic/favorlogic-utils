describe('optional chaining', () => {
    it('compiles and works as intended', () => {
        interface T {a: number; b?: {c?: string, d?(): string}}
        const x: T = {a: 1, b: {}};
        expect(x.b?.c).toBeUndefined();
        expect(x.b?.d?.()).toBeUndefined();
        const y: T = {a: 2, b: {d: () => 'd'}};
        expect(y.b?.d?.()).toEqual('d');
    });
});
