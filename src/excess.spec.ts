import {either} from 'fp-ts';
import * as t from 'io-ts';

import {excess, ExcessType} from './excess';

describe('excess function', () => {
    const TypeSchema = t.type({
        foo: t.string,
    });
    const InterfaceSchema = t.interface({
        foo: t.string,
    });
    const OptionalSchema = t.interface({
        optional: t.union([t.number, t.null]),
    });
    const IntersectionSchema = t.intersection([
        t.type({
            foo: t.string,
        }),
        t.type({
            baz: t.number,
        }),
        t.type({
            foo: t.string,
        }),
    ]);
    const PartialSchema = t.partial({
        foo: t.string,
        num: t.number,
    });

    const ExcessTypeSchema = excess(TypeSchema);
    const ExcessIntersectionSchema = excess(IntersectionSchema);
    const ExcessPartialSchema = excess(PartialSchema);
    const ExcessWithOptionalSchema = excess(t.intersection([InterfaceSchema, OptionalSchema]));

    const value1 = {foo: 'bar'};
    const value2 = {foo: 'bar', baz: 123};
    const value3 = {optional: 22, foo: 'required'};
    const value4 = {optional: null, foo: 'required'};

    it('should correctly validate objects with no excess properties', () => {
        const result1 = ExcessTypeSchema.decode(value1);
        expect(either.isRight(result1)).toBe(true);
        if (either.isRight(result1)) expect(result1.right).toEqual(value1);

        const result2 = ExcessIntersectionSchema.decode(value2);
        expect(either.isRight(result2)).toBe(true);
        if (either.isRight(result2)) expect(result2.right).toEqual(value2);

        const result3 = ExcessPartialSchema.decode(value1);
        expect(either.isRight(result3)).toBe(true);
        if (either.isRight(result3)) expect(result3.right).toEqual(value1);

        const result4 = ExcessWithOptionalSchema.decode(value3);
        expect(either.isRight(result4)).toBe(true);
        if (either.isRight(result4)) expect(result4.right).toEqual(value3);

        const result5 = ExcessWithOptionalSchema.decode(value4);
        expect(either.isRight(result5)).toBe(true);
        if (either.isRight(result5)) expect(result5.right).toEqual(value4);
    });

    it('should fail validating objects with excess properties', () => {
        const result = ExcessTypeSchema.decode(value2);
        expect(either.isLeft(result)).toBe(true);

        const result2 = ExcessIntersectionSchema.decode(value1);
        expect(either.isLeft(result2)).toBe(true);

        const result3 = ExcessPartialSchema.decode(value2);
        expect(either.isLeft(result3)).toBe(true);
    });

    it('should create a subtype of t.Type', () => {
        const ExcessMyTypeSchema = excess(TypeSchema);
        expect(ExcessMyTypeSchema).toBeInstanceOf(ExcessType);
    });
});
