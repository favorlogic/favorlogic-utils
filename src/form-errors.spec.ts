import {FormErrors} from './form-errors';

interface Address {
    city: string;
    street: string;
    zip: string;
}

interface Values {
    name: string;
    age: number;
    description: string | null;
    count: number | null;
    address: Address;
    billingAddress: Address | null;
    values: Array<string>;
    otherValues: Array<number> | null;
}

describe('FormErrors', () => {
    it('should work with basic fields', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const errors: FormErrors<Values> = {
            name: 'error1',
            age: undefined,
            // @ts-expect-error expect string or undefined
            description: 3,
            // @ts-expect-error expect string or undefined
            count: null,
        };
    });

    it('should work with object fields', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const errors: FormErrors<Values> = {
            address: {
                city: 'wrong city',
                street: 'wrong street',
                zip: undefined,
                // @ts-expect-error expect existing keys
                other: 'wrong key',
            },
            billingAddress: 'wrong address',
        };
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const errors2: FormErrors<Values> = {
            address: 'wrong address',
            billingAddress: {
                city: 'wrong city',
                street: 'wrong street',
                zip: undefined,
                // @ts-expect-error expect existing keys
                other: 'wrong key',
            },
        };
    });

    it('should work with array fields', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const errors: FormErrors<Values> = {
            values: [
                'error1',
                'error2',
                // @ts-expect-error expect strings
                123,
            ],
            otherValues: 'error',
        };
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const errors2: FormErrors<Values> = {
            values: 'error',
            otherValues: [
                'error1',
                'error2',
                // @ts-expect-error expect strings
                123,
            ],
        };
    });
});
