import {DatetimeStringSchema, DatetimeStringType} from './datetime-string';
import {accepts, rejects} from './test-utils';

describe('DatetimeString', () => {
    it('is method', () => {
        expect(DatetimeStringSchema.is('')).toBeTruthy();
        expect(DatetimeStringSchema.is(0)).toBeFalsy();
    });

    describe('accepts datetime strings', () => {
        accepts(DatetimeStringSchema, '2007-04-05T10:00');
        accepts(DatetimeStringSchema, '2018-11-29T07:56:11.570Z');
        accepts(DatetimeStringSchema, '2007-04-05T14:30Z');
    });

    describe('doesn\'t accept non-datetime strings', () => {
        rejects(DatetimeStringSchema, '');
        rejects(DatetimeStringSchema, '1. 1. 2018');
        rejects(DatetimeStringSchema, '1.1.2018');
        rejects(DatetimeStringSchema, 'worf');
        rejects(DatetimeStringSchema, '01-01-2018');
    });

    describe('create own schema works', () => {
        const schema = new DatetimeStringType();
        expect(schema.is('')).toBeTruthy();
        expect(schema.is(0)).toBeFalsy();
        accepts(schema, '2007-04-05T10:00');
        rejects(schema, '1. 1. 2018');
    });
});
