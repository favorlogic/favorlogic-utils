import {either} from 'fp-ts';
import {pipe} from 'fp-ts/function';
import * as t from 'io-ts';
import moment from 'moment';

import {beDateFormat} from '../../format/format-date-time';

export class DateStringType extends t.Type<string> {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    public readonly _tag = 'DateStringType' as const;

    constructor() {
        super(
            'DateString',
            (m: unknown): m is string => typeof m === 'string',
            (m, c) =>
                pipe(
                    t.string.validate(m, c),
                    either.chain(s => {
                        const d = moment(s, beDateFormat, true);

                        return d.isValid() ? t.success(s) : t.failure(s, c);
                    }),
                ),
            t.identity,
        );
    }
}

export const DateStringSchema = new DateStringType();
