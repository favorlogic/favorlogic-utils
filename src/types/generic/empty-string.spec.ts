import {EmptyStringSchema} from './empty-string';
import {accepts, rejects} from './test-utils';

describe('EmptyString', () => {
    it('is method', () => {
        expect(EmptyStringSchema.is('')).toBeTruthy();
        expect(EmptyStringSchema.is(0)).toBeFalsy();
    });

    describe('accepts empty string', () => {
        accepts(EmptyStringSchema, '');
    });

    describe('doesn\'t accept non-empty strings', () => {
        rejects(EmptyStringSchema, 'test');
    });
});
