export type KeysOfType<Obj, KeyT> = {
    [K in keyof Obj]-?: Obj[K] extends KeyT ? K : never;
}[keyof Obj];
