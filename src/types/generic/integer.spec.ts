import {IntegerSchema, IntegerType} from './integer';
import {accepts, rejects} from './test-utils';

describe('Integer', () => {
    it('is method', () => {
        expect(IntegerSchema.is(0)).toBeTruthy();
        expect(IntegerSchema.is('')).toBeFalsy();
    });

    describe('accepts integer numberss', () => {
        accepts(IntegerSchema, -3);
        accepts(IntegerSchema, 0);
        accepts(IntegerSchema, 17);
    });

    describe('doesn\'t accept non-integer numbers', () => {
        rejects(IntegerSchema, -2.5);
        rejects(IntegerSchema, 0.7);
        rejects(IntegerSchema, 123.456);
        rejects(IntegerSchema, NaN);
        rejects(IntegerSchema, Infinity);
    });

    describe('create own schema works', () => {
        const schema = new IntegerType();
        expect(schema.is(0)).toBeTruthy();
        expect(schema.is('')).toBeFalsy();
        accepts(schema, -3);
        rejects(schema, -2.5);
    });
});
