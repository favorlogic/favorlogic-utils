import {BlobSchema, BlobType} from './blob';
import {accepts, rejects} from './test-utils';

describe('Blob', () => {
    it('is method', () => {
        expect(BlobSchema.is(new Blob())).toBeTruthy();
        expect(BlobSchema.is('')).toBeFalsy();
    });

    describe('accepts Blob', () => {
        accepts(BlobSchema, new Blob());
    });

    describe('doesn\'t accept other values', () => {
        rejects(BlobSchema, null);
        rejects(BlobSchema, undefined);
        rejects(BlobSchema, []);
    });

    describe('custom schema works correctly', () => {
        const schema = new BlobType();
        expect(schema.is(new Blob())).toBeTruthy();
        expect(schema.is('')).toBeFalsy();
        accepts(schema, new Blob());
        rejects(schema, []);
    });
});
