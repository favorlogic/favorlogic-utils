import {StringPatternSchema, StringPatternType} from './string-pattern';
import {accepts, rejects} from './test-utils';

const NumberStringSchema = StringPatternSchema(/^\d+$/);
const XYZStringSchema = StringPatternSchema(/^xy+z$/);

describe('StringPattern', () => {
    it('is method', () => {
        expect(NumberStringSchema.is('')).toBeTruthy();
        expect(NumberStringSchema.is(0)).toBeFalsy();
    });

    describe('accepts', () => {
        accepts(NumberStringSchema, '123');
        accepts(XYZStringSchema, 'xyyyyz');
    });

    describe('rejects', () => {
        rejects(XYZStringSchema, '');
        rejects(NumberStringSchema, 'z123');
        rejects(NumberStringSchema, '123z');
        rejects(NumberStringSchema, '123z');
    });

    describe('create own schema works', () => {
        const schema = new StringPatternType(/^\d+$/);
        expect(schema.is('')).toBeTruthy();
        expect(schema.is(0)).toBeFalsy();
        accepts(schema, '123');
        rejects(schema, 'z123');
    });
});
