import {either} from 'fp-ts';
import {pipe} from 'fp-ts/function';
import * as t from 'io-ts';
import moment from 'moment';

import {beYearmonthFormat} from '../../format/format-date-time';

export class YearmonthStringType extends t.Type<string> {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    public readonly _tag = 'YearmonthStringType' as const;

    constructor() {
        super(
            'YearmonthString',
            (m: unknown): m is string => typeof m === 'string',
            (m, c) =>
                pipe(
                    t.string.validate(m, c),
                    either.chain(s => {
                        const d = moment(s, beYearmonthFormat, true);

                        return d.isValid() ? t.success(s) : t.failure(s, c);
                    }),
                ),
            t.identity,
        );
    }
}

export const YearmonthStringSchema = new YearmonthStringType();
