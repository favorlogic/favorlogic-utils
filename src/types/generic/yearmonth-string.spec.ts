import {accepts, rejects} from './test-utils';
import {YearmonthStringSchema, YearmonthStringType} from './yearmonth-string';

describe('YearmonthString', () => {
    it('is method', () => {
        expect(YearmonthStringSchema.is('')).toBeTruthy();
        expect(YearmonthStringSchema.is(0)).toBeFalsy();
        expect(YearmonthStringSchema.is(null)).toBeFalsy();
        expect(YearmonthStringSchema.is(undefined)).toBeFalsy();
        expect(YearmonthStringSchema.is(false)).toBeFalsy();
        expect(YearmonthStringSchema.is(true)).toBeFalsy();
    });

    describe('accepts yearmonth string', () => {
        accepts(YearmonthStringSchema, '2007-04');
    });

    describe('doesn\'t accept non-yearmonth strings', () => {
        rejects(YearmonthStringSchema, '');
        rejects(YearmonthStringSchema, '1.1.2018');
        rejects(YearmonthStringSchema, 'worf');
        rejects(YearmonthStringSchema, '2007-04-05');
        rejects(YearmonthStringSchema, '01-01-2018');
        rejects(YearmonthStringSchema, '2018-11-29T07:56:11.570Z');
        rejects(YearmonthStringSchema, '2007-04-05T14:30Z');
        rejects(YearmonthStringSchema, null);
        rejects(YearmonthStringSchema, undefined);
    });
    describe('Create own schema works', () => {
        const schema = new YearmonthStringType();
        rejects(schema, '');
        rejects(schema, '1.1.2018');
        rejects(schema, 'worf');
        rejects(schema, '2007-04-05');
        rejects(schema, '01-01-2018');
        rejects(schema, '2018-11-29T07:56:11.570Z');
        rejects(schema, '2007-04-05T14:30Z');
        expect(schema.is('')).toBeTruthy();
        expect(schema.is(0)).toBeFalsy();
        accepts(schema, '2007-04');
    });
});
