import {either} from 'fp-ts';
import {pipe} from 'fp-ts/function';
import * as t from 'io-ts';

export class IntegerType extends t.Type<number> {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    public readonly _tag = 'IntegerType' as const;

    constructor() {
        super(
            'Integer',
            (m: unknown): m is number => typeof m === 'number',
            (m, c) =>
                pipe(
                    t.number.validate(m, c),
                    either.chain(s => Number.isInteger(s) ? t.success(s) : t.failure(s, c)),
                ),
            t.identity,
        );
    }
}

export const IntegerSchema = new IntegerType();
