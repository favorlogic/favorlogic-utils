import {either} from 'fp-ts';
import {pipe} from 'fp-ts/function';
import * as t from 'io-ts';

export class StringPatternType<T extends string> extends t.Type<T> {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    public readonly _tag = 'StringPatternType' as const;

    constructor(pattern: RegExp) {
        super(
            'StringPattern',
            (m: unknown): m is T => typeof m === 'string',
            (m, c) =>
                pipe(
                    t.string.validate(m, c),
                    either.chain(s => pattern.test(s) ? t.success(s as T) : t.failure(s, c)),
                ),
            t.identity,
        );
    }
}

export const StringPatternSchema = <T extends string>(pattern: RegExp): StringPatternType<T> =>
    new StringPatternType<T>(pattern);
