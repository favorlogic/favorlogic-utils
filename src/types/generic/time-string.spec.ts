import {accepts, rejects} from './test-utils';
import {TimeStringSchema} from './time-string';

describe('TimeString', () => {
    it('is method', () => {
        expect(TimeStringSchema.is('')).toBeTruthy();
        expect(TimeStringSchema.is(0)).toBeFalsy();
    });

    describe('accepts time string', () => {
        accepts(TimeStringSchema, '12:50');
        accepts(TimeStringSchema, '04:05');
    });

    describe('doesn\'t accept non-time strings', () => {
        rejects(TimeStringSchema, '');
        rejects(TimeStringSchema, '4:05');
        rejects(TimeStringSchema, 'worf');
        rejects(TimeStringSchema, '2018-11-29T07:56:11.570Z');
        rejects(TimeStringSchema, '2007-04-05T14:30Z');
    });
});
