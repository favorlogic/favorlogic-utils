import {either} from 'fp-ts';
import {pipe} from 'fp-ts/function';
import * as t from 'io-ts';
import moment, {ISO_8601} from 'moment';

export class DatetimeStringType extends t.Type<string> {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    public readonly _tag = 'DatetimeStringType' as const;

    constructor() {
        super(
            'DatetimeString',
            (m: unknown): m is string => typeof m === 'string',
            (m, c) =>
                pipe(
                    t.string.validate(m, c),
                    either.chain(s => {
                        const d = moment(s, ISO_8601, true);

                        return d.isValid() ? t.success(s) : t.failure(s, c);
                    }),
                ),
            t.identity,
        );
    }
}

export const DatetimeStringSchema = new DatetimeStringType();
