import {either} from 'fp-ts';
import * as t from 'io-ts';

export const accepts = <T, O = T>(type: t.Type<T, O>, x: unknown): void => {
    it(`accepts ${JSON.stringify(x)}`, () => {
        expect(either.isRight(type.decode(x))).toEqual(true);
    });
};

export const rejects = <T, O = T>(type: t.Type<T, O>, x: unknown): void => {
    it(`rejects ${JSON.stringify(x)}`, () => {
        expect(either.isLeft(type.decode(x))).toEqual(true);
    });
};
