import {ArrayBufferSchema, ArrayBufferType} from './array-buffer';
import {accepts, rejects} from './test-utils';

describe('ArrayBuffer', () => {
    it('is method', () => {
        expect(ArrayBufferSchema.is(new ArrayBuffer(0))).toBeTruthy();
        expect(ArrayBufferSchema.is('')).toBeFalsy();
    });

    describe('accepts ArrayBuffer', () => {
        accepts(ArrayBufferSchema, new ArrayBuffer(0));
    });

    describe('doesn\'t accept other values', () => {
        rejects(ArrayBufferSchema, null);
        rejects(ArrayBufferSchema, undefined);
        rejects(ArrayBufferSchema, []);
    });

    describe('custom schema works correctly', () => {
        const schema = new ArrayBufferType();
        expect(schema.is(new ArrayBuffer(0))).toBeTruthy();
        expect(schema.is('')).toBeFalsy();
        accepts(schema, new ArrayBuffer(0));
        rejects(schema, []);
    });
});
