import {DateStringSchema, DateStringType} from './date-string';
import {accepts, rejects} from './test-utils';

describe('DateString', () => {
    it('is method', () => {
        expect(DateStringSchema.is('')).toBeTruthy();
        expect(DateStringSchema.is(0)).toBeFalsy();
    });

    describe('accepts date string', () => {
        accepts(DateStringSchema, '2007-04-05');
    });

    describe('doesn\'t accept non-date strings', () => {
        rejects(DateStringSchema, '');
        rejects(DateStringSchema, '1. 1. 2018');
        rejects(DateStringSchema, '1.1.2018');
        rejects(DateStringSchema, 'worf');
        rejects(DateStringSchema, '01-01-2018');
        rejects(DateStringSchema, '2018-11-29T07:56:11.570Z');
        rejects(DateStringSchema, '2007-04-05T14:30Z');
    });

    describe('custom schema works correctly', () => {
        const schema = new DateStringType();
        expect(schema.is('')).toBeTruthy();
        expect(schema.is(0)).toBeFalsy();
        accepts(schema, '2007-04-05');
        rejects(schema, '2018-11-29T07:56:11.570Z');
    });
});
