import {FormDataSchema, FormDataType} from './form-data';
import {accepts, rejects} from './test-utils';

describe('FormData', () => {
    it('is method', () => {
        expect(FormDataSchema.is(new FormData())).toBeTruthy();
        expect(FormDataSchema.is('')).toBeFalsy();
    });

    describe('accepts FormData', () => {
        accepts(FormDataSchema, new FormData());
    });

    describe('doesn\'t accept other values', () => {
        rejects(FormDataSchema, null);
        rejects(FormDataSchema, undefined);
        rejects(FormDataSchema, {});
    });

    describe('create own schema works', () => {
        const schema = new FormDataType();
        expect(schema.is(new FormData())).toBeTruthy();
        expect(schema.is('')).toBeFalsy();
        accepts(schema, new FormData());
        rejects(schema, null);
    });
});
