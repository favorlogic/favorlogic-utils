export type Nullable<T> = T | null;

export type NullableKeys<T> = {
    [P in keyof T]: Nullable<T[P]>;
};

export type Dictionary<K extends string | number | symbol, V> = Partial<Record<K, V>>;

export type UnknownObject = {
    [_ in string | number]: unknown;
};

export type ValueOfArray<T extends Array<unknown>> = T[number];

export type StringKeyof<T> = Extract<keyof T, string>;

export type PartialKeys<T, OptionalKeys extends keyof T> =
    Partial<Pick<T, OptionalKeys>> & Pick<T, Exclude<keyof T, OptionalKeys>>;
