import * as t from 'io-ts';

export class BlobType extends t.Type<Blob> {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    public readonly _tag = 'BlobType' as const;

    constructor() {
        super(
            'Blob',
            (m: unknown): m is Blob => m instanceof Blob,
            (m, c) => this.is(m) ? t.success(m) : t.failure(m, c),
            t.identity,
        );
    }
}

export const BlobSchema = new BlobType();
