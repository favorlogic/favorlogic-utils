import * as t from 'io-ts';

interface EmptyStringBrand {
    readonly EmptyString: unique symbol;
}

export const EmptyStringSchema = t.brand(
    t.string,
    (s): s is t.Branded<string, EmptyStringBrand> => s.length === 0,
    'EmptyString',
);

export type EmptyString = t.TypeOf<typeof EmptyStringSchema>;
