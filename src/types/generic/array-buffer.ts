import * as t from 'io-ts';

export class ArrayBufferType extends t.Type<ArrayBuffer> {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    public readonly _tag = 'ArrayBufferType' as const;

    constructor() {
        super(
            'ArrayBuffer',
            (m: unknown): m is ArrayBuffer => m instanceof ArrayBuffer,
            (m, c) => this.is(m) ? t.success(m) : t.failure(m, c),
            t.identity,
        );
    }
}

export const ArrayBufferSchema = new ArrayBufferType();
