import * as t from 'io-ts';

export const EmptyObjectSchema = t.exact(t.interface({}));

export type EmptyObject = t.TypeOf<typeof EmptyObjectSchema>;
