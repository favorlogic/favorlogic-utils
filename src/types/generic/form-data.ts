import * as t from 'io-ts';

export class FormDataType extends t.Type<FormData> {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    public readonly _tag = 'FormDataType' as const;

    constructor() {
        super(
            'FormData',
            (m: unknown): m is FormData => m instanceof FormData,
            (m, c) => this.is(m) ? t.success(m) : t.failure(m, c),
            t.identity,
        );
    }
}

export const FormDataSchema = new FormDataType();
