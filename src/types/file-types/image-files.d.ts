declare module '*.jpg';
declare module '*.jpeg';
declare module '*.png';
declare module '*.svg' {
    const content: string;
    // eslint-disable-next-line import/no-unused-modules
    export default content;
}
