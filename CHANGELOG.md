# Changelog

All notable changes in project are documented in this file. The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and the library adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
## Released

### [7.1.0](https://redmine.favorlogic.com/versions/3190) - 21.12.2024

#### Changed
- [R-16552](https://redmine.favorlogic.com/issues/16552): Call refresh to access token only once.

#### Fixed
- [R-16541](http://redmine.favorlogic.com/issues/16541): Fixed refresh token and login urls in requester.

---

### [7.0.0](https://redmine.favorlogic.com/versions/374) - 28.11.2024

#### Change
- **[R-16362](https://redmine.favorlogic.com/issues/16362): Updated node.**
- **[R-16365](https://redmine.favorlogic.com/issues/16365): Updated pnpm.**
- **[R-16368](https://redmine.favorlogic.com/issues/16368): Updated libraries dues to webpack.**
- Updated following libraries:
    - `node` **18**.13.0 --> **22**.10.0
    - `pnpm` **7**.9.1 --> **9**.12.2
    - `typecript` 4.9.**4** --> 4.9.**5**

#### Removed
- [R-16415](https://redmine.favorlogic.com/issues/16415): Removed git hooks.
- Removed following libraries:
    - `husky`

---

### [6.1.0](https://redmine.favorlogic.com/versions/319) - 28.11.2024

#### Added
- [R-16303](https://redmine.favorlogic.com/issues/16303): Added a Jest test for YearMonthString to improve the coverage.
- [R-16302](https://redmine.favorlogic.com/issues/16302): Added jest tests for DateTimeStringType, FormDataType, IntegerType and StringPatternType.
- [R-16299](https://redmine.favorlogic.com/issues/16299): Added tests for excess.ts
- [R-16300](https://redmine.favorlogic.com/issues/16300): Added Jest tests for requester, request builder and response to fulfil test coverage.
- [R-16301](https://redmine.favorlogic.com/issues/16301): Added Jest test for formatCurrencyCzk, formatNumber, blobType and DateStringType to fulfil test coverage.

---

## Released

### [6.0.0](https://redmine.favorlogic.com/versions/319) - 04.10.2024

#### Changed
- **[R-16182](https://redmine.favorlogic.com/issues/16182): Updated ts-opt.**
- **[R-14954](https://redmine.favorlogic.com/issues/14954): Moved to npm registry as `@fl/utils`.**
- Updated following libraries:
    - `eslint-plugin-favorlogic` **3.0.0** --> `@fl/eslint-plugin` **3.0.0-dev.0**
    - `ts-opt` **4.1.0** --> **6.0.2**

#### Added
- [R-15522](https://redmine.favorlogic.com/issues/15522): Added JWT setup.
- [R-16256](https://redmine.favorlogic.com/issues/16256): Added JWT requester jest tests.


### 5.3.0 - 07.11.2023

#### Changed
- [R-13560](https://redmine.favorlogic.com/issues/13560): Made package scripts cross-platform.

---

### 5.2.0 - 27.07.2023

#### Added
- Added following libraries:
    - `eslint-plugin-import-newlines` **1.3.1**
    - `eslint-plugin-newline-destructuring` **1.2.1**
    - `eslint-plugin-favorlogic` **3.0.0**

#### Changed
- [R-13424](https://redmine.favorlogic.com/issues/13424): Updated lint rules and fixed lint errors.
- [R-13174](http://redmine.favorlogic.com/issues/13174): Unified lint.
- [R-13204](http://redmine.favorlogic.com/issues/13204): Install dependencies by pnpm-lock.json in pipelines.
- Updated following libraries:
    - `husky` **3**.1.0 --> **8**.0.3
    - `rimraf` **4**.1.1 --> **5**.0.0


#### Removed
- Removed following libraries:
    - `set-tz`
    - `jest-environment-jsdom`
    - `react-dom`
    - `npm-run-all`

---

### 5.1.0 - 09.05.2023

#### Added
- [R-13072](http://redmine.favorlogic.com/issues/13072): Added d.M.yyyy date format.

---

### 5.0.0 - 18.4.2023

#### Removed
- [R-12677](http://redmine.favorlogic.com/issues/12558): Removed be-errors module.
- [R-12681](http://redmine.favorlogic.com/issues/12681): Removed cmsch/browser specific utilities from basic and file modules.
- [R-12732](http://redmine.favorlogic.com/issues/12732): Removed notification module.
- [R-12678](http://redmine.favorlogic.com/issues/12678): Removed browser specific utilities from element module.
- [R-12686](http://redmine.favorlogic.com/issues/12686): Removed other browser specific utilities and dependencies.
- Removed following libraries:
    - `nano-crypto`
    - `universal-cookie`
    - `react-redux`

### Changed
- [R-12812](http://redmine.favorlogic.com/issues/12812): Changed build output to commonjs.

#### Added
- [R-12774](http://redmine.favorlogic.com/issues/12774): Added function for checking misdirected code response.
- [R-12587](http://redmine.favorlogic.com/issues/12587): Added function for checking duplicities in arrays.

---

### 4.0.1 - 28.02.2023

#### Changed
- [R-12660](http://redmine.favorlogic.com/issues/12660): Removed exporting interface Options from mapStateToSearch.

---

### 4.0.0 - 06.02.2023

#### Added
- [R-12558](http://redmine.favorlogic.com/issues/12558): Added debounceF saga helper.

#### Changed
- Updated following libraries:
    - `@types/lodash` 4.14.**182** --> 4.14.**191**
    - `@types/react` 18.0.**14** --> 18.0.**27**
    - `@types/react-router` 5.1.**18** --> 5.1.**20**
    - `eslint-plugin-deprecation` 1.3.**2** --> 1.3.**3**
    - `eslint-plugin-import` 2.**26**.0 --> 2.**27**.5
    - `eslint-plugin-react` 7.**30**.1 --> 7.**32**.1
    - `fp-ts` 2.**12**.1 --> 2.**13**.1
    - `i18next` **21**.8.11 --> **22**.0.6
    - `io-ts` 2.2.**16** --> 2.2.**20**
    - `moment` 2.29.**3** --> 2.29.**4**
    - `optics-ts` 2.**2**.2 --> 2.**4**.0
    - `react-redux` 8.0.**2** --> 8.0.**5**
    - `react-router` 5.3.**1** --> 5.3.**4**
    - `redux-saga` 1.**1**.3 --> 1.**2**.2
    - `ts-opt` 4.**0**.1 --> 4.**1**.0
    - `typescript` 4.**7**.4 --> 4.**9**.4
    - `@typescript-eslint/eslint-plugin` 5.**30**.3 --> 5.**48**.2
    - `@typescript-eslint/parser` 5.**30**.3 --> 5.**48**.2
    - `eventemitter3` **4**.0.1 --> **5**.0.0
    - `@swc/core` 1.3.**24** --> 1.3.**27**
    - `@types/jest` **28**.1.4 --> **29**.2.6
    - `@types/node` **12**.12.6 --> **18**.11.18
    - `eslint` **7**.32.0 --> **8**.32.0
    - `eslint-import-resolver-typescript` **2**.7.1 --> **3**.5.3
    - `eslint-plugin-unicorn` **40**.1.0 --> **45**.0.2
    - `jest` **28**.1.2 --> **29**.3.1
    - `jest-environment-jsdom` **28**.1.2 --> **29**.3.1
    - `rimraf` **3**.0.2 --> **4**.1.1

### 3.1.2 - 18. 01. 2023

#### Added
- [R-12214](http://redmine.favorlogic.com/issues/12214): Added data test id to notification.

#### Changed
- [R-11202](http://redmine.favorlogic.com/issues/11202): Optimized bitbucket pipelines build minutes used.
- Updated following libraries:
    - `@types/lodash` 4.14.**182** --> 4.14.**191**
    - `@types/react` 18.0.**14** --> 18.0.**26**
    - `@types/react-router` 5.1.**18** --> 5.1.**20**
    - `eslint-plugin-deprecation` 1.3.**2** --> 1.3.**3**
    - `eslint-plugin-import` 2.**26**.0 --> 2.**27**.0
    - `eslint-plugin-react` 7.**30**.1 --> 7.**32**.0
    - `fp-ts` 2.**12**.1 --> 2.**13**.1
    - `i18next` **21**.8.11 --> **22**.0.6
    - `io-ts` 2.2.**16** --> 2.2.**20**
    - `moment` 2.29.**3** --> 2.29.**4**
    - `optics-ts` 2.**2**.2 --> 2.**4**.0
    - `react-redux` 8.0.**2** --> 8.0.**5**
    - `react-router` 5.3.**1** --> 5.3.**4**
    - `redux-saga` 1.**1**.3 --> 1.**2**.2
    - `ts-opt` 4.**0**.1 --> 4.**1**.0
    - `typescript` 4.**7**.4 --> 4.**9**.4
    - `@typescript-eslint/eslint-plugin` 5.**30**.3 --> 5.**48**.1
    - `@typescript-eslint/parser` 5.**30**.3 --> 5.**48**.1
    - `eventemitter3` **4**.0.1 --> **5**.0.0

---

### 3.1.1 - 9. 1. 2023

#### Added
- Added following libraries:
    - `@swc/core` **1.3.24**
    - `@swc/jest` **0.2.24**

#### Removed
- Removed following libraries:
    - `ts-jest`

#### Fixed
- [R-12342](http://redmine.favorlogic.com/issues/12342): Fixed close icon to show again on notification.
- [R-12345](http://redmine.favorlogic.com/issues/12345): Fixed logging type validation errors.

---

### 3.1.0 - 20. 12. 2022

#### Changed
- [R-12177](http://redmine.favorlogic.com/issues/12177): Build optimization.

---

### 3.0.0 - 22. 11 .2022

#### Added
- [R-11586](http://redmine.favorlogic.com/issues/11586): Added option to handle server errors in a custom manner.

---

### 2.0.0 - 20. 10. 2022

#### Added
- [R-10578](http://redmine.favorlogic.com/issues/10578): Added useWindowDimensions hook for getting info about window resolutions.
- [R-10881](http://redmine.favorlogic.com/issues/10881): Added extractVersion utility for formatting data for app info.
- [R-11277](http://redmine.favorlogic.com/issues/11277): Added own implementation of notifications.
- [R-11685](http://redmine.favorlogic.com/issues/11685): Added params custom hooks
- Added following libraries:
    - `react-router` **5.3.1**
    - `@types/react-router` **5.1.18**

#### Changed
- Updated following libraries:
    - `ts-opt` **3**.2.0 --> **4**.7.4

#### Removed
- Removed following libraries:
    - `@types/react-redux-toastr`
    - `react-redux-toastr`

---

### 1.3.0 - 15. 9. 2022

#### Changed
- [R-11601](http://redmine.favorlogic.com/issues/11601): Changed date-time format for tables to `DD.MM.YYYY HH:mm`.

---

### 1.2.0 - 6. 9. 2022

#### Fixed
- [R-11479](http://redmine.favorlogic.com/issues/11479): Added `strict` option to reformatDate method.

---

### 1.1.1 - 1. 9. 2022

#### Changed
- [R-11373](http://redmine.favorlogic.com/issues/11373): Allowed unicode characters to be inputted in auth data.

---

### 1.1.0 - 12. 8. 2022

#### Added
- [R-10365](http://redmine.favorlogic.com/issues/10365) - Added option to extract errors with severity parameter from response.
- [R-10863](http://redmine.favorlogic.com/issues/10863): Added option to process BE errors with or without severity parameter.
- [R-10567](http://redmine.favorlogic.com/issues/10567): Added option to send version header with requests and handle version mismatch responses.
- Added following libraries:
    - `awesome-typed-sass-modules` **1.2.0**

#### Changed
- Updated following libraries:
    - `eslint-plugin-import` 2.**5**.4 --> 2.**6**.0

---

### 1.0.0 -  6. 7. 2022

#### Added
- [R-10481](http://redmine.favorlogic.com/issues/10481): Added `CHANGELOG.md`.
- Added utility functions `orderByAsc`, `orderByDesc`, `setItemBy` and `toggleItemBy`.
- Fixed library exports

#### Changed
- Added support for `string`s to `setAt`.
- Updated following libraries:
    - `i18next` 21.**6**.**16** --> 21.**8**.**11**
    - `react-redux` 8.0.**1** --> 8.0.**2**
    - `@types/jest` **27**.**5**.**0** --> **28**.**1**.**4**
    - `@types/react` 18.0.**8** --> 18.0.**14**
    - `@typescript-eslint/eslint-plugin` **4**.**33**.**0** --> **5**.**30**.**3**
    - `@typescript-eslint/parser` **4**.**33**.**0** --> **5**.**30**.**3**
    - `eslint-plugin-react` 7.**29**.**4** --> 7.**30**.**1**
    - `eslint-plugin-react-hooks` 4.**6**.0 --> 4.**6**.0
    - `react` 18.**1**.0 --> 18.**2**.0
    - `react-dom` 18.**1**.0 --> 18.**2**.0
    - `react-test-renderer` 18.**1**.0 --> 18.**2**.0
    - `ts-jest` 28.0.**1** --> 28.0.**5**

---

### 0.11.0 -  6. 5.2022

#### Added
- Support React 18.
- Added following libraries:
    - `jest-environment-jsdom`

#### Changed
- Updated following libraries:
    - `@types/jest` 27.**4**.**1** ---> 27.**5**.**0**
    - `@types/lodash` 4.14.**180** ---> 4.14.**182**
    - `@types/react` **16**.**14**.**24** ---> **18**.**0**.**8**
    - `@types/react-test-renderer` **16**.**9**.**5** ---> **18**.**0**.**0**
    - `axios` 0.**26**.**1** --> 0.**27**.**2**
    - `eslint-import-resolver-typescript` 2.**5**.**0** ---> 2.**7**.**1**
    - `eslint-plugin-import` 2.**25**.**4** --> 2.**26**.**0**
    - `eslint-plugin-react-hooks` 4.**3**.0 --> 4.**5**.0
    - `eslint-plugin-unicorn` **36**.**0**.0 --> **40**.**1**.0
    - `fp-ts` 2.**11**.**9** --> 2.**12**.**1**
    - `i18next` 21.16.**14** --> 21.16.**16**
    - `jest` **27**.**5**.**1** ---> **28**.**0**.**3**
    - `moment` 2.29.**1** --> 2.29.**3**
    - `react` **16**.**13**.**1** --> **18**.**1**.**0**
    - `react-dom` **16**.**13**.**1** --> **18**.**1**.**0**
    - `react-redux` **7**.**2**.**6** --> **8**.**0**.**1**
    - `react-test-renderer` **16**.**13**.**1** --> **18**.**1**.**0**
    - `redux` 4.**1**.**2** --> 4.**2**.**0**
    - `ts-jest` **27**.**1**.**3** ---> **28**.**0**.**1**

#### Removed
- Removed following libraries:
    - `enyzme`
    - `enzyme-adapter-react-16`
