Favorlogic utils
===

Installation
---

Tested with Node 22.10.0

PNPM Install
```sh
corepack enable
```
```sh
pnpm i
```

Development
---

Run check tasks:

```sh
pnpm check
```

Run tests:

```sh
pnpm test
```

Building
---

For build use:

```sh
pnpm build
```

Local development
---

```sh
pnpm pack /path/to/this/package
```
creates fl-utils-X.Y.Z.tgz which can be used in package.json as

```sh
"@fl/utils": "file:/path/to/fl-utils-X.Y.Z.tgz"
```
