module.exports = require('babel-jest').default.createTransformer({
    plugins: [
        '@babel/plugin-transform-runtime',
    ],
    presets: [
        '@babel/preset-react',
        '@babel/preset-env',
    ],
});
