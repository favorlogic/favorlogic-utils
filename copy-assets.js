// eslint-disable-next-line @typescript-eslint/no-var-requires, @typescript-eslint/no-require-imports, no-undef
const copyfiles = require('copyfiles');

const copyTo = 'dist/';
const paths = [
    'src/**/*.png',
    'src/**/*.jpg',
    'src/**/*.gif',
    'src/**/*.ico',
    'src/**/*.svg',
    copyTo,
];

copyfiles(paths, {up: 1}, () => undefined);
